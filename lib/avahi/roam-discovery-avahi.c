/* roam-discovery-avahi.c
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "roam-discovery-avahi"

#include <avahi-gobject/ga-client.h>
#include <avahi-gobject/ga-service-browser.h>
#include <avahi-gobject/ga-service-resolver.h>

#include "roam-discovery-avahi.h"
#include "roam-peer-avahi.h"

struct _RoamDiscoveryAvahi
{
  RoamDiscovery      parent_instance;
  GaClient          *client;
  GaServiceBrowser  *browser;
};

G_DEFINE_TYPE (RoamDiscoveryAvahi, roam_discovery_avahi, ROAM_TYPE_DISCOVERY)

static gchar *
create_peer_id (gint         interface,
                GaProtocol   protocol,
                const gchar *name,
                const gchar *type,
                const gchar *domain)
{
  return g_strdup_printf ("avahi.%d.%d.%s.%s.%s",
                          interface, protocol, type, domain, name);
}

static void
roam_discovery_avahi_new_service_cb (RoamDiscoveryAvahi  *self,
                                     gint                 interface,
                                     GaProtocol           protocol,
                                     const gchar         *name,
                                     const gchar         *type,
                                     const gchar         *domain,
                                     GaLookupResultFlags  flags,
                                     GaServiceBrowser    *browser)
{
  g_autoptr(RoamDiscoveryAvahi) peer = NULL;
  GaServiceResolver *resolver = NULL;
  g_autoptr(GError) error = NULL;
  g_autofree gchar *peer_id = NULL;

  g_assert (ROAM_IS_DISCOVERY_AVAHI (self));
  g_assert (IS_GA_SERVICE_BROWSER (browser));

  if (g_strcmp0 (type, "_roam._tcp") != 0)
    return;

  resolver = ga_service_resolver_new (interface, protocol, name, type, domain,
                                      GA_PROTOCOL_UNSPEC, GA_LOOKUP_NO_FLAGS);

  peer_id = create_peer_id (interface, protocol, name, type, domain);
  peer = g_object_new (ROAM_TYPE_PEER_AVAHI,
                       "id", peer_id,
                       "resolver", resolver,
                       NULL);

  if (!ga_service_resolver_attach (resolver, self->client, &error))
    g_warning ("Failed to attach Avahi resolver: %s", error->message);
  else
    roam_discovery_emit_peer_added (ROAM_DISCOVERY (self), ROAM_PEER (peer));

  g_clear_object (&resolver);
}

static void
roam_discovery_avahi_removed_service_cb (RoamDiscoveryAvahi  *self,
                                         gint                 interface,
                                         GaProtocol           protocol,
                                         const gchar         *name,
                                         const gchar         *type,
                                         const gchar         *domain,
                                         GaLookupResultFlags  flags,
                                         GaServiceBrowser    *browser)
{
  g_autofree gchar *peer_id = NULL;
  RoamPeer *peer;

  g_assert (ROAM_IS_DISCOVERY_AVAHI (self));
  g_assert (IS_GA_SERVICE_BROWSER (browser));

  if (g_strcmp0 (type, "_roam._tcp") != 0)
    return;

  peer_id = create_peer_id (interface, protocol, name, type, domain);
  peer = roam_discovery_get_peer_by_id (ROAM_DISCOVERY (self), peer_id);
  if (peer != NULL)
    roam_discovery_emit_peer_removed (ROAM_DISCOVERY (self), peer);
}

static void
roam_discovery_avahi_startup_async (RoamDiscovery       *discovery,
                                    GCancellable        *cancellable,
                                    GAsyncReadyCallback  callback,
                                    gpointer             user_data)
{
  RoamDiscoveryAvahi *self = (RoamDiscoveryAvahi *)discovery;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = NULL;

  g_assert (ROAM_IS_DISCOVERY_AVAHI (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  g_debug ("initializing avahi discovery");

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, roam_discovery_avahi_startup_async);
  g_task_set_priority (task, G_PRIORITY_LOW);

  self->client = ga_client_new (GA_CLIENT_FLAG_NO_FLAGS);

  if (!ga_client_start (self->client, &error))
    {
      g_debug ("%s", error->message);
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  self->browser = ga_service_browser_new ("_roam._tcp");

  if (!ga_service_browser_attach (self->browser, self->client, &error))
    {
      g_debug ("%s", error->message);
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  g_signal_connect_object (self->browser,
                           "new-service",
                           G_CALLBACK (roam_discovery_avahi_new_service_cb),
                           self,
                           G_CONNECT_SWAPPED);

  g_signal_connect_object (self->browser,
                           "removed-service",
                           G_CALLBACK (roam_discovery_avahi_removed_service_cb),
                           self,
                           G_CONNECT_SWAPPED);

  g_task_return_boolean (task, TRUE);
}

static gboolean
roam_discovery_avahi_startup_finish (RoamDiscovery  *discovery,
                                     GAsyncResult   *result,
                                     GError        **error)
{
  g_assert (ROAM_IS_DISCOVERY_AVAHI (discovery));
  g_assert (G_IS_TASK (result));

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
roam_discovery_avahi_dispose (GObject *object)
{
  RoamDiscoveryAvahi *self = (RoamDiscoveryAvahi *)object;

  g_clear_object (&self->client);
  g_clear_object (&self->browser);

  G_OBJECT_CLASS (roam_discovery_avahi_parent_class)->dispose (object);
}

static void
roam_discovery_avahi_class_init (RoamDiscoveryAvahiClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  RoamDiscoveryClass *discovery_class = ROAM_DISCOVERY_CLASS (klass);

  object_class->dispose = roam_discovery_avahi_dispose;

  discovery_class->startup_async = roam_discovery_avahi_startup_async;
  discovery_class->startup_finish = roam_discovery_avahi_startup_finish;
}

static void
roam_discovery_avahi_init (RoamDiscoveryAvahi *self)
{
}

RoamDiscoveryAvahi *
roam_discovery_avahi_new (void)
{
  return g_object_new (ROAM_TYPE_DISCOVERY_AVAHI, NULL);
}
