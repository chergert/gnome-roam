/* roam-discovery-avahi.h
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "roam-discovery.h"

G_BEGIN_DECLS

#define ROAM_TYPE_DISCOVERY_AVAHI (roam_discovery_avahi_get_type())

G_DECLARE_FINAL_TYPE (RoamDiscoveryAvahi, roam_discovery_avahi, ROAM, DISCOVERY_AVAHI, RoamDiscovery)

RoamDiscoveryAvahi *roam_discovery_avahi_new (void);

G_END_DECLS
