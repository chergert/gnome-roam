/* roam-peer-avahi.c
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "roam-peer-avahi"

#include <avahi-gobject/ga-service-resolver.h>
#include <jsonrpc-glib.h>

#include "roam-file.h"
#include "roam-mount.h"
#include "roam-peer-avahi.h"
#include "roam-rpc-client.h"
#include "roam-utils.h"

struct _RoamPeerAvahi
{
  RoamPeer           parent_instance;
  GaServiceResolver *resolver;
  GCancellable      *cancellable;
  RoamRpcClient     *client;
};

G_DEFINE_TYPE (RoamPeerAvahi, roam_peer_avahi, ROAM_TYPE_PEER)

enum {
  PROP_0,
  PROP_RESOLVER,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
roam_peer_avahi_mount_added_cb (RoamPeerAvahi *self,
                                GVariant      *mount_info,
                                RoamRpcClient *client)
{
  const gchar *symbolic_icon_name = NULL;
  const gchar *icon_name = NULL;
  const gchar *mount_id = NULL;
  const gchar *name = NULL;
  gboolean success;

  g_assert (ROAM_IS_PEER_AVAHI (self));
  g_assert (mount_info != NULL);
  g_assert (ROAM_IS_RPC_CLIENT (client));

  success = JSONRPC_MESSAGE_PARSE (mount_info,
    "mount-id", JSONRPC_MESSAGE_GET_STRING (&mount_id),
    "name", JSONRPC_MESSAGE_GET_STRING (&name),
    "icon-name", JSONRPC_MESSAGE_GET_STRING (&icon_name),
    "symbolic-icon-name", JSONRPC_MESSAGE_GET_STRING (&symbolic_icon_name)
  );

  if (success)
    {
      g_autoptr(RoamMount) mount = NULL;

      mount = roam_mount_new (mount_id, name, icon_name, symbolic_icon_name);
      roam_mount_add_peer (mount, ROAM_PEER (self));
      roam_peer_emit_mount_added (ROAM_PEER (self), mount);
    }
}

static void
roam_peer_avahi_mount_removed_cb (RoamPeerAvahi *self,
                                  GVariant      *mount_info,
                                  RoamRpcClient *client)
{
  const gchar *mount_id = NULL;
  gboolean success;

  g_assert (ROAM_IS_PEER_AVAHI (self));
  g_assert (mount_info != NULL);
  g_assert (ROAM_IS_RPC_CLIENT (client));

  success = JSONRPC_MESSAGE_PARSE (mount_info,
    "mount-id", JSONRPC_MESSAGE_GET_STRING (&mount_id)
  );

  if (success)
    {
      RoamMount *mount = roam_peer_get_mount_by_id (ROAM_PEER (self), mount_id);

      if (mount != NULL)
        roam_peer_emit_mount_removed (ROAM_PEER (self), mount);
    }
}

static void
roam_peer_avahi_client_init_cb (GObject      *object,
                                GAsyncResult *result,
                                gpointer      user_data)
{
  RoamRpcClient *client = (RoamRpcClient *)object;
  g_autoptr(RoamPeerAvahi) self = user_data;
  g_autoptr(GError) error = NULL;

  g_assert (ROAM_IS_RPC_CLIENT (client));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (ROAM_IS_PEER_AVAHI (self));

  if (!g_async_initable_init_finish (G_ASYNC_INITABLE (client), result, &error))
    {
      g_warning ("Failed to initialize RPC client: %s", error->message);
      g_clear_object (&self->client);
      return;
    }

  g_debug ("RPC client initialized");
}

static void
roam_peer_avahi_connect_cb (GObject      *object,
                            GAsyncResult *result,
                            gpointer      user_data)
{
  GSocketClient *client = (GSocketClient *)object;
  g_autoptr(RoamPeerAvahi) self = user_data;
  g_autoptr(GSocketConnection) conn = NULL;
  g_autoptr(GError) error = NULL;

  g_assert (G_IS_SOCKET_CLIENT (client));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (ROAM_IS_PEER_AVAHI (self));

  conn = g_socket_client_connect_finish (client, result, &error);

  if (error != NULL)
    {
      g_warning ("Failed to connect to avahi peer: %s", error->message);
      return;
    }

  self->client = roam_rpc_client_new_for_stream (G_IO_STREAM (conn));

  g_signal_connect_object (self->client,
                           "mount-added",
                           G_CALLBACK (roam_peer_avahi_mount_added_cb),
                           self,
                           G_CONNECT_SWAPPED);

  g_signal_connect_object (self->client,
                           "mount-removed",
                           G_CALLBACK (roam_peer_avahi_mount_removed_cb),
                           self,
                           G_CONNECT_SWAPPED);

  g_async_initable_init_async (G_ASYNC_INITABLE (self->client),
                               G_PRIORITY_LOW,
                               self->cancellable,
                               roam_peer_avahi_client_init_cb,
                               g_object_ref (self));
}

static void
roam_peer_avahi_connect (RoamPeerAvahi      *self,
                         GInetSocketAddress *address)
{
  g_autoptr(GSocketClient) client = NULL;
  GInetAddress *inet_addr;
  GSocketFamily family;

  g_assert (ROAM_IS_PEER_AVAHI (self));
  g_assert (G_IS_SOCKET_ADDRESS (address));

  inet_addr = g_inet_socket_address_get_address (address);
  family = g_inet_address_get_family (inet_addr);

  client = g_socket_client_new ();
  g_socket_client_set_family (client, family);

  /* XXX: for now */
  g_socket_client_set_tls (client, TRUE);
  g_socket_client_set_tls_validation_flags (client, 0);

  g_socket_client_connect_async (client,
                                 G_SOCKET_CONNECTABLE (address),
                                 self->cancellable,
                                 roam_peer_avahi_connect_cb,
                                 g_object_ref (self));
}

static void
roam_peer_avahi_resolver_found_cb (RoamPeerAvahi       *self,
                                   AvahiIfIndex         iface,
                                   GaProtocol           protocol,
                                   const gchar         *name,
                                   const gchar         *type,
                                   const gchar         *domain,
                                   const gchar         *host_name,
                                   const AvahiAddress  *address,
                                   gint                 port,
                                   AvahiStringList     *txt,
                                   GaLookupResultFlags  flags,
                                   GaServiceResolver   *resolver)
{
  g_autoptr(GInetAddress) inet_addr = NULL;
  g_autoptr(GInetSocketAddress) sock_addr = NULL;

  g_assert (ROAM_IS_PEER_AVAHI (self));
  g_assert (IS_GA_SERVICE_RESOLVER (resolver));
  g_assert (address != NULL);
  g_assert (port > 0);

  switch (protocol)
    {
    case AVAHI_PROTO_INET:
      inet_addr = g_inet_address_new_from_bytes ((const guint8 *)&address->data.ipv4.address,
                                                 G_SOCKET_FAMILY_IPV4);
      break;

    case AVAHI_PROTO_INET6:
      inet_addr = g_inet_address_new_from_bytes (address->data.ipv6.address,
                                                 G_SOCKET_FAMILY_IPV6);
      break;

    default:
      g_warning ("Unknown avahi protocol %02x", protocol);
      return;
    }

  sock_addr = g_object_new (G_TYPE_INET_SOCKET_ADDRESS,
                            "address", inet_addr,
                            "port", port,
                            NULL);

  roam_peer_avahi_connect (self, sock_addr);
}

static void
roam_peer_avahi_resolver_failure_cb (RoamPeerAvahi     *self,
                                     const GError      *error,
                                     GaServiceResolver *resolver)
{
  g_assert (ROAM_IS_PEER_AVAHI (self));
  g_assert (error != NULL);
  g_assert (IS_GA_SERVICE_RESOLVER (resolver));

  /* Peer has failed, but we should be notified of that
   * higher up and kill this peer from the discovery agent.
   */
}

static void
roam_peer_avahi_list_files_cb (GObject      *object,
                               GAsyncResult *result,
                               gpointer      user_data)
{
  RoamRpcClient *client = (RoamRpcClient *)object;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GVariant) vfiles = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GPtrArray) files = NULL;

  g_assert (ROAM_IS_RPC_CLIENT (client));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  vfiles = roam_rpc_client_list_files_finish (client, result, &error);

  if (error != NULL)
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  if (!(g_variant_is_of_type (vfiles, G_VARIANT_TYPE ("mv")) ||
        g_variant_is_of_type (vfiles, G_VARIANT_TYPE ("aa{sv}")) ||
        g_variant_is_of_type (vfiles, G_VARIANT_TYPE ("av"))))
    {
      g_warning ("Invalid files encoding");
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               G_IO_ERROR_INVALID_DATA,
                               "Unexpected type received from peer");
      return;
    }

  files = g_ptr_array_new_with_free_func (g_object_unref);

  if (vfiles != NULL)
    {
      guint n_children = g_variant_n_children (vfiles);
      const gchar *base;

      base = g_object_get_data (G_OBJECT (task), "PATH_BASE");
      g_assert (base != NULL);

      for (guint i = 0; i < n_children; i++)
        {
          g_autoptr(GVariant) vfile = g_variant_get_child_value (vfiles, i);
          const gchar *name = NULL;
          const gchar *icon_name = NULL;
          const gchar *symbolic_icon_name = NULL;
          const gchar *content_type = NULL;
          gint64 size = 0;
          gboolean is_dir = FALSE;
          gboolean success;

          success = JSONRPC_MESSAGE_PARSE (vfile,
            "name", JSONRPC_MESSAGE_GET_STRING (&name),
            "is-dir", JSONRPC_MESSAGE_GET_BOOLEAN (&is_dir),
            "icon", JSONRPC_MESSAGE_GET_STRING (&icon_name),
            "symbolic-icon", JSONRPC_MESSAGE_GET_STRING (&symbolic_icon_name),
            "content-type", JSONRPC_MESSAGE_GET_STRING (&content_type),
            "size", JSONRPC_MESSAGE_GET_INT64 (&size)
          );

          if (success)
            {
              g_autoptr(RoamFile) file = NULL;
              g_autoptr(GIcon) icon = NULL;
              g_autoptr(GIcon) symbolic_icon = NULL;
              g_autofree gchar *path = NULL;

              if (!roam_str_empty0 (icon_name))
                icon = g_icon_new_for_string (icon_name, NULL);

              if (symbolic_icon_name != NULL)
                symbolic_icon = g_icon_new_for_string (symbolic_icon_name, NULL);

              path = g_build_filename (base, name, NULL);
              file = roam_file_new_for_path (path);
              roam_file_set_is_directory (file, is_dir);
              roam_file_set_content_type (file, content_type);
              roam_file_set_icon (file, icon);
              roam_file_set_symbolic_icon (file, symbolic_icon);
              roam_file_set_size (file, size);

              g_ptr_array_add (files, g_steal_pointer (&file));
            }
        }
    }

  g_task_return_pointer (task,
                         g_steal_pointer (&files),
                         (GDestroyNotify)g_ptr_array_unref);
}

static void
roam_peer_avahi_list_files_async (RoamPeer            *peer,
                                  RoamMount           *mount,
                                  const gchar         *path,
                                  GCancellable        *cancellable,
                                  GAsyncReadyCallback  callback,
                                  gpointer             user_data)
{
  RoamPeerAvahi *self = (RoamPeerAvahi *)peer;
  g_autoptr(GTask) task = NULL;

  g_assert (ROAM_IS_PEER_AVAHI (self));
  g_assert (ROAM_IS_MOUNT (mount));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, roam_peer_avahi_list_files_async);
  g_task_set_priority (task, G_PRIORITY_LOW);

  if (self->client == NULL)
    {
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               G_IO_ERROR_NOT_CONNECTED,
                               "No connection to peer available");
      return;
    }

  g_object_set_data_full (G_OBJECT (task), "PATH_BASE", g_strdup (path), g_free);

  roam_rpc_client_list_files_async (self->client,
                                    roam_mount_get_id (mount),
                                    path,
                                    cancellable,
                                    roam_peer_avahi_list_files_cb,
                                    g_steal_pointer (&task));
}

static GPtrArray *
roam_peer_avahi_list_files_finish (RoamPeer      *peer,
                                   GAsyncResult  *result,
                                   GError       **error)
{
  g_assert (ROAM_IS_PEER_AVAHI (peer));
  g_assert (G_IS_TASK (result));

  return g_task_propagate_pointer (G_TASK (result), error);
}

static void
roam_peer_avahi_constructed (GObject *object)
{
  RoamPeerAvahi *self = (RoamPeerAvahi *)object;

  g_assert (ROAM_IS_PEER_AVAHI (self));
  g_assert (self->resolver != NULL);

  g_signal_connect_object (self->resolver,
                           "found",
                           G_CALLBACK (roam_peer_avahi_resolver_found_cb),
                           self,
                           G_CONNECT_SWAPPED);

  g_signal_connect_object (self->resolver,
                           "failure",
                           G_CALLBACK (roam_peer_avahi_resolver_failure_cb),
                           self,
                           G_CONNECT_SWAPPED);

  G_OBJECT_CLASS (roam_peer_avahi_parent_class)->constructed (object);
}

static void
roam_peer_avahi_dispose (GObject *object)
{
  RoamPeerAvahi *self = (RoamPeerAvahi *)object;

  g_cancellable_cancel (self->cancellable);
  g_clear_object (&self->cancellable);
  g_clear_object (&self->resolver);

  G_OBJECT_CLASS (roam_peer_avahi_parent_class)->dispose (object);
}

static void
roam_peer_avahi_get_property (GObject    *object,
                              guint       prop_id,
                              GValue     *value,
                              GParamSpec *pspec)
{
  RoamPeerAvahi *self = ROAM_PEER_AVAHI (object);

  switch (prop_id)
    {
    case PROP_RESOLVER:
      g_value_set_object (value, self->resolver);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
roam_peer_avahi_set_property (GObject      *object,
                              guint         prop_id,
                              const GValue *value,
                              GParamSpec   *pspec)
{
  RoamPeerAvahi *self = ROAM_PEER_AVAHI (object);

  switch (prop_id)
    {
    case PROP_RESOLVER:
      self->resolver = g_value_dup_object (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
roam_peer_avahi_class_init (RoamPeerAvahiClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  RoamPeerClass *peer_class = ROAM_PEER_CLASS (klass);

  object_class->constructed = roam_peer_avahi_constructed;
  object_class->dispose = roam_peer_avahi_dispose;
  object_class->get_property = roam_peer_avahi_get_property;
  object_class->set_property = roam_peer_avahi_set_property;

  peer_class->list_files_async = roam_peer_avahi_list_files_async;
  peer_class->list_files_finish = roam_peer_avahi_list_files_finish;

  /**
   * RoamPeerAvahi:resolver:
   *
   * The "resover" property is a #GaResolver that can be used to
   * resolve the peer address.
   *
   * Since: 3.28
   */
  properties [PROP_RESOLVER] =
    g_param_spec_object ("resolver",
                         "Resolver",
                         "The avahi resolver for the peer",
                         GA_TYPE_SERVICE_RESOLVER,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
roam_peer_avahi_init (RoamPeerAvahi *self)
{
}
