/* roam-publisher-avahi.c
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "roam-publisher-avahi"

#include <avahi-gobject/ga-client.h>
#include <avahi-gobject/ga-entry-group.h>

#include "roam-publisher-avahi.h"
#include "roam-rpc-server.h"
#include "roam-tls-manager.h"
#include "roam-utils.h"

struct _RoamPublisherAvahi
{
  RoamPublisher  parent_instance;

  /*
   * The RoamManager we are publishing/exporting to the local network.
   * We use this to validate peers, check permissions, list mounts and
   * files, etc.
   */
  RoamManager *manager;

  /*
   * The RPC server protocol we use to communicate with peers and accept
   * new connections.
   */
  RoamRpcServer *server;

  /*
   * Our Avahi client to notify the local network of addresses for which they
   * can connect to us. We automatically advertise our connection when the
   * socket reaches the listen state.
   */
  GaClient *avahi_client;

  /*
   * The entry group contains the mapping of service type (_roam._tcp) to a
   * given address (1.2.3.4:1234) with service name (localhost).
   */
  GaEntryGroup *avahi_entry_group;
};

enum {
  PROP_0,
  PROP_MANAGER,
  N_PROPS
};

G_DEFINE_TYPE (RoamPublisherAvahi, roam_publisher_avahi, ROAM_TYPE_PUBLISHER)

static GParamSpec *properties [N_PROPS];

static void
roam_publisher_avahi_publish (RoamPublisherAvahi *self,
                              GSocket            *socket)
{
  g_autoptr(GSocketAddress) address = NULL;
  g_autoptr(GError) error = NULL;
  GaEntryGroupService *service;
  const gchar *peer_id;
  guint16 port;

  g_assert (ROAM_IS_PUBLISHER_AVAHI (self));
  g_assert (G_IS_SOCKET (socket));

  address = g_socket_get_local_address (socket, &error);

  if (error != NULL)
    {
      g_warning ("Failed to advertise, could not get local address: %s",
                 error->message);
      return;
    }

  if (!G_IS_INET_SOCKET_ADDRESS (address))
    return;

  port = g_inet_socket_address_get_port (G_INET_SOCKET_ADDRESS (address));

  /* Setup our Avahi client if necessary. */

  if (self->avahi_client == NULL)
    {
      self->avahi_client = ga_client_new (GA_CLIENT_FLAG_NO_FLAGS);

      if (!ga_client_start (self->avahi_client, &error))
        {
          g_clear_object (&self->avahi_client);
          g_warning ("Failed to create avahi client: %s", error->message);
          return;
        }
    }

  g_assert (self->avahi_client != NULL);
  g_assert (IS_GA_CLIENT (self->avahi_client));

  /* Setup Avahi Entry Group if necessary */

  if (self->avahi_entry_group == NULL)
    {
      self->avahi_entry_group = ga_entry_group_new ();

      if (!ga_entry_group_attach (self->avahi_entry_group, self->avahi_client, &error))
        {
          g_clear_object (&self->avahi_entry_group);
          g_warning ("Failed to create avahi entry group: %s", error->message);
          return;
        }
    }

  /* Publish this socket info to the entry group. It would be nice if we
   * could limit to the interface the socket address is listening on, but
   * to do that on Linux we need to setup a Netlink interface to track the
   * index changes of the routing table entries. Not fun (nor even possible
   * without priveliges, likely).
   */

  service = ga_entry_group_add_service (self->avahi_entry_group,
                                        g_get_host_name (),
                                        "_roam._tcp",
                                        port,
                                        &error,
                                        NULL);
  if (service == NULL)
    {
      g_clear_object (&self->avahi_entry_group);
      return;
    }

  peer_id = roam_get_peer_id ();

  ga_entry_group_service_freeze (service);
  if (!ga_entry_group_service_set (service, "peer-id", peer_id, &error) ||
      !ga_entry_group_service_thaw (service, &error))
    {
      g_clear_object (&self->avahi_entry_group);
      g_warning ("Failed to register TXT info: %s", error->message);
      return;
    }

  if (!ga_entry_group_commit (self->avahi_entry_group, &error))
    {
      g_clear_object (&self->avahi_entry_group);
      g_warning ("Failed to commit Avahi entry group: %s", error->message);
      return;
    }
}


static void
roam_publisher_avahi_server_event (RoamPublisherAvahi   *self,
                                   GSocketListenerEvent  event,
                                   GSocket              *socket,
                                   RoamRpcServer        *server)
{
  g_assert (ROAM_IS_PUBLISHER_AVAHI (self));
  g_assert (G_IS_SOCKET (socket));
  g_assert (ROAM_IS_RPC_SERVER (server));

  switch (event)
    {
    case G_SOCKET_LISTENER_BINDING:
    case G_SOCKET_LISTENER_BOUND:
    case G_SOCKET_LISTENER_LISTENING:
    default:
      break;

    case G_SOCKET_LISTENER_LISTENED:
      roam_publisher_avahi_publish (self, socket);
      break;
    }
}

static void
roam_publisher_avahi_load_certificate_cb (GObject      *object,
                                          GAsyncResult *result,
                                          gpointer      user_data)
{
  RoamPublisherAvahi *self;
  RoamTlsManager *tls_manager = (RoamTlsManager *)object;
  g_autoptr(GTlsCertificate) cert = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;

  g_assert (ROAM_IS_TLS_MANAGER (tls_manager));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  cert = roam_tls_manager_load_certificate_finish (tls_manager, result, &error);

  if (error != NULL)
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  self = g_task_get_source_object (task);
  g_assert (ROAM_IS_PUBLISHER_AVAHI (self));

  if (self->manager == NULL)
    {
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               G_IO_ERROR_CANCELLED,
                               "Publisher has been disposed");
      return;
    }

  self->server = roam_rpc_server_new (self->manager, cert);

  g_signal_connect_object (self->server,
                           "event",
                           G_CALLBACK (roam_publisher_avahi_server_event),
                           self,
                           G_CONNECT_SWAPPED);

  /* TODO: We need to have a setting to determine if we should
   *       be listening on this network. We also need to track
   *       the changing of networks to update our settings.
   */

  g_socket_listener_add_any_inet_port (G_SOCKET_LISTENER (self->server), NULL, &error);

  if (error != NULL)
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  g_socket_service_start (G_SOCKET_SERVICE (self->server));

  /*
   * RoamRpcServer will automatically advertise our connection
   * addresses after the socket starts listening.
   */

  g_task_return_boolean (task, TRUE);
}

static void
roam_publisher_avahi_startup_async (RoamPublisher       *self,
                                    GCancellable        *cancellable,
                                    GAsyncReadyCallback  callback,
                                    gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;
  RoamTlsManager *tls_manager;

  g_assert (ROAM_IS_PUBLISHER_AVAHI (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, roam_publisher_avahi_startup_async);
  g_task_set_priority (task, G_PRIORITY_LOW);

  /*
   * We need to load our TLS server certificate. It's self-signed, but
   * it allows us to communicate with peers encrypted with TOFU (trust
   * on first use). Part of the loading process may involve generating
   * a new certificate.
   */

  tls_manager = roam_tls_manager_get_default ();

  roam_tls_manager_load_certificate_async (tls_manager,
                                           cancellable,
                                           roam_publisher_avahi_load_certificate_cb,
                                           g_steal_pointer (&task));
}

static gboolean
roam_publisher_avahi_startup_finish (RoamPublisher  *self,
                                     GAsyncResult   *result,
                                     GError        **error)
{
  g_assert (ROAM_IS_PUBLISHER_AVAHI (self));
  g_assert (G_IS_TASK (result));

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
roam_publisher_avahi_shutdown_async (RoamPublisher       *publisher,
                                     GCancellable        *cancellable,
                                     GAsyncReadyCallback  callback,
                                     gpointer             user_data)
{
  RoamPublisherAvahi *self = (RoamPublisherAvahi *)publisher;
  g_autoptr(GTask) task = NULL;

  g_assert (ROAM_IS_PUBLISHER_AVAHI (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, roam_publisher_avahi_startup_async);
  g_task_set_priority (task, G_PRIORITY_LOW);

  if (self->server != NULL)
    g_socket_service_stop (G_SOCKET_SERVICE (self->server));

  g_clear_object (&self->manager);
  g_clear_object (&self->server);

  g_task_return_boolean (task, TRUE);
}

static gboolean
roam_publisher_avahi_shutdown_finish (RoamPublisher  *self,
                                      GAsyncResult   *result,
                                      GError        **error)
{
  g_assert (ROAM_IS_PUBLISHER_AVAHI (self));
  g_assert (G_IS_TASK (result));

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
roam_publisher_avahi_dispose (GObject *object)
{
  RoamPublisherAvahi *self = (RoamPublisherAvahi *)object;

  roam_dispose_object (&self->avahi_client);
  roam_dispose_object (&self->avahi_entry_group);
  roam_dispose_object (&self->server);

  g_clear_object (&self->manager);

  G_OBJECT_CLASS (roam_publisher_avahi_parent_class)->dispose (object);
}

static void
roam_publisher_avahi_get_property (GObject    *object,
                                   guint       prop_id,
                                   GValue     *value,
                                   GParamSpec *pspec)
{
  RoamPublisherAvahi *self = ROAM_PUBLISHER_AVAHI (object);

  switch (prop_id)
    {
    case PROP_MANAGER:
      g_value_set_object (value, self->manager);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
roam_publisher_avahi_set_property (GObject      *object,
                                   guint         prop_id,
                                   const GValue *value,
                                   GParamSpec   *pspec)
{
  RoamPublisherAvahi *self = ROAM_PUBLISHER_AVAHI (object);

  switch (prop_id)
    {
    case PROP_MANAGER:
      /* Break cycle in dispose */
      self->manager = g_value_dup_object (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
roam_publisher_avahi_class_init (RoamPublisherAvahiClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  RoamPublisherClass *publisher_class = ROAM_PUBLISHER_CLASS (klass);

  object_class->dispose = roam_publisher_avahi_dispose;
  object_class->get_property = roam_publisher_avahi_get_property;
  object_class->set_property = roam_publisher_avahi_set_property;

  publisher_class->startup_async = roam_publisher_avahi_startup_async;
  publisher_class->startup_finish = roam_publisher_avahi_startup_finish;
  publisher_class->shutdown_async = roam_publisher_avahi_shutdown_async;
  publisher_class->shutdown_finish = roam_publisher_avahi_shutdown_finish;

  properties [PROP_MANAGER] =
    g_param_spec_object ("manager",
                         "Manager",
                         "The manager for the publisher",
                         ROAM_TYPE_MANAGER,
                         G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);
  
  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
roam_publisher_avahi_init (RoamPublisherAvahi *self)
{
}

RoamPublisherAvahi *
roam_publisher_avahi_new (void)
{
  return g_object_new (ROAM_TYPE_PUBLISHER_AVAHI, NULL);
}
