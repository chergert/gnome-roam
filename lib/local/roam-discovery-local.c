/* roam-discovery-local.c
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "roam-discovery-local"

#include "roam-discovery-local.h"
#include "roam-file-list.h"
#include "roam-file.h"
#include "roam-mount-list.h"
#include "roam-mount.h"
#include "roam-peer-local.h"

/**
 * SECTION:roam-discovery-local
 * @title: RoamDiscoveryLocal
 * @short_description: a local discovery, primarily for testing
 *
 * The #RoamDiscoveryLocal is used to provide a peer that is backed
 * by local disk data. It's particularly useful for implementing unit tests
 * with mocked data.
 *
 * It is unlikely that this is useful in a production scenario, but that
 * really depends on the direction the project takes.
 *
 * Since: 3.28
 */

struct _RoamDiscoveryLocal
{
  RoamDiscovery  parent_instance;
  RoamPeerLocal *peer;
};

G_DEFINE_TYPE (RoamDiscoveryLocal, roam_discovery_local, ROAM_TYPE_DISCOVERY)

static void
roam_discovery_local_startup_async (RoamDiscovery       *discovery,
                                    GCancellable        *cancellable,
                                    GAsyncReadyCallback  callback,
                                    gpointer             user_data)
{
  RoamDiscoveryLocal *self = (RoamDiscoveryLocal *)discovery;
  g_autoptr(GTask) task = NULL;

  g_return_if_fail (ROAM_IS_DISCOVERY_LOCAL (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, roam_discovery_local_startup_async);
  g_task_set_priority (task, G_PRIORITY_LOW);

  roam_discovery_emit_peer_added (discovery, ROAM_PEER (self->peer));

  g_task_return_boolean (task, TRUE);
}

static gboolean
roam_discovery_local_startup_finish (RoamDiscovery  *discovery,
                                     GAsyncResult   *result,
                                     GError        **error)
{
  g_return_val_if_fail (ROAM_IS_DISCOVERY_LOCAL (discovery), FALSE);
  g_return_val_if_fail (G_IS_TASK (result), FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
roam_discovery_local_shutdown_async (RoamDiscovery       *discovery,
                                     GCancellable        *cancellable,
                                     GAsyncReadyCallback  callback,
                                     gpointer             user_data)
{
  RoamDiscoveryLocal *self = (RoamDiscoveryLocal *)discovery;
  g_autoptr(GTask) task = NULL;

  g_return_if_fail (ROAM_IS_DISCOVERY_LOCAL (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, roam_discovery_local_shutdown_async);
  g_task_set_priority (task, G_PRIORITY_LOW);

  roam_discovery_emit_peer_removed (discovery, ROAM_PEER (self->peer));

  g_task_return_boolean (task, TRUE);
}

static gboolean
roam_discovery_local_shutdown_finish (RoamDiscovery  *discovery,
                                      GAsyncResult   *result,
                                      GError        **error)
{
  g_return_val_if_fail (ROAM_IS_DISCOVERY_LOCAL (discovery), FALSE);
  g_return_val_if_fail (G_IS_TASK (result), FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
roam_discovery_local_dispose (GObject *object)
{
  RoamDiscoveryLocal *self = (RoamDiscoveryLocal *)object;

  g_clear_object (&self->peer);

  G_OBJECT_CLASS (roam_discovery_local_parent_class)->dispose (object);
}

static void
roam_discovery_local_constructed (GObject *object)
{
  RoamDiscoveryLocal *self = (RoamDiscoveryLocal *)object;

  G_OBJECT_CLASS (roam_discovery_local_parent_class)->constructed (object);

  roam_discovery_emit_peer_added (ROAM_DISCOVERY (self), ROAM_PEER (self->peer));
}

static void
roam_discovery_local_class_init (RoamDiscoveryLocalClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  RoamDiscoveryClass *discovery_class = ROAM_DISCOVERY_CLASS (klass);

  object_class->constructed = roam_discovery_local_constructed;
  object_class->dispose = roam_discovery_local_dispose;

  discovery_class->startup_async = roam_discovery_local_startup_async;
  discovery_class->startup_finish = roam_discovery_local_startup_finish;
  discovery_class->shutdown_async = roam_discovery_local_shutdown_async;
  discovery_class->shutdown_finish = roam_discovery_local_shutdown_finish;
}

static void
roam_discovery_local_init (RoamDiscoveryLocal *self)
{
  self->peer = roam_peer_local_new ();
}

RoamDiscoveryLocal *
roam_discovery_local_new (void)
{
  return g_object_new (ROAM_TYPE_DISCOVERY_LOCAL, NULL);
}

/**
 * roam_discovery_local_add_mount:
 * @self: a #RoamDiscoveryLocal
 * @id: the id for the mount
 * @name: the name for the mount
 * @icon_name: the icon name for the mount
 * @symbolic_icon_name: the symbolic icon for the mount
 * @directory: the directory to back the mount
 *
 * This adds a new mount that will be registered with the peer
 * managed by this discovery agent.
 *
 * This is useful when you want to feed data from a local backing
 * directory.
 *
 * Since: 3.28
 */
void
roam_discovery_local_add_mount (RoamDiscoveryLocal *self,
                                const gchar   *id,
                                const gchar   *name,
                                const gchar   *icon_name,
                                const gchar   *symbolic_icon_name,
                                GFile         *directory)
{
  g_return_if_fail (ROAM_IS_DISCOVERY_LOCAL (self));
  g_return_if_fail (id != NULL);
  g_return_if_fail (name != NULL);
  g_return_if_fail (G_IS_FILE (directory));
  g_return_if_fail (self->peer != NULL);

  roam_peer_local_add_mount (self->peer, id, name, icon_name, symbolic_icon_name, directory);
}
