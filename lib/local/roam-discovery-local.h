/* roam-discovery-local.h
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "roam-discovery.h"

G_BEGIN_DECLS

#define ROAM_TYPE_DISCOVERY_LOCAL (roam_discovery_local_get_type())

G_DECLARE_FINAL_TYPE (RoamDiscoveryLocal, roam_discovery_local, ROAM, DISCOVERY_LOCAL, RoamDiscovery)

RoamDiscoveryLocal *roam_discovery_local_new       (void);
void                roam_discovery_local_add_mount (RoamDiscoveryLocal *self,
                                                    const gchar        *id,
                                                    const gchar        *name,
                                                    const gchar        *icon_name,
                                                    const gchar        *symbolic_icon_name,
                                                    GFile              *directory);

G_END_DECLS
