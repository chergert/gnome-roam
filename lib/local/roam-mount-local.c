/* roam-mount-local.c
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "roam-mount-local"

#include "roam-mount-local.h"

struct _RoamMountLocal
{
  RoamMount  parent_instance;
  GFile     *file;
};

G_DEFINE_TYPE (RoamMountLocal, roam_mount_local, ROAM_TYPE_MOUNT)

enum {
  PROP_0,
  PROP_FILE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
roam_mount_local_dispose (GObject *object)
{
  RoamMountLocal *self = (RoamMountLocal *)object;

  g_clear_object (&self->file);

  G_OBJECT_CLASS (roam_mount_local_parent_class)->dispose (object);
}

static void
roam_mount_local_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  RoamMountLocal *self = ROAM_MOUNT_LOCAL (object);

  switch (prop_id)
    {
    case PROP_FILE:
      g_value_set_object (value, self->file);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
roam_mount_local_set_property (GObject      *object,
                               guint         prop_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  RoamMountLocal *self = ROAM_MOUNT_LOCAL (object);

  switch (prop_id)
    {
    case PROP_FILE:
      self->file = g_value_dup_object (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
roam_mount_local_class_init (RoamMountLocalClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = roam_mount_local_dispose;
  object_class->get_property = roam_mount_local_get_property;
  object_class->set_property = roam_mount_local_set_property;

  properties [PROP_FILE] =
    g_param_spec_object ("file",
                         "File",
                         "The the root for the mount",
                         G_TYPE_FILE,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
roam_mount_local_init (RoamMountLocal *self)
{
}

/**
 * roam_mount_local_get_file:
 * @self: a #RoamMountLocal
 *
 * Gets the file that is the root of the mount.
 *
 * Returns: (transfer none): a #GFile
 *
 * Since: 3.28
 */
GFile *
roam_mount_local_get_file (RoamMountLocal *self)
{
  g_return_val_if_fail (ROAM_IS_MOUNT_LOCAL (self), NULL);

  return self->file;
}
