/* roam-peer-local.c
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "roam-peer-local"

#include "roam-file.h"
#include "roam-file-list.h"
#include "roam-mount-local.h"
#include "roam-mount.h"
#include "roam-mount-list.h"
#include "roam-peer-local.h"
#include "roam-utils.h"

#define DEFAULT_ENUM_LIST_SIZE 25

/**
 * SECTION:roam-peer-local
 * @title: RoamPeerLocal
 * @short_description: A peer representing a local directory
 *
 * The #RoamPeerLocal class is primarily a mock peer for testing
 * synchronization without having to use the network.
 *
 * It is primarily for testing, but may become useful in other
 * scenarios in the future.
 *
 * Since: 3.28
 */

struct _RoamPeerLocal
{
  RoamPeer parent_instance;
};

G_DEFINE_TYPE (RoamPeerLocal, roam_peer_local, ROAM_TYPE_PEER)

static void
roam_peer_local_next_files_cb (GObject      *object,
                               GAsyncResult *result,
                               gpointer      user_data)
{
  GFileEnumerator *enumerator = (GFileEnumerator *)object;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;
  g_autolist(GFileInfo) list = NULL;
  const gchar *base;
  GPtrArray *files;

  g_assert (G_IS_FILE_ENUMERATOR (enumerator));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  files = g_task_get_task_data (task);
  g_assert (files != NULL);

  list = g_file_enumerator_next_files_finish (enumerator, result, &error);

  if (error != NULL)
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  base = g_object_get_data (G_OBJECT (task), "PATH_BASE");
  g_assert (base != NULL);

  for (const GList *iter = list; iter != NULL; iter = iter->next)
    {
      GFileInfo *info = iter->data;
      g_autoptr(RoamFile) file = NULL;
      g_autofree gchar *path = NULL;
      const gchar *name;
      const gchar *content_type;
      GFileType file_type;
      GObject *icon;
      guint64 size;

      file_type = g_file_info_get_file_type (info);

      if (file_type != G_FILE_TYPE_DIRECTORY && file_type != G_FILE_TYPE_REGULAR)
        continue;

      name = g_file_info_get_name (info);
      path = g_build_filename (base, name, NULL);
      file = roam_file_new_for_path (path);

      icon = g_file_info_get_attribute_object (info, G_FILE_ATTRIBUTE_STANDARD_ICON);
      roam_file_set_icon (file, G_ICON (icon));

      icon = g_file_info_get_attribute_object (info, G_FILE_ATTRIBUTE_STANDARD_SYMBOLIC_ICON);
      roam_file_set_symbolic_icon (file, G_ICON (icon));

      size = g_file_info_get_attribute_uint64 (info, G_FILE_ATTRIBUTE_STANDARD_SIZE);
      roam_file_set_size (file, size);

      content_type = g_file_info_get_attribute_string (info, G_FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE);
      roam_file_set_content_type (file, content_type);

      roam_file_set_is_directory (file, file_type == G_FILE_TYPE_DIRECTORY);

      g_ptr_array_add (files, g_steal_pointer (&file));
    }

  if (list != NULL)
    {
      GCancellable *cancellable = g_task_get_cancellable (task);
      g_file_enumerator_next_files_async (enumerator,
                                          DEFAULT_ENUM_LIST_SIZE,
                                          G_PRIORITY_LOW,
                                          cancellable,
                                          roam_peer_local_next_files_cb,
                                          g_steal_pointer (&task));
      return;
    }

  g_task_return_pointer (task,
                         g_ptr_array_ref (files),
                         (GDestroyNotify) g_ptr_array_unref);
}

static void
roam_peer_local_enumerate_children_cb (GObject      *object,
                                       GAsyncResult *result,
                                       gpointer      user_data)
{
  GFile *local_file = (GFile *)object;
  g_autoptr(GFileEnumerator) enumerator = NULL;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;
  GCancellable *cancellable;

  g_assert (G_IS_FILE (local_file));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  enumerator = g_file_enumerate_children_finish (local_file, result, &error);

  if (error != NULL)
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  cancellable = g_task_get_cancellable (task);

  g_file_enumerator_next_files_async (enumerator,
                                      DEFAULT_ENUM_LIST_SIZE,
                                      G_PRIORITY_LOW,
                                      cancellable,
                                      roam_peer_local_next_files_cb,
                                      g_steal_pointer (&task));
}

static void
roam_peer_local_list_files_async (RoamPeer            *peer,
                                  RoamMount           *mount,
                                  const gchar         *path,
                                  GCancellable        *cancellable,
                                  GAsyncReadyCallback  callback,
                                  gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;
  g_autoptr(GFile) child = NULL;
  GFile *root;

  g_assert (ROAM_IS_PEER_LOCAL (peer));
  g_assert (ROAM_IS_MOUNT_LOCAL (mount));
  g_assert (path != NULL);
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (peer, cancellable, callback, user_data);
  g_task_set_source_tag (task, roam_peer_local_list_files_async);
  g_task_set_priority (task, G_PRIORITY_LOW);
  g_task_set_task_data (task,
                        g_ptr_array_new_with_free_func (g_object_unref),
                        (GDestroyNotify) g_ptr_array_unref);

  root = roam_mount_local_get_file (ROAM_MOUNT_LOCAL (mount));

  if (path == NULL)
    path = "/";

  g_object_set_data_full (G_OBJECT (task), "PATH_BASE", g_strdup (path), g_free);

  while (*path == '/')
    path++;

  if (roam_str_empty0 (path))
    child = g_object_ref (root);
  else
    child = g_file_get_child (root, path);

  g_file_enumerate_children_async (child,
                                   G_FILE_ATTRIBUTE_STANDARD_NAME","
                                   G_FILE_ATTRIBUTE_STANDARD_SIZE","
                                   G_FILE_ATTRIBUTE_STANDARD_TYPE","
                                   G_FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE","
                                   G_FILE_ATTRIBUTE_STANDARD_ICON","
                                   G_FILE_ATTRIBUTE_STANDARD_SYMBOLIC_ICON,
                                   G_FILE_QUERY_INFO_NOFOLLOW_SYMLINKS,
                                   G_PRIORITY_LOW,
                                   cancellable,
                                   roam_peer_local_enumerate_children_cb,
                                   g_steal_pointer (&task));
}

static GPtrArray *
roam_peer_local_list_files_finish (RoamPeer      *peer,
                                   GAsyncResult  *result,
                                   GError       **error)
{
  g_assert (ROAM_IS_PEER_LOCAL (peer));
  g_assert (G_IS_TASK (result));

  return g_task_propagate_pointer (G_TASK (result), error);
}

static void
roam_peer_local_class_init (RoamPeerLocalClass *klass)
{
  RoamPeerClass *peer_class = ROAM_PEER_CLASS (klass);

  peer_class->list_files_async = roam_peer_local_list_files_async;
  peer_class->list_files_finish = roam_peer_local_list_files_finish;
}

static void
roam_peer_local_init (RoamPeerLocal *self)
{
}

RoamPeerLocal *
roam_peer_local_new (void)
{
  return g_object_new (ROAM_TYPE_PEER_LOCAL, NULL);
}

void
roam_peer_local_add_mount (RoamPeerLocal *self,
                           const gchar   *id,
                           const gchar   *name,
                           const gchar   *icon_name,
                           const gchar   *symbolic_icon_name,
                           GFile         *root)
{
  g_autoptr(RoamMountLocal) local = NULL;

  g_return_if_fail (ROAM_IS_PEER_LOCAL (self));
  g_return_if_fail (G_IS_FILE (root));
  g_return_if_fail (id != NULL);
  g_return_if_fail (name != NULL);

  local = g_object_new (ROAM_TYPE_MOUNT_LOCAL,
                        "exportable", TRUE,
                        "id", id,
                        "name", name,
                        "file", root,
                        "icon-name", icon_name,
                        "symbolic-icon-name", symbolic_icon_name,
                        NULL);
  roam_mount_add_peer (ROAM_MOUNT (local), ROAM_PEER (self));
  roam_peer_emit_mount_added (ROAM_PEER (self), ROAM_MOUNT (local));
}
