/* roam-avatar.c
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "roam-avatar.h"

#include "roam-avatar.h"
#include "roam-enums.h"

G_DEFINE_INTERFACE (RoamAvatar, roam_avatar, G_TYPE_OBJECT)

enum {
  MOUNT_ADDED,
  MOUNT_REMOVED,
  MONITOR_CHANGED,
  N_SIGNALS
};

static guint signals [N_SIGNALS];

static void
roam_avatar_default_init (RoamAvatarInterface *iface)
{
  signals [MOUNT_ADDED] =
    g_signal_new ("mount-added",
                  G_TYPE_FROM_INTERFACE (iface),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (RoamAvatarInterface, mount_added),
                  NULL, NULL,
                  g_cclosure_marshal_generic,
                  G_TYPE_NONE,
                  2,
                  G_TYPE_STRING | G_SIGNAL_TYPE_STATIC_SCOPE,
                  G_TYPE_VARIANT | G_SIGNAL_TYPE_STATIC_SCOPE);

  signals [MOUNT_REMOVED] =
    g_signal_new ("mount-removed",
                  G_TYPE_FROM_INTERFACE (iface),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (RoamAvatarInterface, mount_removed),
                  NULL, NULL,
                  g_cclosure_marshal_generic,
                  G_TYPE_NONE,
                  2,
                  G_TYPE_STRING | G_SIGNAL_TYPE_STATIC_SCOPE,
                  G_TYPE_VARIANT | G_SIGNAL_TYPE_STATIC_SCOPE);

  signals [MONITOR_CHANGED] =
    g_signal_new ("monitor-changed",
                  G_TYPE_FROM_INTERFACE (iface),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (RoamAvatarInterface, monitor_changed),
                  NULL, NULL,
                  g_cclosure_marshal_generic,
                  G_TYPE_NONE,
                  3,
                  G_TYPE_UINT,
                  ROAM_TYPE_MONITOR_EVENT,
                  G_TYPE_STRING | G_SIGNAL_TYPE_STATIC_SCOPE);
}

void
roam_avatar_list_mounts_async (RoamAvatar          *self,
                               GCancellable        *cancellable,
                               GAsyncReadyCallback  callback,
                               gpointer             user_data)
{
  g_return_if_fail (ROAM_IS_AVATAR (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  ROAM_AVATAR_GET_IFACE (self)->list_mounts_async (self, cancellable, callback, user_data);
}

GPtrArray *
roam_avatar_list_mounts_finish (RoamAvatar    *self,
                                GAsyncResult  *result,
                                GError       **error)
{
  g_return_val_if_fail (ROAM_IS_AVATAR (self), NULL);
  g_return_val_if_fail (G_IS_ASYNC_RESULT (result), NULL);

  return ROAM_AVATAR_GET_IFACE (self)->list_mounts_finish (self, result, error);
}

void
roam_avatar_enumerate_children_async (RoamAvatar          *self,
                                      const gchar         *mount_id,
                                      const gchar         *path,
                                      guint                more_token,
                                      GCancellable        *cancellable,
                                      GAsyncReadyCallback  callback,
                                      gpointer             user_data)
{
  g_return_if_fail (ROAM_IS_AVATAR (self));
  g_return_if_fail (mount_id != NULL);
  g_return_if_fail (path != NULL);
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  ROAM_AVATAR_GET_IFACE (self)->enumerate_children_async (self, mount_id, path, more_token, cancellable, callback, user_data);
}

GPtrArray *
roam_avatar_enumerate_children_finish (RoamAvatar    *self,
                                       GAsyncResult  *result,
                                       guint         *more_token,
                                       GError       **error)
{
  g_return_val_if_fail (ROAM_IS_AVATAR (self), NULL);
  g_return_val_if_fail (G_IS_ASYNC_RESULT (result), NULL);

  if (more_token != NULL)
    *more_token = 0;

  return ROAM_AVATAR_GET_IFACE (self)->enumerate_children_finish (self, result, more_token, error);
}

void
roam_avatar_monitor_async (RoamAvatar          *self,
                           const gchar         *mount_id,
                           const gchar         *path,
                           GCancellable        *cancellable,
                           GAsyncReadyCallback  callback,
                           gpointer             user_data)
{
  g_return_if_fail (ROAM_IS_AVATAR (self));
  g_return_if_fail (mount_id != NULL);
  g_return_if_fail (path != NULL);
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  ROAM_AVATAR_GET_IFACE (self)->monitor_async (self, mount_id, path, cancellable, callback, user_data);
}

guint
roam_avatar_monitor_finish (RoamAvatar    *self,
                            GAsyncResult  *result,
                            GError       **error)
{
  g_return_val_if_fail (ROAM_IS_AVATAR (self), 0);
  g_return_val_if_fail (G_IS_ASYNC_RESULT (result), 0);

  return ROAM_AVATAR_GET_IFACE (self)->monitor_finish (self, result, error);
}

void
roam_avatar_unmonitor_async (RoamAvatar          *self,
                             guint                monitor_id,
                             GCancellable        *cancellable,
                             GAsyncReadyCallback  callback,
                             gpointer             user_data)
{
  g_return_if_fail (ROAM_IS_AVATAR (self));
  g_return_if_fail (monitor_id > 0);
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  ROAM_AVATAR_GET_IFACE (self)->unmonitor_async (self, monitor_id, cancellable, callback, user_data);
}

guint
roam_avatar_unmonitor_finish (RoamAvatar    *self,
                              GAsyncResult  *result,
                              GError       **error)
{
  g_return_val_if_fail (ROAM_IS_AVATAR (self), 0);
  g_return_val_if_fail (G_IS_ASYNC_RESULT (result), 0);

  return ROAM_AVATAR_GET_IFACE (self)->unmonitor_finish (self, result, error);
}
