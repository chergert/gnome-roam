/* roam-avatar.h
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gio/gio.h>

#include "roam-types.h"

G_BEGIN_DECLS

#define ROAM_TYPE_AVATAR (roam_avatar_get_type())

G_DECLARE_INTERFACE (RoamAvatar, roam_avatar, ROAM, AVATAR, GObject)

struct _RoamAvatarInterface
{
  GTypeInterface parent_iface;

  /* Signals */
  void       (*mount_added)               (RoamAvatar           *self,
                                           const gchar          *mount_id,
                                           GVariant             *mount_info);
  void       (*mount_removed)             (RoamAvatar           *self,
                                           const gchar          *mount_id,
                                           GVariant             *mount_info);
  void       (*monitor_changed)           (RoamAvatar           *avatar,
                                           guint                 monitor_id,
                                           RoamMonitorEvent      event,
                                           const gchar          *path);

  /* VFuncs */
  void       (*list_mounts_async)         (RoamAvatar           *self,
                                           GCancellable         *cancellable,
                                           GAsyncReadyCallback   callback,
                                           gpointer              user_data);
  GPtrArray *(*list_mounts_finish)        (RoamAvatar           *self,
                                           GAsyncResult         *result,
                                           GError              **error);
  void       (*enumerate_children_async)  (RoamAvatar           *self,
                                           const gchar          *mount_id,
                                           const gchar          *path,
                                           guint                 more_token,
                                           GCancellable         *cancellable,
                                           GAsyncReadyCallback   callback,
                                           gpointer              user_data);
  GPtrArray *(*enumerate_children_finish) (RoamAvatar           *self,
                                           GAsyncResult         *result,
                                           guint                *more_token,
                                           GError              **error);
  void       (*monitor_async)             (RoamAvatar           *self,
                                           const gchar          *mount_id,
                                           const gchar          *path,
                                           GCancellable         *cancellable,
                                           GAsyncReadyCallback   callback,
                                           gpointer              user_data);
  guint      (*monitor_finish)            (RoamAvatar           *self,
                                           GAsyncResult         *result,
                                           GError              **error);
  void       (*unmonitor_async)           (RoamAvatar           *self,
                                           guint                 monitor_id,
                                           GCancellable         *cancellable,
                                           GAsyncReadyCallback   callback,
                                           gpointer              user_data);
  gboolean   (*unmonitor_finish)          (RoamAvatar           *self,
                                           GAsyncResult         *result,
                                           GError              **error);
};

void        roam_avatar_list_mounts_async         (RoamAvatar           *self,
                                                   GCancellable         *cancellable,
                                                   GAsyncReadyCallback   callback,
                                                   gpointer              user_data);
GPtrArray  *roam_avatar_list_mounts_finish        (RoamAvatar           *self,
                                                   GAsyncResult         *result,
                                                   GError              **error);
void        roam_avatar_enumerate_children_async  (RoamAvatar           *self,
                                                   const gchar          *mount_id,
                                                   const gchar          *path,
                                                   guint                 more_token,
                                                   GCancellable         *cancellable,
                                                   GAsyncReadyCallback   callback,
                                                   gpointer              user_data);
GPtrArray  *roam_avatar_enumerate_children_finish (RoamAvatar           *self,
                                                   GAsyncResult         *result,
                                                   guint                *more_token,
                                                   GError              **error);
void        roam_avatar_monitor_async             (RoamAvatar           *self,
                                                   const gchar          *mount_id,
                                                   const gchar          *path,
                                                   GCancellable         *cancellable,
                                                   GAsyncReadyCallback   callback,
                                                   gpointer              user_data);
guint       roam_avatar_monitor_finish            (RoamAvatar           *self,
                                                   GAsyncResult         *result,
                                                   GError              **error);
void        roam_avatar_unmonitor_async           (RoamAvatar           *self,
                                                   guint                 monitor_id,
                                                   GCancellable         *cancellable,
                                                   GAsyncReadyCallback   callback,
                                                   gpointer              user_data);
guint       roam_avatar_unmonitor_finish          (RoamAvatar           *self,
                                                   GAsyncResult         *result,
                                                   GError              **error);

G_END_DECLS
