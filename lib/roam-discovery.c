/* roam-discovery.c
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "roam-discovery"

#include "roam-discovery.h"
#include "roam-peer.h"

typedef struct
{
  GPtrArray *peers;
} RoamDiscoveryPrivate;

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE (RoamDiscovery, roam_discovery, G_TYPE_OBJECT)

enum {
  PEER_ADDED,
  PEER_REMOVED,
  N_SIGNALS
};

static guint signals [N_SIGNALS];

static void
roam_discovery_real_peer_added (RoamDiscovery *self,
                                RoamPeer      *peer)
{
  RoamDiscoveryPrivate *priv = roam_discovery_get_instance_private (self);

  g_assert (ROAM_IS_DISCOVERY (self));
  g_assert (ROAM_IS_PEER (peer));

  g_ptr_array_add (priv->peers, g_object_ref (peer));
}

static void
roam_discovery_real_peer_removed (RoamDiscovery *self,
                                  RoamPeer      *peer)
{
  RoamDiscoveryPrivate *priv = roam_discovery_get_instance_private (self);

  g_assert (ROAM_IS_DISCOVERY (self));
  g_assert (ROAM_IS_PEER (peer));

  g_ptr_array_remove (priv->peers, peer);
}

static void
roam_discovery_real_startup_async (RoamDiscovery       *self,
                                   GCancellable        *cancellable,
                                   GAsyncReadyCallback  callback,
                                   gpointer             user_data)
{
  g_autoptr(GTask) task = g_task_new (self, cancellable, callback, user_data);
  g_task_return_boolean (task, TRUE);
}

static gboolean
roam_discovery_real_startup_finish (RoamDiscovery  *self,
                                    GAsyncResult   *result,
                                    GError        **error)
{
  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
roam_discovery_real_shutdown_async (RoamDiscovery       *self,
                                    GCancellable        *cancellable,
                                    GAsyncReadyCallback  callback,
                                    gpointer             user_data)
{
  g_autoptr(GTask) task = g_task_new (self, cancellable, callback, user_data);
  g_task_return_boolean (task, TRUE);
}

static gboolean
roam_discovery_real_shutdown_finish (RoamDiscovery  *self,
                                     GAsyncResult   *result,
                                     GError        **error)
{
  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
roam_discovery_dispose (GObject *object)
{
  RoamDiscovery *self = (RoamDiscovery *)object;
  RoamDiscoveryPrivate *priv = roam_discovery_get_instance_private (self);

  g_clear_pointer (&priv->peers, g_ptr_array_unref);

  G_OBJECT_CLASS (roam_discovery_parent_class)->dispose (object);
}

static void
roam_discovery_class_init (RoamDiscoveryClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = roam_discovery_dispose;

  klass->startup_async = roam_discovery_real_startup_async;
  klass->startup_finish = roam_discovery_real_startup_finish;
  klass->shutdown_async = roam_discovery_real_shutdown_async;
  klass->shutdown_finish = roam_discovery_real_shutdown_finish;
  klass->peer_added = roam_discovery_real_peer_added;
  klass->peer_removed = roam_discovery_real_peer_removed;

  signals [PEER_ADDED] =
    g_signal_new ("peer-added",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (RoamDiscoveryClass, peer_added),
                  NULL, NULL, NULL,
                  G_TYPE_NONE, 1, ROAM_TYPE_PEER);

  signals [PEER_REMOVED] =
    g_signal_new ("peer-removed",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (RoamDiscoveryClass, peer_removed),
                  NULL, NULL, NULL,
                  G_TYPE_NONE, 1, ROAM_TYPE_PEER);
}

static void
roam_discovery_init (RoamDiscovery *self)
{
  RoamDiscoveryPrivate *priv = roam_discovery_get_instance_private (self);

  priv->peers = g_ptr_array_new_with_free_func (g_object_unref);
}

void
roam_discovery_startup_async (RoamDiscovery       *self,
                              GCancellable        *cancellable,
                              GAsyncReadyCallback  callback,
                              gpointer             user_data)
{
  ROAM_DISCOVERY_GET_CLASS (self)->startup_async (self, cancellable, callback, user_data);
}

gboolean
roam_discovery_startup_finish (RoamDiscovery  *self,
                               GAsyncResult   *result,
                               GError        **error)
{
  return ROAM_DISCOVERY_GET_CLASS (self)->startup_finish (self, result, error);
}

void
roam_discovery_shutdown_async (RoamDiscovery       *self,
                               GCancellable        *cancellable,
                               GAsyncReadyCallback  callback,
                               gpointer             user_data)
{
  ROAM_DISCOVERY_GET_CLASS (self)->shutdown_async (self, cancellable, callback, user_data);
}

gboolean
roam_discovery_shutdown_finish (RoamDiscovery  *self,
                                GAsyncResult   *result,
                                GError        **error)
{
  return ROAM_DISCOVERY_GET_CLASS (self)->shutdown_finish (self, result, error);
}

void
roam_discovery_emit_peer_added (RoamDiscovery *self,
                                RoamPeer      *peer)
{
  g_return_if_fail (ROAM_IS_DISCOVERY (self));
  g_return_if_fail (ROAM_IS_PEER (peer));

  g_signal_emit (self, signals [PEER_ADDED], 0, peer);
}

void
roam_discovery_emit_peer_removed (RoamDiscovery *self,
                                  RoamPeer      *peer)
{
  g_return_if_fail (ROAM_IS_DISCOVERY (self));
  g_return_if_fail (ROAM_IS_PEER (peer));

  g_signal_emit (self, signals [PEER_REMOVED], 0, peer);
}

/**
 * roam_discovery_get_peers:
 * @self: a #RoamDiscovery
 *
 * Gets the peers that have been registered by the discovery.
 *
 * Returns: (transfer container): a #GPtrArray of #RoamPeer
 *
 * Since: 3.28
 */
GPtrArray *
roam_discovery_get_peers (RoamDiscovery *self)
{
  RoamDiscoveryPrivate *priv = roam_discovery_get_instance_private (self);
  g_autoptr(GPtrArray) copy = NULL;

  g_return_val_if_fail (ROAM_IS_DISCOVERY (self), NULL);

  copy = g_ptr_array_new_with_free_func (g_object_unref);

  if (priv->peers != NULL)
    {
      for (guint i = 0; i < priv->peers->len; i++)
        {
          RoamPeer *peer = g_ptr_array_index (priv->peers, i);
          g_ptr_array_add (copy, g_object_ref (peer));
        }
    }

  return g_steal_pointer (&copy);
}

/**
 * roam_discovery_get_peer_by_id:
 * @self: a #RoamDiscovery
 * @peer_id: the identifier for the peer
 *
 * Gets a peer by the #RoamPeer:id identifier.
 *
 * Returns: (transfer none) (nullable): a #RoamPeer or %NULL
 *
 * Since: 3.28
 */
RoamPeer *
roam_discovery_get_peer_by_id (RoamDiscovery *self,
                               const gchar   *peer_id)
{
  RoamDiscoveryPrivate *priv = roam_discovery_get_instance_private (self);

  g_return_val_if_fail (ROAM_IS_DISCOVERY (self), NULL);

  for (guint i = 0; i < priv->peers->len; i++)
    {
      RoamPeer *peer = g_ptr_array_index (priv->peers, i);
      const gchar *id = roam_peer_get_id (peer);

      if (g_strcmp0 (peer_id, id) == 0)
        return peer;
    }

  return NULL;
}
