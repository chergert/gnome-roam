/* roam-discovery.h
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gio/gio.h>

#include "roam-types.h"

G_BEGIN_DECLS

#define ROAM_TYPE_DISCOVERY (roam_discovery_get_type())

G_DECLARE_DERIVABLE_TYPE (RoamDiscovery, roam_discovery, ROAM, DISCOVERY, GObject)

struct _RoamDiscoveryClass
{
  GObjectClass parent;

  void     (*startup_async)   (RoamDiscovery        *self,
                               GCancellable         *cancellable,
                               GAsyncReadyCallback   callback,
                               gpointer              user_data);
  gboolean (*startup_finish)  (RoamDiscovery        *self,
                               GAsyncResult         *result,
                               GError              **error);
  void     (*shutdown_async)  (RoamDiscovery        *self,
                               GCancellable         *cancellable,
                               GAsyncReadyCallback   callback,
                               gpointer              user_data);
  gboolean (*shutdown_finish) (RoamDiscovery        *self,
                               GAsyncResult         *result,
                               GError              **error);

  void     (*peer_added)      (RoamDiscovery        *self,
                               RoamPeer             *peer);
  void     (*peer_removed)    (RoamDiscovery        *self,
                               RoamPeer             *peer);

  /*< private >*/
  gpointer _reserved[12];
};

GPtrArray *roam_discovery_get_peers         (RoamDiscovery        *self);
RoamPeer  *roam_discovery_get_peer_by_id    (RoamDiscovery        *self,
                                             const gchar          *peer);
void       roam_discovery_emit_peer_added   (RoamDiscovery        *self,
                                             RoamPeer             *peer);
void       roam_discovery_emit_peer_removed (RoamDiscovery        *self,
                                             RoamPeer             *peer);
void       roam_discovery_startup_async     (RoamDiscovery        *self,
                                             GCancellable         *cancellable,
                                             GAsyncReadyCallback   callback,
                                             gpointer              user_data);
gboolean   roam_discovery_startup_finish    (RoamDiscovery        *self,
                                             GAsyncResult         *result,
                                             GError              **error);
void       roam_discovery_shutdown_async    (RoamDiscovery        *self,
                                             GCancellable         *cancellable,
                                             GAsyncReadyCallback   callback,
                                             gpointer              user_data);
gboolean   roam_discovery_shutdown_finish   (RoamDiscovery        *self,
                                             GAsyncResult         *result,
                                             GError              **error);

G_END_DECLS
