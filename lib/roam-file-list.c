/* roam-file-list.c
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "roam-file-list"

#include "roam-file.h"
#include "roam-file-list.h"
#include "roam-path.h"

struct _RoamFileList
{
  GObject parent_instance;

  /*
   * A cancellable that should be used by work operations. That allows all
   * operations to cancel when roam_file_list_cancel() is called.
   */
  GCancellable *cancellable;

  /*
   * The mount identifier that contains this file list.
   */
  gchar *mount_id;

  /*
   * The path for the file list within the mount.
   */
  gchar *path;

  /*
   * A GSequence of file items, sorted to simplify deduplication and natural
   * sort when browsing contents.
   */
  GSequence *items;

  /*
   * Track the busy count for the RoamMount so that we can provide an accurate
   * busy property while peers are loading data.
   */
  gint busy;
};

static void list_model_iface_init (GListModelInterface *iface);

G_DEFINE_TYPE_WITH_CODE (RoamFileList, roam_file_list, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (G_TYPE_LIST_MODEL, list_model_iface_init))

enum {
  PROP_0,
  PROP_BUSY,
  PROP_MOUNT_ID,
  PROP_PATH,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
roam_file_list_set_path (RoamFileList *self,
                         const gchar  *path)
{
  g_assert (ROAM_IS_FILE_LIST (self));

  if (path == NULL)
    path = "/";

  self->path = roam_path_canonicalize (path);
}

static void
roam_file_list_dispose (GObject *object)
{
  RoamFileList *self = (RoamFileList *)object;

  g_debug ("Disposing file list for %s of mount %s\n",
           self->path, self->mount_id);

  g_clear_pointer (&self->mount_id, g_free);
  g_clear_pointer (&self->path, g_free);
  g_clear_pointer (&self->items, g_sequence_free);

  G_OBJECT_CLASS (roam_file_list_parent_class)->dispose (object);
}

static void
roam_file_list_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
  RoamFileList *self = ROAM_FILE_LIST (object);

  switch (prop_id)
    {
    case PROP_BUSY:
      g_value_set_boolean (value, roam_file_list_get_busy (self));
      break;

    case PROP_MOUNT_ID:
      g_value_set_string (value, roam_file_list_get_mount_id (self));
      break;

    case PROP_PATH:
      g_value_set_string (value, roam_file_list_get_path (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
roam_file_list_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
  RoamFileList *self = ROAM_FILE_LIST (object);

  switch (prop_id)
    {
    case PROP_MOUNT_ID:
      self->mount_id = g_value_dup_string (value);
      break;

    case PROP_PATH:
      roam_file_list_set_path (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
roam_file_list_class_init (RoamFileListClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = roam_file_list_dispose;
  object_class->get_property = roam_file_list_get_property;
  object_class->set_property = roam_file_list_set_property;

  /**
   * RoamFileList:busy:
   *
   * The "busy" property is set to %TRUE when peers are performing
   * work on behalf of the file list.
   *
   * Since: 3.28
   */
  properties [PROP_BUSY] =
    g_param_spec_boolean ("busy",
                          "Busy",
                          "If the peers are busy doing work for the mount",
                          FALSE,
                          G_PARAM_READABLE | G_PARAM_STATIC_STRINGS);

  /**
   * RoamFileList:mount-id:
   *
   * The "mount-id" property is the identifier of the mount. A peer
   * will use this to resolve the path into the proper export.
   *
   * Since: 3.28
   */
  properties [PROP_MOUNT_ID] =
    g_param_spec_string ("mount-id",
                         "Mount Identifier",
                         "The identifier of the mount being queried",
                         NULL,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  /**
   * RoamFileList:path:
   *
   * The "path" property is the path for which the files are contained.
   *
   * Since: 3.28
   */
  properties [PROP_PATH] =
    g_param_spec_string ("path",
                         "Path",
                         "The path of the file list",
                         NULL,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
roam_file_list_init (RoamFileList *self)
{
  self->cancellable = g_cancellable_new ();
  self->items = g_sequence_new (g_object_unref);
}

/**
 * roam_file_list_get_mount_id:
 *
 * Gets the #RoamFileList:mount-id property.
 *
 * This is the identifier of the mount for which #RoamFileList:path is
 * contained within.
 *
 * Returns: the mount identifier
 *
 * Since: 3.28
 */
const gchar *
roam_file_list_get_mount_id (RoamFileList *self)
{
  g_return_val_if_fail (ROAM_IS_FILE_LIST (self), NULL);

  return self->mount_id;
}

/**
 * roam_file_list_get_path:
 * @self: a #RoamFileList
 *
 * Gets the "path" property, which is the virtual path for which
 * contains the list of files.
 *
 * Returns: the path for the file list
 *
 * Since: 3.28
 */
const gchar *
roam_file_list_get_path (RoamFileList *self)
{
  g_return_val_if_fail (ROAM_IS_FILE_LIST (self), NULL);

  return self->path;
}

static GType
roam_file_list_get_item_type (GListModel *model)
{
  return ROAM_TYPE_FILE;
}

static guint
roam_file_list_get_n_items (GListModel *model)
{
  RoamFileList *self = (RoamFileList *)model;

  g_assert (ROAM_IS_FILE_LIST (self));

  return g_sequence_get_length (self->items);
}

static gpointer
roam_file_list_get_item (GListModel *model,
                         guint       position)
{
  RoamFileList *self = (RoamFileList *)model;
  GSequenceIter *iter;

  g_assert (ROAM_IS_FILE_LIST (self));
  g_assert (position < g_sequence_get_length (self->items));

  iter = g_sequence_get_iter_at_pos (self->items, position);

  if (!g_sequence_iter_is_end (iter))
    return g_object_ref (g_sequence_get (iter));

  return NULL;
}

static void
list_model_iface_init (GListModelInterface *iface)
{
  iface->get_item_type = roam_file_list_get_item_type;
  iface->get_n_items = roam_file_list_get_n_items;
  iface->get_item = roam_file_list_get_item;
}

void
roam_file_list_remove (RoamFileList *self,
                       RoamFile     *file)
{
  GSequenceIter *iter;
  RoamFile *existing;

  g_return_if_fail (ROAM_IS_FILE_LIST (self));
  g_return_if_fail (ROAM_IS_FILE (file));

  iter = g_sequence_lookup (self->items,
                            file,
                            (GCompareDataFunc)roam_file_compare,
                            NULL);
  g_return_if_fail (iter != NULL);

  existing = g_sequence_get (iter);
  g_assert (ROAM_IS_FILE (existing));

  if (roam_file_release (existing))
    {
      guint position;

      position = g_sequence_iter_get_position (iter);
      g_sequence_remove (iter);
      g_list_model_items_changed (G_LIST_MODEL (self), position, 1, 0);
    }
}

/**
 * roam_file_list_take:
 * @self: a #RoamFileList
 * @file: (transfer full): a #RoamFile
 *
 * Adds @file to the list.
 *
 * This should only be used by #RoamPeer to add files to the
 * list when populating.
 *
 * This function steals the reference to @file.
 *
 * Since: 3.28
 */
void
roam_file_list_take (RoamFileList *self,
                     RoamFile     *file)
{
  GSequenceIter *iter;

  g_return_if_fail (ROAM_IS_FILE_LIST (self));
  g_return_if_fail (ROAM_IS_FILE (file));

  iter = g_sequence_lookup (self->items,
                            file,
                            (GCompareDataFunc)roam_file_compare,
                            NULL);

  /*
   * We already have another file within the mount matching this name.
   * That would be because multiple peers provided a file in the mount
   * at this path with the same name. The might be the same file, or the
   * could be different versions of the same file, or they could actually
   * be different files.
   *
   * For now, we're just going to trust peers, and simple use a count.
   * Where this could break down, is if peers proxy mallicious remove or
   * add events causing the files to disapear when they shouldn't.
   */
  if (iter != NULL)
    {
      RoamFile *existing = g_sequence_get (iter);
      g_assert (ROAM_IS_FILE (existing));
      roam_file_hold (existing);
      return;
    }

  iter = g_sequence_insert_sorted (self->items,
                                   file,
                                   (GCompareDataFunc)roam_file_compare,
                                   NULL);

  g_list_model_items_changed (G_LIST_MODEL (self),
                              g_sequence_iter_get_position (iter),
                              0, 1);
}

/**
 * roam_file_list_add:
 * @self: a #RoamFileList
 * @file: a #RoamFile
 *
 * Adds @file to the list.
 *
 * This should only be used by #RoamPeer to add files to the
 * list when populating.
 *
 * Since: 3.28
 */
void
roam_file_list_add (RoamFileList *self,
                    RoamFile     *file)
{
  g_return_if_fail (ROAM_IS_FILE_LIST (self));
  g_return_if_fail (ROAM_IS_FILE (file));

  roam_file_list_take (self, g_object_ref (file));
}

/**
 * roam_file_list_new:
 * @mount_id: the identifier for the mount
 * @path: the path within the mount
 *
 * Creates a new #RoamFileList.
 *
 * @mount_id should be the identifier of the mount which will
 * populate the file list.
 *
 * @path should be the path within the mount.
 *
 * Generally, you do not need to call this as it will be handled
 * internally by #RoamManger.
 *
 * Returns: (transfer full): a #RoamFileList
 */
RoamFileList *
roam_file_list_new (const gchar *mount_id,
                    const gchar *path)
{
  g_return_val_if_fail (mount_id != NULL, NULL);

  return g_object_new (ROAM_TYPE_FILE_LIST,
                       "mount-id", mount_id,
                       "path", path,
                       NULL);
}

/**
 * roam_file_list_get_cancellable:
 *
 * Gets a #GCancellable that can be used in operations that are
 * related to this #RoamFileList.
 *
 * Generally, the #RoamPeer's might use this to cancel their
 * background operations that are updating the list contents.
 *
 * Returns: (transfer none): A #GCancellable
 *
 * Since: 3.28
 */
GCancellable *
roam_file_list_get_cancellable (RoamFileList *self)
{
  g_return_val_if_fail (ROAM_IS_FILE_LIST (self), NULL);

  return self->cancellable;
}

/**
 * roam_file_list_cancel:
 *
 * Cancels any pending operations that have been registered with
 * the #RoamFileList. Generally, this means the background operations
 * to populate the file list.
 *
 * Since: 3.28
 */
void
roam_file_list_cancel (RoamFileList *self)
{
  g_autoptr(GCancellable) cancellable = g_cancellable_new ();

  g_return_if_fail (ROAM_IS_FILE_LIST (self));

  /* Cancel any operations in flight and reset the cancellable. */
  g_cancellable_cancel (self->cancellable);
  g_set_object (&self->cancellable, cancellable);
}

/**
 * roam_file_list_clear:
 *
 * Removes all items from the #RoamFileList.
 *
 * Generally, this should only be used by #RoamMount to handle cases
 * where peers have changed and need to be reloaded.
 *
 * Since: 3.28
 */
void
roam_file_list_clear (RoamFileList *self)
{
  g_return_if_fail (ROAM_IS_FILE_LIST (self));

  if (!g_sequence_is_empty (self->items))
    {
      guint n_items = g_sequence_get_length (self->items);

      g_sequence_remove_range (g_sequence_get_begin_iter (self->items),
                               g_sequence_get_end_iter (self->items));
      g_list_model_items_changed (G_LIST_MODEL (self), 0, n_items, 0);
    }
}

/**
 * roam_file_list_get_busy:
 * @self: a #RoamFileList
 *
 * Gets the #RoamFileList:busy property. This is %TRUE when the file
 * list is doing background work.
 *
 * Returns: %TRUE if the file list is performing background work.
 *
 * Sicne: 3.28
 */
gboolean
roam_file_list_get_busy (RoamFileList *self)
{
  g_return_val_if_fail (ROAM_IS_FILE_LIST (self), FALSE);

  return self->busy > 0;
}

/**
 * roam_file_list_mark_busy:
 * @self: a #RoamFileList
 *
 * Marks the file list as busy. This can be called more than once
 * as long as you call roam_file_list_unmark_busy() the same number
 * of times.
 *
 * Since: 3.28
 */
void
roam_file_list_mark_busy (RoamFileList *self)
{
  g_return_if_fail (ROAM_IS_FILE_LIST (self));
  g_return_if_fail (self->busy >= 0);

  self->busy++;

  if (self->busy == 1)
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_BUSY]);
}

/**
 * roam_file_list_unmark_busy:
 * @self: a #RoamFileList
 *
 * Unmarks a #RoamFileList from being busy.
 *
 * The #RoamFileList will be considered unbusy when the busy
 * count has reached zero.
 *
 * Since: 3.28
 */
void
roam_file_list_unmark_busy (RoamFileList *self)
{
  g_return_if_fail (ROAM_IS_FILE_LIST (self));
  g_return_if_fail (self->busy > 0);

  self->busy--;

  if (self->busy == 0)
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_BUSY]);
}
