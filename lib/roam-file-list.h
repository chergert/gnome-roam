/* roam-file-list.h
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "roam-types.h"

G_BEGIN_DECLS

#define ROAM_TYPE_FILE_LIST (roam_file_list_get_type())

G_DECLARE_FINAL_TYPE (RoamFileList, roam_file_list, ROAM, FILE_LIST, GObject)

RoamFileList *roam_file_list_new             (const gchar  *mount_id,
                                              const gchar  *path);
const gchar  *roam_file_list_get_mount_id    (RoamFileList *self);
const gchar  *roam_file_list_get_path        (RoamFileList *self);
void          roam_file_list_add             (RoamFileList *self,
                                              RoamFile     *file);
void          roam_file_list_remove          (RoamFileList *self,
                                              RoamFile     *file);
void          roam_file_list_take            (RoamFileList *self,
                                              RoamFile     *file);
GCancellable *roam_file_list_get_cancellable (RoamFileList *self);
void          roam_file_list_cancel          (RoamFileList *self);
void          roam_file_list_clear           (RoamFileList *self);
void          roam_file_list_mark_busy       (RoamFileList *self);
void          roam_file_list_unmark_busy     (RoamFileList *self);
gboolean      roam_file_list_get_busy        (RoamFileList *self);

G_END_DECLS
