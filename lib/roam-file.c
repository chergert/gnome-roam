/* roam-file.c
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "roam-file"

#include <jsonrpc-glib.h>
#include <string.h>

#include "roam-file.h"
#include "roam-path.h"

/**
 * SECTION:roam-file
 * @title: RoamFile
 * @short_description: represents a virtual file
 *
 * A #RoamFile is a virtual file that can be resolved to either
 * a local backing file or remote file data access.
 *
 * Since: 3.28
 */

struct _RoamFile
{
  GObject      parent_instance;
  gchar       *path;
  GIcon       *icon;
  GIcon       *symbolic_icon;
  const gchar *content_type;
  guint64      size;
  gint         hold;
  guint        is_directory : 1;
};

G_DEFINE_TYPE (RoamFile, roam_file, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_CONTENT_TYPE,
  PROP_ICON,
  PROP_IS_DIRECTORY,
  PROP_NAME,
  PROP_PATH,
  PROP_SIZE,
  PROP_SYMBOLIC_ICON,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
roam_file_set_path (RoamFile    *self,
                    const gchar *path)
{
  g_assert (ROAM_IS_FILE (self));

  if (path == NULL)
    path = "/";

  self->path = roam_path_canonicalize (path);
}

static void
roam_file_finalize (GObject *object)
{
  RoamFile *self = (RoamFile *)object;

  g_clear_pointer (&self->path, g_free);

  G_OBJECT_CLASS (roam_file_parent_class)->finalize (object);
}

static void
roam_file_get_property (GObject    *object,
                        guint       prop_id,
                        GValue     *value,
                        GParamSpec *pspec)
{
  RoamFile *self = ROAM_FILE (object);

  switch (prop_id)
    {
    case PROP_ICON:
      g_value_set_object (value, roam_file_get_icon (self));
      break;

    case PROP_PATH:
      g_value_set_string (value, roam_file_get_path (self));
      break;

    case PROP_NAME:
      g_value_set_string (value, roam_file_get_name (self));
      break;

    case PROP_SIZE:
      g_value_set_uint64 (value, roam_file_get_size (self));
      break;

    case PROP_SYMBOLIC_ICON:
      g_value_set_object (value, roam_file_get_symbolic_icon (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
roam_file_set_property (GObject      *object,
                        guint         prop_id,
                        const GValue *value,
                        GParamSpec   *pspec)
{
  RoamFile *self = ROAM_FILE (object);

  switch (prop_id)
    {
    case PROP_ICON:
      roam_file_set_icon (self, g_value_get_object (value));
      break;

    case PROP_PATH:
      roam_file_set_path (self, g_value_get_string (value));
      break;

    case PROP_SIZE:
      roam_file_set_size (self, g_value_get_uint64 (value));
      break;

    case PROP_SYMBOLIC_ICON:
      roam_file_set_symbolic_icon (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
roam_file_class_init (RoamFileClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = roam_file_finalize;
  object_class->get_property = roam_file_get_property;
  object_class->set_property = roam_file_set_property;

  /**
   * RoamFile:content-type:
   *
   * The "content-type" of the file, if known.
   *
   * Since: 3.28
   */
  properties [PROP_CONTENT_TYPE] =
    g_param_spec_string ("content-type",
                         "Content Type",
                         "The content type of the file if known",
                         NULL,
                         G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);

  /**
   * RoamFile:icon:
   *
   * The "icon" is the #GIcon representing the file.
   *
   * Since: 3.28
   */
  properties [PROP_ICON] =
    g_param_spec_object ("icon",
                         "Icon",
                         "The icon for the file",
                         G_TYPE_ICON,
                         G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);

  /**
   * RoamFile:is-directory:
   *
   * The "is-directory" property indicates if the #RoamFile is a directory.
   *
   * Sicne: 3.28
   */
  properties [PROP_IS_DIRECTORY] =
    g_param_spec_boolean ("is-directory",
                          "Is Directory",
                          "If the file is a directory",
                          FALSE,
                          G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);

  /**
   * RoamFile:name:
   *
   * The "name" property is similar to the result of the POSIX
   * basename() function on a file path. It is the name of the
   * file within a directory (the last part of the path).
   *
   * Since: 3.28
   */
  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "The name of the file, similar to basename",
                         NULL,
                         G_PARAM_READABLE | G_PARAM_STATIC_STRINGS);

  /**
   * RoamFile:path:
   *
   * The "path" for the #RoamFile.
   *
   * This is a virtual path, meaning it does not exist at this path
   * on disk, but only at this path relative to a mount. It must
   * be resolved into a file system access or ntework access via
   * the included API.
   *
   * See roam_file_open_async() and others.
   *
   * Since: 3.28
   */
  properties [PROP_PATH] =
    g_param_spec_string ("path",
                         "Path",
                         "The virtual path for the file",
                         NULL,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  /**
   * RoamFile:size:
   *
   * The "size" property is the size of the underlying file, if known.
   *
   * Since: 3.28
   */
  properties [PROP_SIZE] =
    g_param_spec_uint64 ("size",
                         "Size",
                         "The size of the file, if known",
                         0, G_MAXUINT64, 0,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  /**
   * RoamFile:symbolic-icon:
   *
   * The "symbolic-icon" is the symbolic #GIcon representing the file.
   *
   * Since: 3.28
   */
  properties [PROP_SYMBOLIC_ICON] =
    g_param_spec_object ("symbolic-icon",
                         "Symbolic Icon",
                         "The symbolic icon for the file",
                         G_TYPE_ICON,
                         G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
roam_file_init (RoamFile *self)
{
  self->hold = 1;
}

/**
 * roam_file_get_name:
 * @self: a #RoamFile
 *
 * Gets the "name" property, which is the base name for the file.
 *
 * The resulting string is internal to the #RoamFile and should not be
 * modified or freed.
 *
 * Returns: (transfer none): a string containing the name of the file.
 *
 * Since: 3.28
 */
const gchar *
roam_file_get_name (RoamFile *self)
{
  g_return_val_if_fail (ROAM_IS_FILE (self), NULL);

  return roam_path_get_name (self->path);
}

/**
 * roam_file_get_path:
 * @self: a #RoamFile
 *
 * Gets the #RoamFile:path property.
 *
 * Returns: the virtual path of the file
 *
 * Since: 3.28
 */
const gchar *
roam_file_get_path (RoamFile *self)
{
  g_return_val_if_fail (ROAM_IS_FILE (self), NULL);

  return self->path;
}

/**
 * roam_file_new_for_path:
 * @path: the virtual path
 *
 * Creates a new #RoamFile using the virtual path provided.
 *
 * The virtual path may contain ../ but must be URI escaped. It will be
 * canonicalized as part of the process of creating the file.
 *
 * Returns: a newly created #RoamFile
 *
 * Since: 3.28
 */
RoamFile *
roam_file_new_for_path (const gchar *path)
{
  g_return_val_if_fail (path != NULL, NULL);

  return g_object_new (ROAM_TYPE_FILE,
                       "path", path,
                       NULL);
}

/**
 * roam_file_compare:
 * @a: a #RoamFile
 * @b: a #RoamFile
 *
 * A qsort style compare function for #RoamFile.
 *
 * Returns: -1 if @a is before b, 0 if equal, 1 if @a is after @b.
 */
gint
roam_file_compare (const RoamFile *a,
                   const RoamFile *b)
{
  g_autofree gchar *a_key = g_utf8_collate_key_for_filename (a->path, -1);
  g_autofree gchar *b_key = g_utf8_collate_key_for_filename (b->path, -1);

  return strcmp (a_key, b_key);
}

/**
 * roam_file_get_content_type:
 * @self: a #RoamFile
 *
 * Gets the content type for the file.
 *
 * Returns: (nullable): a content-type or %NULL
 *
 * Since: 3.28
 */
const gchar *
roam_file_get_content_type (RoamFile *self)
{
  g_return_val_if_fail (ROAM_IS_FILE (self), NULL);

  return self->content_type;
}

/**
 * roam_file_set_content_type:
 * @self: a #RoamFile
 * @content_type: (nullable): a content-type or %NULL
 *
 * Sets the content type for the file.
 *
 * This does not actually change the content-type on disk, only
 * what consumers of this #RoamFile instance will see.
 *
 * Since: 3.28
 */
void
roam_file_set_content_type (RoamFile    *self,
                            const gchar *content_type)
{
  g_return_if_fail (ROAM_IS_FILE (self));

  content_type = g_intern_string (content_type);

  if (self->content_type != content_type)
    {
      self->content_type = content_type;
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_CONTENT_TYPE]);
    }
}

/**
 * roam_file_get_icon:
 * @self: a #RoamFile
 *
 * Gets the #GIcon for the file.
 *
 * Returns: (transfer none): a #GIcon
 *
 * Since: 3.28
 */
GIcon *
roam_file_get_icon (RoamFile *self)
{
  g_return_val_if_fail (ROAM_IS_FILE (self), NULL);

  return self->icon;
}

/**
 * roam_file_set_icon:
 * @self: a #RoamFile
 * @icon: (nullable): a #GIcon or %NULL
 *
 * Sets the #GIcon for the #RoamFile instance.
 *
 * This does not change the icon set in extended-attributes, only
 * for the consumers of this #RoamFile instance.
 *
 * Since: 3.28
 */
void
roam_file_set_icon (RoamFile *self,
                    GIcon    *icon)
{
  g_return_if_fail (ROAM_IS_FILE (self));
  g_return_if_fail (!icon || G_IS_ICON (icon));

  if (g_set_object (&self->icon, icon))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ICON]);
}

/**
 * roam_file_get_symbolic_icon:
 * @self: a #RoamFile
 *
 * Gets the symbolic #GIcon for the file.
 *
 * Returns: (transfer none): a #GIcon
 *
 * Since: 3.28
 */
GIcon *
roam_file_get_symbolic_icon (RoamFile *self)
{
  g_return_val_if_fail (ROAM_IS_FILE (self), NULL);

  return self->symbolic_icon;
}

/**
 * roam_file_set_symbolic_icon:
 * @self: a #RoamFile
 * @icon: (nullable): a #GIcon or %NULL
 *
 * Sets the symbolic #GIcon for the #RoamFile instance.
 *
 * This does not change the icon set in extended-attributes, only
 * for the consumers of this #RoamFile instance.
 *
 * Since: 3.28
 */
void
roam_file_set_symbolic_icon (RoamFile *self,
                             GIcon    *symbolic_icon)
{
  g_return_if_fail (ROAM_IS_FILE (self));
  g_return_if_fail (!symbolic_icon || G_IS_ICON (symbolic_icon));

  if (g_set_object (&self->symbolic_icon, symbolic_icon))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_SYMBOLIC_ICON]);
}

/**
 * roam_file_get_size:
 * @self: a #RoamFile
 *
 * Gets the "size" property.
 *
 * This contains the size of the underlying file, if known.
 *
 * Returns: the size of the file, or 0 if not known
 *
 * Since: 3.28
 */
guint64
roam_file_get_size (RoamFile *self)
{
  g_return_val_if_fail (ROAM_IS_FILE (self), 0);

  return self->size;
}

/**
 * roam_file_set_size:
 * @self: a #RoamFile
 * @size: the size of the file
 *
 * Sets the "file" property.
 *
 * Since: 3.28
 */
void
roam_file_set_size (RoamFile *self,
                    guint64   size)
{
  g_return_if_fail (ROAM_IS_FILE (self));

  if (size != self->size)
    {
      self->size = size;
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_SIZE]);
    }
}

/**
 * roam_file_get_is_directory:
 * @self: a #RoamFile
 *
 * Gets the "is-directory" property. If this is %TRUE, then
 * you can list the children of this directory.
 *
 * Returns: %TRUE if @self represents a directory
 *
 * Since: 3.28
 */
gboolean
roam_file_get_is_directory (RoamFile *self)
{
  g_return_val_if_fail (ROAM_IS_FILE (self), FALSE);

  return self->is_directory;
}

/**
 * roam_file_set_is_directory:
 * @self: a #RoamFile
 * @is_directory: if the file is a directory
 *
 * Sets the "is-directory" property.
 *
 * This is generally used by backends when creating #RoamFile instances.
 *
 * Since: 3.28
 */
void
roam_file_set_is_directory (RoamFile *self,
                            gboolean  is_directory)
{
  g_return_if_fail (ROAM_IS_FILE (self));

  is_directory = !!is_directory;

  if (self->is_directory != is_directory)
    {
      self->is_directory = is_directory;
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_IS_DIRECTORY]);
    }
}

/**
 * roam_file_to_variant:
 * @self: a #RoamFile
 *
 * Creates a variant representation of the file.
 *
 * Returns: (transfer full): a #GVariant describing the file
 *
 * Since: 3.28
 */
GVariant *
roam_file_to_variant (RoamFile *self)
{
  g_autofree gchar *icon = NULL;
  g_autofree gchar *symbolic_icon = NULL;
  const gchar *name;

  g_return_val_if_fail (ROAM_IS_FILE (self), NULL);

  name = roam_file_get_name (self);

  if (self->icon != NULL)
    icon = g_icon_to_string (self->icon);

  if (self->symbolic_icon != NULL)
    symbolic_icon = g_icon_to_string (self->symbolic_icon);

  return JSONRPC_MESSAGE_NEW (
    "name", JSONRPC_MESSAGE_PUT_STRING (name),
    "content-type", JSONRPC_MESSAGE_PUT_STRING (self->content_type),
    "size", JSONRPC_MESSAGE_PUT_INT64 (self->size),
    "is-dir", JSONRPC_MESSAGE_PUT_BOOLEAN (self->is_directory),
    "icon", JSONRPC_MESSAGE_PUT_STRING (icon),
    "symbolic-icon", JSONRPC_MESSAGE_PUT_STRING (symbolic_icon)
  );
}

/**
 * roam_file_hold:
 * @self: a #RoamFile
 *
 * Used to track how many peers are using a given file.
 *
 * Since: 3.28
 */
void
roam_file_hold (RoamFile *self)
{
  g_return_if_fail (ROAM_IS_FILE (self));
  g_return_if_fail (self->hold > 0);

  self->hold++;
}

/**
 * roam_file_release:
 * @self: a #RoamFile
 *
 * Releases a hold on the file. When the hold count reaches zero,
 * it should be reomved from the containing file list.
 *
 * Returns: %TRUE if the file should be discarded
 *
 * Since: 3.28
 */
gboolean
roam_file_release (RoamFile *self)
{
  g_return_val_if_fail (ROAM_IS_FILE (self), FALSE);
  g_return_val_if_fail (self->hold > 0, FALSE);

  self->hold--;

  return self->hold == 0;
}
