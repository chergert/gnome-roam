/* roam-file.h
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "roam-types.h"

G_BEGIN_DECLS

#define ROAM_TYPE_FILE (roam_file_get_type())

G_DECLARE_FINAL_TYPE (RoamFile, roam_file, ROAM, FILE, GObject)

RoamFile    *roam_file_new_for_path           (const gchar    *path);
GVariant    *roam_file_to_variant             (RoamFile       *self);
const gchar *roam_file_get_name               (RoamFile       *self);
const gchar *roam_file_get_path               (RoamFile       *self);
gint         roam_file_compare                (const RoamFile *a,
                                               const RoamFile *b);
const gchar *roam_file_get_content_type       (RoamFile       *self);
void         roam_file_set_content_type       (RoamFile       *self,
                                               const gchar    *content_type);
GIcon       *roam_file_get_icon               (RoamFile       *self);
void         roam_file_set_icon               (RoamFile       *self,
                                               GIcon          *icon);
GIcon       *roam_file_get_symbolic_icon      (RoamFile       *self);
void         roam_file_set_symbolic_icon      (RoamFile       *self,
                                               GIcon          *symbolic_icon);
guint64      roam_file_get_size               (RoamFile       *self);
void         roam_file_set_size               (RoamFile       *self,
                                               guint64         size);
gboolean     roam_file_get_is_directory       (RoamFile       *self);
void         roam_file_set_is_directory       (RoamFile       *self,
                                               gboolean        is_directory);
void         roam_file_hold                   (RoamFile       *self);
gboolean     roam_file_release                (RoamFile       *self);

G_END_DECLS
