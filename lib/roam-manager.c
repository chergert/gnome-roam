/* roam-manager.c
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "roam-manager"

#include <glib/gi18n.h>

#include "roam-discovery.h"
#include "roam-file-list.h"
#include "roam-file.h"
#include "roam-manager.h"
#include "roam-mount-list.h"
#include "roam-mount.h"
#include "roam-peer.h"
#include "roam-publisher.h"
#include "roam-tls-manager.h"

typedef struct
{
  /*
   * List of RoamDiscovery instances for tracking access to new
   * peers. We get notified of the peers showing up and leaving
   * in response to events known to the discovery.
   */
  GListStore *discoverers;

  /*
   * A list of our peers that we have to query for information
   * about mounts and files and such.
   */
  GListStore *peers;

  /*
   * A list of our publishers which are responsible for exporting
   * the local mounts to peers who are intended to access them.
   */
  GListStore *publishers;

  /*
   * A list of mounts that have been discovered by peers. We keep
   * them here and loaded so that it is easy for UI to track changes
   * to the available mounts.
   */
  RoamMountList *mounts;

  /*
   * State tracking to ensure we don't stomp over ourselves while
   * performing various asynchronous tasks. One of the
   * MANAGER_STATE_INITIAL..MANAGER_STATE_DISPOSED states.
   */
  gint state;
} RoamManagerPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (RoamManager, roam_manager, G_TYPE_OBJECT)

enum {
  MANAGER_STATE_INITIAL,
  MANAGER_STATE_STARTUP,
  MANAGER_STATE_RUNNING,
  MANAGER_STATE_SHUTDOWN,
  MANAGER_STATE_FAILED,
  MANAGER_STATE_DISPOSED,
};

enum {
  DISCOVERY_ADDED,
  DISCOVERY_REMOVED,
  MOUNT_ADDED,
  MOUNT_REMOVED,
  PEER_ADDED,
  PEER_REMOVED,
  N_SIGNALS
};

static guint signals [N_SIGNALS];

static void
roam_manager_peer_mount_added_cb (RoamManager *self,
                                  RoamMount   *mount,
                                  RoamPeer    *peer)
{
  RoamManagerPrivate *priv = roam_manager_get_instance_private (self);
  const gchar *mount_id;
  gboolean is_new;

  g_assert (ROAM_IS_MANAGER (self));
  g_assert (ROAM_IS_MOUNT (mount));
  g_assert (ROAM_IS_PEER (peer));

  mount_id = roam_mount_get_id (mount);
  is_new = roam_mount_list_get_by_id (priv->mounts, mount_id) == NULL;

  roam_mount_list_add (priv->mounts, mount);

  if (is_new)
    {
      /* Get the "toplevel" mount instead of the peer mount */
      mount = roam_mount_list_get_by_id (priv->mounts, mount_id);
      g_debug ("New mount %s", mount_id);
      g_signal_emit (self, signals [MOUNT_ADDED], 0, mount);
    }
}

static void
roam_manager_peer_mount_removed_cb (RoamManager *self,
                                    RoamMount   *mount,
                                    RoamPeer    *peer)
{
  RoamManagerPrivate *priv = roam_manager_get_instance_private (self);
  g_autoptr(RoamMount) toplevel = NULL;
  g_autofree gchar *mount_id = NULL;

  g_assert (ROAM_IS_MANAGER (self));
  g_assert (ROAM_IS_MOUNT (mount));
  g_assert (ROAM_IS_PEER (peer));

  mount_id = g_strdup (roam_mount_get_id (mount));

  /* Get the toplevel, which we might need for signal emission */
  toplevel = roam_mount_list_get_by_id (priv->mounts, mount_id);
  if (toplevel != NULL)
    g_object_ref (toplevel);

  roam_mount_list_remove (priv->mounts, mount);

  if (roam_mount_list_get_by_id (priv->mounts, mount_id) == NULL)
    {
      g_debug ("Mount removed %s", mount_id);
      g_signal_emit (self, signals [MOUNT_REMOVED], 0, toplevel);
    }
}

static void
roam_manager_real_peer_added (RoamManager   *self,
                              RoamDiscovery *discovery,
                              RoamPeer      *peer)
{
  RoamManagerPrivate *priv = roam_manager_get_instance_private (self);
  g_autoptr(GPtrArray) mounts = NULL;

  g_assert (ROAM_IS_MANAGER (self));
  g_assert (ROAM_IS_DISCOVERY (discovery));
  g_assert (ROAM_IS_PEER (peer));

  if (priv->peers == NULL)
    return;

  g_debug ("%s discovery added peer %s",
           G_OBJECT_TYPE_NAME (discovery),
           G_OBJECT_TYPE_NAME (peer));

  g_signal_connect_object (peer,
                           "mount-added",
                           G_CALLBACK (roam_manager_peer_mount_added_cb),
                           self,
                           G_CONNECT_SWAPPED);

  g_signal_connect_object (peer,
                           "mount-removed",
                           G_CALLBACK (roam_manager_peer_mount_removed_cb),
                           self,
                           G_CONNECT_SWAPPED);

  g_list_store_insert_sorted (priv->peers,
                              peer,
                              (GCompareDataFunc)roam_peer_compare,
                              NULL);

  mounts = roam_peer_get_mounts (peer);

  for (guint i = 0; i < mounts->len; i++)
    {
      RoamMount *mount = g_ptr_array_index (mounts, i);
      roam_manager_peer_mount_added_cb (self, mount, peer);
    }
}

static void
roam_manager_real_peer_removed (RoamManager   *self,
                                RoamDiscovery *discovery,
                                RoamPeer      *peer)
{
  RoamManagerPrivate *priv = roam_manager_get_instance_private (self);
  g_autoptr(GPtrArray) mounts = NULL;
  guint n_items;

  g_assert (ROAM_IS_MANAGER (self));
  g_assert (ROAM_IS_DISCOVERY (discovery));
  g_assert (ROAM_IS_PEER (peer));

  g_debug ("%s discovery removed peer %s",
           G_OBJECT_TYPE_NAME (discovery),
           G_OBJECT_TYPE_NAME (peer));

  if (priv->peers == NULL)
    return;

  mounts = roam_peer_get_mounts (peer);

  for (guint i = 0; i < mounts->len; i++)
    {
      RoamMount *mount = g_ptr_array_index (mounts, i);
      roam_manager_peer_mount_removed_cb (self, mount, peer);
    }

  n_items = g_list_model_get_n_items (G_LIST_MODEL (priv->peers));

  for (guint i = 0; i < n_items; i++)
    {
      g_autoptr(RoamPeer) item = g_list_model_get_item (G_LIST_MODEL (priv->peers), i);

      if (item == peer)
        {
          g_list_store_remove (priv->peers, i);
          return;
        }
    }
}

static void
roam_manager_set_state (RoamManager *self,
                        gint         state)
{
  RoamManagerPrivate *priv = roam_manager_get_instance_private (self);

  g_assert (ROAM_IS_MANAGER (self));
  g_assert (state > MANAGER_STATE_INITIAL);
  g_assert (state <= MANAGER_STATE_DISPOSED);

  if (state == MANAGER_STATE_FAILED || state == MANAGER_STATE_DISPOSED)
    {
      guint n_items = g_list_model_get_n_items (G_LIST_MODEL (priv->discoverers));

      for (guint i = 0; i < n_items; i++)
        {
          g_autoptr(RoamDiscovery) discovery = NULL;

          discovery = g_list_model_get_item (G_LIST_MODEL (priv->discoverers), i);

          g_signal_handlers_disconnect_by_func (discovery,
                                                G_CALLBACK (roam_manager_real_peer_added),
                                                self);
          g_signal_handlers_disconnect_by_func (discovery,
                                                G_CALLBACK (roam_manager_real_peer_removed),
                                                self);
        }

      g_list_store_remove_all (priv->discoverers);
    }

  priv->state = state;
}

static void
roam_manager_dispose (GObject *object)
{
  RoamManager *self = (RoamManager *)object;
  RoamManagerPrivate *priv = roam_manager_get_instance_private (self);

  g_assert (ROAM_IS_MANAGER (self));

  roam_manager_set_state (self, MANAGER_STATE_DISPOSED);

  g_clear_pointer (&priv->discoverers, g_ptr_array_unref);
  g_clear_object (&priv->peers);

  G_OBJECT_CLASS (roam_manager_parent_class)->dispose (object);
}

static void
roam_manager_class_init (RoamManagerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = roam_manager_dispose;

  klass->peer_added = roam_manager_real_peer_added;
  klass->peer_removed = roam_manager_real_peer_removed;

  signals [DISCOVERY_ADDED] =
    g_signal_new ("discovery-added",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (RoamManagerClass, discovery_added),
                  NULL, NULL, NULL,
                  G_TYPE_NONE, 1, ROAM_TYPE_DISCOVERY);

  signals [DISCOVERY_REMOVED] =
    g_signal_new ("discovery-removed",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (RoamManagerClass, discovery_removed),
                  NULL, NULL, NULL,
                  G_TYPE_NONE, 1, ROAM_TYPE_DISCOVERY);

  signals [MOUNT_ADDED] =
    g_signal_new ("mount-added",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (RoamManagerClass, mount_added),
                  NULL, NULL,
                  g_cclosure_marshal_VOID__OBJECT,
                  G_TYPE_NONE, 1, ROAM_TYPE_MOUNT);

  signals [MOUNT_REMOVED] =
    g_signal_new ("mount-removed",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (RoamManagerClass, mount_removed),
                  NULL, NULL,
                  g_cclosure_marshal_VOID__OBJECT,
                  G_TYPE_NONE, 1, ROAM_TYPE_MOUNT);

  signals [PEER_ADDED] =
    g_signal_new ("peer-added",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (RoamManagerClass, peer_added),
                  NULL, NULL, NULL,
                  G_TYPE_NONE, 2, ROAM_TYPE_DISCOVERY, ROAM_TYPE_PEER);

  signals [PEER_REMOVED] =
    g_signal_new ("peer-removed",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (RoamManagerClass, peer_removed),
                  NULL, NULL, NULL,
                  G_TYPE_NONE, 2, ROAM_TYPE_DISCOVERY, ROAM_TYPE_PEER);
}

static void
roam_manager_init (RoamManager *self)
{
  RoamManagerPrivate *priv = roam_manager_get_instance_private (self);

  priv->discoverers = g_list_store_new (ROAM_TYPE_DISCOVERY);
  priv->mounts = roam_mount_list_new ();
  priv->peers = g_list_store_new (ROAM_TYPE_PEER);
  priv->publishers = g_list_store_new (ROAM_TYPE_PUBLISHER);
}

RoamManager *
roam_manager_new (void)
{
  return g_object_new (ROAM_TYPE_MANAGER, NULL);
}

static void
roam_manager_startup_cb (GObject      *object,
                         GAsyncResult *result,
                         gpointer      user_data)
{
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;
  RoamManager *self;
  GPtrArray *items;

  g_assert (ROAM_IS_DISCOVERY (object) || ROAM_IS_PUBLISHER (object));
  g_assert (G_IS_ASYNC_RESULT (result));

  self = g_task_get_source_object (task);
  g_assert (ROAM_IS_MANAGER (self));

  items = g_task_get_task_data (task);
  g_assert (items != NULL);
  g_assert (items->len > 0);

  if (!g_ptr_array_remove (items, object))
    g_assert_not_reached ();

  if ((ROAM_IS_DISCOVERY (object) &&
       !roam_discovery_startup_finish (ROAM_DISCOVERY (object), result, &error)) ||
      (ROAM_IS_PUBLISHER (object) &&
       !roam_publisher_startup_finish (ROAM_PUBLISHER (object), result, &error)))
    {
      g_debug ("%s failed startup: %s",
               G_OBJECT_TYPE_NAME (object),
               error->message);

      if (!g_task_get_completed (task))
        {
          roam_manager_set_state (self, MANAGER_STATE_FAILED);
          g_task_return_error (task, g_steal_pointer (&error));
        }

      return;
    }

  if (items->len == 0)
    {
      roam_manager_set_state (self, MANAGER_STATE_RUNNING);
      g_task_return_boolean (task, TRUE);
    }
}

/**
 * roam_manager_startup_async:
 * @self: a #RoamManager
 * @cancellable: (nullable): a #GCancellable, or %NULL
 * @callback: (nullable): a callback to execute upon completion
 * @user_data: (nullable): user data for @callback
 *
 * Asynchronously requests the manager to startup (and any registered
 * discovery agents and peers). You probably want to call
 * roam_manager_add_discovery() before calling this function.
 *
 * @callback should call roam_manager_startup_finish() to get the
 * result of this function.
 *
 * Returns: %TRUE on success; %FALSE on failure and @error is set.
 *
 * Since: 3.28
 */
void
roam_manager_startup_async (RoamManager         *self,
                            GCancellable        *cancellable,
                            GAsyncReadyCallback  callback,
                            gpointer             user_data)
{
  RoamManagerPrivate *priv = roam_manager_get_instance_private (self);
  g_autoptr(GTask) task = NULL;
  g_autoptr(GPtrArray) copy = NULL;
  guint n_items;

  g_return_if_fail (ROAM_IS_MANAGER (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));
  g_return_if_fail (priv->state == MANAGER_STATE_INITIAL);

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_priority (task, G_PRIORITY_LOW);
  g_task_set_source_tag (task, roam_manager_startup_async);

  /*
   * Create an array of objects to startup. We can put both discovery
   * and publisher agents into this and we'll do the right thing based
   * on the type.
   */
  copy = g_ptr_array_new_with_free_func (g_object_unref);

  /* Copy discovery agents to a private array */
  n_items = g_list_model_get_n_items (G_LIST_MODEL (priv->discoverers));
  for (guint i = 0; i < n_items; i++)
    g_ptr_array_add (copy, g_list_model_get_item (G_LIST_MODEL (priv->discoverers), i));

  /* Copy publisher agents to a private array */
  n_items = g_list_model_get_n_items (G_LIST_MODEL (priv->publishers));
  for (guint i = 0; i < n_items; i++)
    g_ptr_array_add (copy, g_list_model_get_item (G_LIST_MODEL (priv->publishers), i));

  g_task_set_task_data (task, g_ptr_array_ref (copy), (GDestroyNotify)g_ptr_array_unref);

  for (guint i = 0; i < copy->len; i++)
    {
      GObject *item = g_ptr_array_index (copy, i);
      GParamSpec *pspec;

      /* Set the manager instance if the object has one */
      pspec = g_object_class_find_property (G_OBJECT_GET_CLASS (item), "manager");
      if (pspec != NULL &&
          (pspec->flags & G_PARAM_WRITABLE) != 0 &&
          (pspec->flags & G_PARAM_CONSTRUCT_ONLY) == 0 &&
          g_type_is_a (pspec->value_type, ROAM_TYPE_MANAGER))
        g_object_set (item, "manager", self, NULL);

      if (ROAM_IS_DISCOVERY (item))
        roam_discovery_startup_async (ROAM_DISCOVERY (item),
                                      cancellable,
                                      roam_manager_startup_cb,
                                      g_object_ref (task));
      else if (ROAM_IS_PUBLISHER (item))
        roam_publisher_startup_async (ROAM_PUBLISHER (item),
                                      cancellable,
                                      roam_manager_startup_cb,
                                      g_object_ref (task));
      else
        g_assert_not_reached ();
    }

  roam_manager_set_state (self, MANAGER_STATE_STARTUP);

  /* If there was nothing to do, return success */
  if (copy->len == 0)
    {
      g_task_return_boolean (task, TRUE);
      roam_manager_set_state (self, MANAGER_STATE_RUNNING);
    }
}

/**
 * roam_manager_startup_finish:
 * @self: a #RoamManager
 * @result: a #GAsyncResult provided to the callback
 * @error: a location for a #GError, or %NULL
 *
 * Completes an asynchronous request to startup the manager
 * and any registered discovery agents, etc. You probably want
 * to call roam_manager_add_discovery() before calling this function
 * so that the manager is primed with a discovery agent up-front.
 *
 * Returns: %TRUE on success; %FALSE on failure and @error is set.
 *
 * Since: 3.28
 */
gboolean
roam_manager_startup_finish (RoamManager   *self,
                             GAsyncResult  *result,
                             GError       **error)
{
  g_return_val_if_fail (ROAM_IS_MANAGER (self), FALSE);
  g_return_val_if_fail (G_IS_TASK (result), FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
roam_manager_shutdown_cb (GObject      *object,
                          GAsyncResult *result,
                          gpointer      user_data)
{
  RoamDiscovery *discovery = (RoamDiscovery *)object;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;
  RoamManager *self;
  GPtrArray *discoverers;

  g_assert (ROAM_IS_DISCOVERY (discovery));
  g_assert (G_IS_ASYNC_RESULT (result));

  self = g_task_get_source_object (task);
  g_assert (ROAM_IS_MANAGER (self));

  discoverers = g_task_get_task_data (task);
  g_ptr_array_remove (discoverers, discovery);

  if (!roam_discovery_shutdown_finish (discovery, result, &error))
    {
      if (!g_task_get_completed (task))
        {
          roam_manager_set_state (self, MANAGER_STATE_FAILED);
          g_task_return_error (task, g_steal_pointer (&error));
        }
      return;
    }

  if (discoverers->len == 0)
    {
      roam_manager_set_state (self, MANAGER_STATE_DISPOSED);
      g_task_return_boolean (task, TRUE);
    }
}

/**
 * roam_manager_shutdown_async:
 * @self: a #RoamManager
 * @cancellable: (nullable): a #GCancellable, or %NULL
 * @callback: (nullable): a callback to execute upon completion
 * @user_data: (nullable): user data for @callback
 *
 * Attempts to shutdown the #RoamManager and release any resources
 * that are held by the manager. This will also request that any
 * aftive discovery agent also shutdown gracefully.
 *
 * @callback should call roam_manager_shutdown_finish() to get the
 * result of this asynchronous operation.
 *
 * Since: 3.28
 */
void
roam_manager_shutdown_async (RoamManager         *self,
                             GCancellable        *cancellable,
                             GAsyncReadyCallback  callback,
                             gpointer             user_data)
{
  RoamManagerPrivate *priv = roam_manager_get_instance_private (self);
  g_autoptr(GTask) task = NULL;
  g_autoptr(GPtrArray) copy = NULL;
  guint n_items;

  g_return_if_fail (ROAM_IS_MANAGER (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));
  g_return_if_fail (priv->state == MANAGER_STATE_RUNNING);

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_priority (task, G_PRIORITY_LOW);
  g_task_set_source_tag (task, roam_manager_shutdown_async);

  /* Copy each discovery agent to a private array */
  copy = g_ptr_array_new_with_free_func (g_object_unref);
  n_items = g_list_model_get_n_items (G_LIST_MODEL (priv->discoverers));
  for (guint i = 0; i < n_items; i++)
    g_ptr_array_add (copy, g_list_model_get_item (G_LIST_MODEL (priv->discoverers), i));

  g_task_set_task_data (task, g_ptr_array_ref (copy), (GDestroyNotify)g_ptr_array_unref);

  for (guint i = 0; i < copy->len; i++)
    {
      RoamDiscovery *discovery = g_ptr_array_index (copy, i);

      roam_discovery_shutdown_async (discovery,
                                     cancellable,
                                     roam_manager_shutdown_cb,
                                     g_object_ref (task));
    }

  if (copy->len == 0)
    {
      g_task_return_boolean (task, TRUE);
      roam_manager_set_state (self, MANAGER_STATE_DISPOSED);
      return;
    }

  roam_manager_set_state (self, MANAGER_STATE_SHUTDOWN);
}

/**
 * roam_manager_shutdown_finish:
 * @self: a #RoamManager
 * @result: a #GAsyncResult provided to the callback
 * @error: a location for a #GError, or %NULL
 *
 * Completes an asynchronous request to shutdown the manager
 * and any registered discovery agents, etc. This should be
 * called to ensure graceful shutdown of the process.
 *
 * Returns: %TRUE on success; %FALSE on failure and @error is set.
 *
 * Since: 3.28
 */
gboolean
roam_manager_shutdown_finish (RoamManager   *self,
                              GAsyncResult  *result,
                              GError       **error)
{
  g_return_val_if_fail (ROAM_IS_MANAGER (self), FALSE);
  g_return_val_if_fail (G_IS_TASK (result), FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
roam_manager_discovery_peer_added_cb (RoamManager   *self,
                                      RoamPeer      *peer,
                                      RoamDiscovery *discovery)
{
  g_assert (ROAM_IS_MANAGER (self));
  g_assert (ROAM_IS_PEER (peer));
  g_assert (ROAM_IS_DISCOVERY (discovery));

  g_signal_emit (self, signals [PEER_ADDED], 0, discovery, peer);
}

static void
roam_manager_discovery_peer_removed_cb (RoamManager   *self,
                                        RoamPeer      *peer,
                                        RoamDiscovery *discovery)
{
  g_assert (ROAM_IS_MANAGER (self));
  g_assert (ROAM_IS_PEER (peer));
  g_assert (ROAM_IS_DISCOVERY (discovery));

  g_signal_emit (self, signals [PEER_REMOVED], 0, discovery, peer);
}

/**
 * roam_manager_add_discovery:
 * @self: a #RoamManager
 * @discovery: a #RoamDiscovery
 *
 * This adds a new #RoamDiscovery to the list of discovery agents that
 * will be consulted by the #RoamManager.
 *
 * The discovery agents are responsible for forwarding the manager information
 * about peers on the network which are automatically discovered, or from a
 * source of peers which are known up-front.
 *
 * Since: 3.28
 */
void
roam_manager_add_discovery (RoamManager   *self,
                            RoamDiscovery *discovery)
{
  RoamManagerPrivate *priv = roam_manager_get_instance_private (self);
  g_autoptr(GPtrArray) peers = NULL;

  g_return_if_fail (ROAM_IS_MANAGER (self));
  g_return_if_fail (ROAM_IS_DISCOVERY (discovery));
  g_return_if_fail (priv->state == MANAGER_STATE_INITIAL ||
                    priv->state == MANAGER_STATE_STARTUP ||
                    priv->state == MANAGER_STATE_RUNNING);

  g_list_store_append (priv->discoverers, discovery);

  g_signal_connect_object (discovery,
                           "peer-added",
                           G_CALLBACK (roam_manager_discovery_peer_added_cb),
                           self,
                           G_CONNECT_SWAPPED);

  g_signal_connect_object (discovery,
                           "peer-removed",
                           G_CALLBACK (roam_manager_discovery_peer_removed_cb),
                           self,
                           G_CONNECT_SWAPPED);

  if (priv->state != MANAGER_STATE_INITIAL)
    roam_manager_startup_async (self, NULL, NULL, NULL);

  g_signal_emit (self, signals [DISCOVERY_ADDED], 0, discovery);

  peers = roam_discovery_get_peers (discovery);

  for (guint i = 0; i < peers->len; i++)
    {
      RoamPeer *peer = g_ptr_array_index (peers, i);

      g_assert (ROAM_IS_PEER (peer));

      roam_manager_discovery_peer_added_cb (self, peer, discovery);
    }
}

static void
roam_manager_remove_shutdown_cb (GObject      *object,
                                 GAsyncResult *result,
                                 gpointer      user_data)
{
  RoamDiscovery *discovery = (RoamDiscovery *)object;
  g_autoptr(RoamManager) self = user_data;
  g_autoptr(GError) error = NULL;

  g_assert (ROAM_IS_DISCOVERY (discovery));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (ROAM_IS_MANAGER (self));

  g_signal_handlers_disconnect_by_func (discovery,
                                        G_CALLBACK (roam_manager_discovery_peer_removed_cb),
                                        self);

  if (!roam_discovery_shutdown_finish (discovery, result, &error))
    {
      g_warning ("%s", error->message);
      return;
    }
}

void
roam_manager_remove_discovery (RoamManager   *self,
                               RoamDiscovery *discovery)
{
  RoamManagerPrivate *priv = roam_manager_get_instance_private (self);
  guint n_items;

  g_return_if_fail (ROAM_IS_MANAGER (self));
  g_return_if_fail (ROAM_IS_DISCOVERY (discovery));

  n_items = g_list_model_get_n_items (G_LIST_MODEL (priv->discoverers));

  for (guint i = 0; i < n_items; i++)
    {
      g_autoptr(RoamDiscovery) item = NULL;

      item = g_list_model_get_item (G_LIST_MODEL (priv->discoverers), i);

      if (item != discovery)
        continue;

      g_signal_handlers_disconnect_by_func (discovery,
                                            G_CALLBACK (roam_manager_discovery_peer_added_cb),
                                            self);

      g_list_store_remove (priv->discoverers, i);

      if (priv->state == MANAGER_STATE_RUNNING ||
          priv->state == MANAGER_STATE_STARTUP)
        roam_discovery_shutdown_async (item,
                                       NULL,
                                       roam_manager_remove_shutdown_cb,
                                       g_object_ref (self));
      else
        g_signal_handlers_disconnect_by_func (discovery,
                                              G_CALLBACK (roam_manager_discovery_peer_removed_cb),
                                              self);

      g_signal_emit (self, signals [DISCOVERY_REMOVED], 0, discovery);

      break;
    }
}

/**
 * roam_manager_get_peers:
 * @self: a #RoamManager
 *
 * Gets a #GListModel representing the peers. This list will be updated
 * as peers are added and removed from the manager.
 *
 * Returns: (transfer full): a #GListModel containing the peers that
 *   have been discovered.
 *
 * Since: 3.28
 */
GListModel *
roam_manager_get_peers (RoamManager *self)
{
  RoamManagerPrivate *priv = roam_manager_get_instance_private (self);

  g_return_val_if_fail (ROAM_IS_MANAGER (self), NULL);

  return g_object_ref (G_LIST_MODEL (priv->peers));
}

/**
 * roam_manager_get_discoverers:
 * @self: a #RoamManager
 *
 * Gets a #GListModel representing the discoverers. This list will be updated
 * as discoverers are added and removed from the manager.
 *
 * Returns: (transfer full): a #GListModel containing the discoverers that
 *   have been discovered.
 *
 * Since: 3.28
 */
GListModel *
roam_manager_get_discoverers (RoamManager *self)
{
  RoamManagerPrivate *priv = roam_manager_get_instance_private (self);

  g_return_val_if_fail (ROAM_IS_MANAGER (self), NULL);

  return g_object_ref (G_LIST_MODEL (priv->discoverers));
}

/**
 * roam_manager_get_mounts:
 * @self: a #RoamManager
 *
 * Gets a #GListModel containing the mounts discovered by the #RoamPeer
 *
 * Returns: (transfer full): a #GListModel of #RoamMount
 *
 * Since: 3.28
 */
GListModel *
roam_manager_get_mounts (RoamManager *self)
{
  RoamManagerPrivate *priv = roam_manager_get_instance_private (self);

  g_return_val_if_fail (ROAM_IS_MANAGER (self), NULL);

  return g_object_ref (G_LIST_MODEL (priv->mounts));
}

/**
 * roam_manager_get_publishers:
 * @self: a #RoamManager
 *
 * Gets a #GListModel that is updated as #RoamPublisher are added to
 * or removed from the manager.
 *
 * Returns: (transfer full): a #GListModel
 *
 * Since: 3.28
 */
GListModel *
roam_manager_get_publishers (RoamManager *self)
{
  RoamManagerPrivate *priv = roam_manager_get_instance_private (self);

  g_return_val_if_fail (ROAM_IS_MANAGER (self), NULL);

  return g_object_ref (G_LIST_MODEL (priv->publishers));
}

/**
 * roam_manager_add_publisher:
 * @self: a #RoamManager
 * @publisher: a #RoamPublisher
 *
 * Adds a #RoamPublisher to the #RoamManager.
 *
 * A publisher is reponsible for allowing a remote system
 * to access exported mounts.
 *
 * Since: 3.28
 */
void
roam_manager_add_publisher (RoamManager   *self,
                            RoamPublisher *publisher)
{
  RoamManagerPrivate *priv = roam_manager_get_instance_private (self);

  g_return_if_fail (ROAM_IS_MANAGER (self));
  g_return_if_fail (ROAM_IS_PUBLISHER (publisher));

  g_list_store_append (priv->publishers, publisher);
}

/**
 * roam_manager_remove_publisher:
 * @self: a #RoamManager
 * @publisher: a #RoamPublisher
 *
 * Removes a previously added #RoamManager.
 *
 * Since: 3.28
 */
void
roam_manager_remove_publisher (RoamManager   *self,
                               RoamPublisher *publisher)
{
  RoamManagerPrivate *priv = roam_manager_get_instance_private (self);
  guint n_items;

  g_return_if_fail (ROAM_IS_MANAGER (self));
  g_return_if_fail (ROAM_IS_PUBLISHER (publisher));

  n_items = g_list_model_get_n_items (G_LIST_MODEL (priv->publishers));

  for (guint i = 0; i < n_items; i++)
    {
      g_autoptr(RoamPublisher) item = NULL;

      item = g_list_model_get_item (G_LIST_MODEL (priv->publishers), i);

      if (item != publisher)
        {
          g_list_store_remove (priv->publishers, i);
          break;
        }
    }
}

/**
 * roam_manager_can_export:
 * @self: a #RoamManager
 * @credentials: the user credentials
 * @mount: a #RoamMount
 *
 * Checks if @credentials by a peer are sufficient to access @mount
 * or a shadow of the mount.
 *
 * Returns: %TRUE if @credentials are sufficient.
 *
 * Since: 3.28
 */
gboolean
roam_manager_can_export (RoamManager     *self,
                         RoamCredentials *credentials,
                         RoamMount       *mount)
{
  g_return_val_if_fail (ROAM_IS_MANAGER (self), FALSE);
  g_return_val_if_fail (ROAM_IS_MOUNT (mount), FALSE);

  if (!roam_mount_get_exportable (mount))
    return FALSE;

  /* TODO: Check credentials.
   *
   *       We also need the ability to determine which shadowed mounts
   *       may be exported to a peer based on their credentials.
   */

  return TRUE;
}
