/* roam-manager.h
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gio/gio.h>

#include "roam-types.h"

G_BEGIN_DECLS

#define ROAM_TYPE_MANAGER (roam_manager_get_type())

G_DECLARE_DERIVABLE_TYPE (RoamManager, roam_manager, ROAM, MANAGER, GObject)

struct _RoamManagerClass
{
  GObjectClass parent_class;

  void     (*discovery_added)   (RoamManager   *self,
                                 RoamDiscovery *discovery);
  void     (*discovery_removed) (RoamManager   *self,
                                 RoamDiscovery *discovery);
  void     (*mount_added)       (RoamManager   *self,
                                 RoamMount     *mount);
  void     (*mount_removed)     (RoamManager   *self,
                                 RoamMount     *mount);
  void     (*peer_added)        (RoamManager   *self,
                                 RoamDiscovery *discovery,
                                 RoamPeer      *peer);
  void     (*peer_removed)      (RoamManager   *self,
                                 RoamDiscovery *discovery,
                                 RoamPeer      *peer);
  gboolean (*can_export)        (RoamManager    *self,
                                 RoamMount      *mount);

  /*< private >*/
  gpointer _reserved[16];
};

RoamManager *roam_manager_new                (void);
void         roam_manager_add_discovery      (RoamManager          *self,
                                              RoamDiscovery        *discovery);
void         roam_manager_add_publisher      (RoamManager          *self,
                                              RoamPublisher        *publisher);
GListModel  *roam_manager_get_discoverers    (RoamManager          *self);
GListModel  *roam_manager_get_mounts         (RoamManager          *self);
GListModel  *roam_manager_get_peers          (RoamManager          *self);
gboolean     roam_manager_can_export         (RoamManager          *self,
                                              RoamCredentials      *credentials,
                                              RoamMount            *mount);
void         roam_manager_startup_async      (RoamManager          *self,
                                              GCancellable         *cancellable,
                                              GAsyncReadyCallback   callback,
                                              gpointer              user_data);
gboolean     roam_manager_startup_finish     (RoamManager          *self,
                                              GAsyncResult         *result,
                                              GError              **error);

G_END_DECLS
