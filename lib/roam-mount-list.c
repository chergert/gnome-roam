/* roam-mount-list.c
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "roam-mount-list"

#include "roam-mount.h"
#include "roam-mount-list.h"

struct _RoamMountList
{
  GObject       parent_instance;
  GSequence    *items;
  GCancellable *cancellable;
};

static void list_model_iface_init (GListModelInterface *iface);

G_DEFINE_TYPE_WITH_CODE (RoamMountList, roam_mount_list, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (G_TYPE_LIST_MODEL, list_model_iface_init))

static void
roam_mount_list_dispose (GObject *object)
{
  RoamMountList *self = (RoamMountList *)object;

  if (!g_cancellable_is_cancelled (self->cancellable))
    g_cancellable_cancel (self->cancellable);

  g_clear_pointer (&self->items, g_sequence_free);

  G_OBJECT_CLASS (roam_mount_list_parent_class)->dispose (object);
}

static void
roam_mount_list_class_init (RoamMountListClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = roam_mount_list_dispose;
}

static void
roam_mount_list_init (RoamMountList *self)
{
  self->cancellable = g_cancellable_new ();
  self->items = g_sequence_new (g_object_unref);
}

static GType
roam_mount_list_get_item_type (GListModel *model)
{
  return ROAM_TYPE_MOUNT;
}

static guint
roam_mount_list_get_n_items (GListModel *model)
{
  RoamMountList *self = (RoamMountList *)model;

  g_assert (ROAM_IS_MOUNT_LIST (self));

  if G_LIKELY (self->items != NULL)
    return g_sequence_get_length (self->items);

  return 0;
}

static gpointer
roam_mount_list_get_item (GListModel *model,
                          guint       position)
{
  RoamMountList *self = (RoamMountList *)model;

  g_assert (ROAM_IS_MOUNT_LIST (self));

  if G_LIKELY (self->items != NULL)
    {
      GSequenceIter *iter;

      g_assert (position < g_sequence_get_length (self->items));
      iter = g_sequence_get_iter_at_pos (self->items, position);
      g_assert (!g_sequence_iter_is_end (iter));

      return g_object_ref (g_sequence_get (iter));
    }

  return NULL;
}

static void
list_model_iface_init (GListModelInterface *iface)
{
  iface->get_item_type = roam_mount_list_get_item_type;
  iface->get_n_items = roam_mount_list_get_n_items;
  iface->get_item = roam_mount_list_get_item;
}

void
roam_mount_list_add (RoamMountList *self,
                     RoamMount     *mount)
{
  GSequenceIter *iter;

  g_return_if_fail (ROAM_IS_MOUNT_LIST (self));
  g_return_if_fail (ROAM_IS_MOUNT (mount));

  /*
   * We never add the mount directly to our sequence because we
   * want to simplify the case where multiple peers provide the
   * same mount. This is expected because lots of peers could be
   * exporting "Music", "Videos", etc.
   *
   * To deal with this, we insert a synthetic mount into our
   * sequence and then ask it to shadow the real mounts.
   */

  iter = g_sequence_lookup (self->items,
                            mount,
                            (GCompareDataFunc)roam_mount_compare,
                            NULL);

  if (iter == NULL)
    {
      g_autoptr(RoamMount) synthetic = NULL;

      /*
       * This is the first mount to be added with this "id". So create
       * a new synthetic mount and add it to the sequence.
       */

      synthetic = roam_mount_new (roam_mount_get_id (mount),
                                  roam_mount_get_name (mount),
                                  roam_mount_get_icon_name (mount),
                                  roam_mount_get_symbolic_icon_name (mount));
      roam_mount_shadow (synthetic, mount);
      iter = g_sequence_insert_sorted (self->items,
                                       g_steal_pointer (&synthetic),
                                       (GCompareDataFunc)roam_mount_compare,
                                       NULL);
      g_list_model_items_changed (G_LIST_MODEL (self),
                                  g_sequence_iter_get_position (iter),
                                  0, 1);
    }
  else
    {
      RoamMount *existing = g_sequence_get (iter);

      g_assert (ROAM_IS_MOUNT (existing));

      /*
       * The synthetic mount already exists, so add this new mount to the
       * list of shadowed mounts. We notify any listeners that the mount
       * changed with items-changed to remove/add the mount.
       */

      roam_mount_shadow (existing, mount);
      g_list_model_items_changed (G_LIST_MODEL (self),
                                  g_sequence_iter_get_position (iter),
                                  1, 1);
    }
}

/**
 * roam_mount_list_remove:
 * @self: a #RoamMountList
 * @mount: a #RoamMount
 *
 * Locates a #RoamMount and removes it.
 *
 * If @mount is the last mount providing a mount matching #RoamMount:id,
 * then the synethetic mount is also removed.
 *
 * Since: 3.28
 */
void
roam_mount_list_remove (RoamMountList *self,
                        RoamMount     *mount)
{
  GSequenceIter *iter;

  g_return_if_fail (ROAM_IS_MOUNT_LIST (self));
  g_return_if_fail (ROAM_IS_MOUNT (mount));

  iter = g_sequence_lookup (self->items,
                            mount,
                            (GCompareDataFunc)roam_mount_compare,
                            NULL);

  if (iter != NULL)
    {
      guint position = g_sequence_iter_get_position (iter);
      RoamMount *synthetic = g_sequence_get (iter);

      g_assert (ROAM_IS_MOUNT (synthetic));

      roam_mount_unshadow (synthetic, mount);

      if (!roam_mount_has_shadows (synthetic))
        {
          /* No more shadows left, remove this mount */
          g_sequence_remove (iter);
          g_list_model_items_changed (G_LIST_MODEL (self), position, 1, 0);
        }
    }
}

RoamMountList *
roam_mount_list_new (void)
{
  return g_object_new (ROAM_TYPE_MOUNT_LIST, NULL);
}

/**
 * roam_mount_list_get_cancellable:
 *
 * Gets the cancellable for the mount list. Peers use this to
 * cancel in-flight operations when the mount list is disposed.
 *
 * Returns: (transfer none): a #GCancellable
 *
 * Since: 3.28
 */
GCancellable *
roam_mount_list_get_cancellable (RoamMountList *self)
{
  g_return_val_if_fail (ROAM_IS_MOUNT_LIST (self), NULL);

  return self->cancellable;
}

void
roam_mount_list_cancel (RoamMountList *self)
{
  g_return_if_fail (ROAM_IS_MOUNT_LIST (self));

  g_cancellable_cancel (self->cancellable);
}

static gint
compare_by_id (gconstpointer a,
               gconstpointer b,
               gpointer      user_data)
{
  const gchar *id = user_data;

  g_assert (ROAM_IS_MOUNT ((gpointer)a));
  g_assert (b == NULL);
  g_assert (id != NULL);

  return g_strcmp0 (roam_mount_get_id ((RoamMount *)a), id);
}

/**
 * roam_mount_list_get_by_id:
 * @self: a #RoamMountList
 * @mount_id: the identifier of the intended mount
 *
 * Fins a #RoamMount that has a #RoamMount:id matching @id.
 *
 * Returns: (transfer none): a #RoamMount or %NULL if no match was found.
 *
 * Since: 3.28
 */
RoamMount *
roam_mount_list_get_by_id (RoamMountList *self,
                           const gchar   *id)
{
  GSequenceIter *iter;

  g_return_val_if_fail (ROAM_IS_MOUNT_LIST (self), NULL);
  g_return_val_if_fail (id != NULL, NULL);

  iter = g_sequence_lookup (self->items, NULL, compare_by_id, (gpointer)id);

  if (iter != NULL)
    return g_sequence_get (iter);

  return NULL;
}
