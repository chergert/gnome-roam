/* roam-mount-list.h
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gio/gio.h>

#include "roam-types.h"

G_BEGIN_DECLS

#define ROAM_TYPE_MOUNT_LIST (roam_mount_list_get_type())

G_DECLARE_FINAL_TYPE (RoamMountList, roam_mount_list, ROAM, MOUNT_LIST, GObject)

RoamMountList *roam_mount_list_new             (void);
void           roam_mount_list_add             (RoamMountList *self,
                                                RoamMount     *mount);
void           roam_mount_list_remove          (RoamMountList *self,
                                                RoamMount     *mount);
RoamMount     *roam_mount_list_get_by_id       (RoamMountList *self,
                                                const gchar   *id);
GCancellable  *roam_mount_list_get_cancellable (RoamMountList *self);
void           roam_mount_list_cancel          (RoamMountList *self);

G_END_DECLS
