/* roam-mount.c
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "roam-mount"

#include <jsonrpc-glib.h>
#include <string.h>

#include "roam-file.h"
#include "roam-file-list.h"
#include "roam-mount.h"
#include "roam-peer.h"
#include "roam-utils.h"

/**
 * SECTION:roam-mount
 * @title: RoamMount
 * @short_description: a virtual mount
 *
 * The #RoamMount instance is a virtual mount that is managed by
 * the #RoamManager. It contains files and directories that may be
 * backed by a number of data stores provided via discovered #RoamPeer.
 *
 * Since: 3.28
 */

typedef struct
{
  /* A unique identifier for the mount */
  gchar *id;

  /* User readable name of the mount */
  gchar *name;

  /*
   * The icons for the mount. These are intern'd strings since
   * it doesn't make much sense to keep string copies of icon
   * names which get used repeatedly.
   */
  const gchar *icon_name;
  const gchar *symbolic_icon_name;

  /*
   * An array of RoamPeer that have information for the given
   * mount. The peers are communicated with to access the information
   * in their stores, or to sync that information locally.
   */
  GPtrArray *peers;

  /*
   * Multiple peers may provide this mount, so when we do any operation,
   * we need to query the shadowed mounts. This is only used by the
   * "synthetic mount" created to hold all of the shadow mounts (as
   * created by the mount list when a mount is added).
   */
  GPtrArray *shadowed;

  /*
   * To avoid reloading things during mass-peer-removal, we stash the
   * RoamFileLists in a ptrarray. We reload them in a high-priority idle so
   * it happens immediately at the end of this main-loop cycle.
   */
  GPtrArray *reload_file_lists;
  guint reload_source;

  /*
   * If the mount is subject to export. This is only meant to let the
   * system know if the mount is capabile of being exported, not that
   * it should be exported. The default is FALSE.
   */
  guint exportable : 1;
} RoamMountPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (RoamMount, roam_mount, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_EXPORTABLE,
  PROP_ICON_NAME,
  PROP_ID,
  PROP_NAME,
  PROP_SYMBOLIC_ICON_NAME,
  N_PROPS
};

enum {
  CHANGED,
  N_SIGNALS
};

static GParamSpec *properties [N_PROPS];
static guint signals [N_SIGNALS];

static void
roam_mount_dispose (GObject *object)
{
  RoamMount *self = (RoamMount *)object;
  RoamMountPrivate *priv = roam_mount_get_instance_private (self);

  roam_clear_source (&priv->reload_source);

  g_clear_pointer (&priv->id, g_free);
  g_clear_pointer (&priv->name, g_free);
  g_clear_pointer (&priv->peers, g_ptr_array_unref);
  g_clear_pointer (&priv->shadowed, g_ptr_array_unref);
  g_clear_pointer (&priv->reload_file_lists, g_ptr_array_unref);

  priv->icon_name = NULL;
  priv->symbolic_icon_name = NULL;

  G_OBJECT_CLASS (roam_mount_parent_class)->dispose (object);
}

static void
roam_mount_get_property (GObject    *object,
                         guint       prop_id,
                         GValue     *value,
                         GParamSpec *pspec)
{
  RoamMount *self = ROAM_MOUNT (object);

  switch (prop_id)
    {
    case PROP_EXPORTABLE:
      g_value_set_boolean (value, roam_mount_get_exportable (self));
      break;

    case PROP_ID:
      g_value_set_string (value, roam_mount_get_id (self));
      break;

    case PROP_ICON_NAME:
      g_value_set_static_string (value, roam_mount_get_icon_name (self));
      break;

    case PROP_NAME:
      g_value_set_string (value, roam_mount_get_name (self));
      break;

    case PROP_SYMBOLIC_ICON_NAME:
      g_value_set_static_string (value, roam_mount_get_symbolic_icon_name (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
roam_mount_set_property (GObject      *object,
                         guint         prop_id,
                         const GValue *value,
                         GParamSpec   *pspec)
{
  RoamMount *self = ROAM_MOUNT (object);
  RoamMountPrivate *priv = roam_mount_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_EXPORTABLE:
      roam_mount_set_exportable (self, g_value_get_boolean (value));
      break;

    case PROP_ID:
      priv->id = g_value_dup_string (value);
      break;

    case PROP_ICON_NAME:
      priv->icon_name = g_intern_string (g_value_get_string (value));
      break;

    case PROP_NAME:
      priv->name = g_value_dup_string (value);
      break;

    case PROP_SYMBOLIC_ICON_NAME:
      priv->symbolic_icon_name = g_intern_string (g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
roam_mount_class_init (RoamMountClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = roam_mount_dispose;
  object_class->get_property = roam_mount_get_property;
  object_class->set_property = roam_mount_set_property;

  /**
   * RoamMount:exportable:
   *
   * The "exportable" property should be set to %TRUE if the peer
   * providing the mount is capable of being exported.
   *
   * Since: 3.28
   */
  properties [PROP_EXPORTABLE] =
    g_param_spec_boolean ("exportable",
                          "Exportable",
                          "If the mount is capable of exportation",
                          FALSE,
                          G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);

  /**
   * RoamMount:id:
   *
   * The "id" property for the #RoamMount is an identifier for
   * the mount. This allows the mount to be provided by multiple
   * peers which can be useful to merge data sets from multiple
   * sources.
   *
   * Since: 3.28
   */
  properties [PROP_ID] =
    g_param_spec_string ("id",
                         "Identifier",
                         "The identifier for the mount",
                         NULL,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  /**
   * RoamMount:name:
   *
   * The "name" property is the display name for the mount, such
   * as "Music" or "Videos".
   *
   * Since: 3.28
   */
  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "The name of the mount",
                         NULL,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  /**
   * RoamMount:icon-name:
   *
   * The "icon-name" for the mount.
   *
   * Since: 3.28
   */
  properties [PROP_ICON_NAME] =
    g_param_spec_string ("icon-name",
                         "Icon Name",
                         "The icon-name for the mount",
                         NULL,
                         G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  /**
   * RoamMount:symbolic-icon-name:
   *
   * The "symbolic-icon-name" for the mount.
   *
   * Since: 3.28
   */
  properties [PROP_SYMBOLIC_ICON_NAME] =
    g_param_spec_string ("symbolic-icon-name",
                         "Symbolic Icon Name",
                         "The symbolic icon-name for the mount",
                         NULL,
                         G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);

  /**
   * RoamMount::changed:
   *
   * The "changed" signal is emitted when a shadow mount has been added
   * or removed from the toplevel mount. It is not emitted on shadow mounts.
   *
   * This signal is useful to notify #RoamFileList instances that their
   * mount has changed, and therefore needs to be reload.
   *
   * Since: 3.28
   */
  signals [CHANGED] = g_signal_new ("changed",
                                    G_TYPE_FROM_CLASS (klass),
                                    G_SIGNAL_RUN_LAST,
                                    0,
                                    NULL, NULL,
                                    g_cclosure_marshal_VOID__VOID,
                                    G_TYPE_NONE, 0);
  g_signal_set_va_marshaller (signals [CHANGED], G_TYPE_FROM_CLASS (klass),
                              g_cclosure_marshal_VOID__VOIDv);
}

static void
roam_mount_init (RoamMount *self)
{
  RoamMountPrivate *priv = roam_mount_get_instance_private (self);

  priv->icon_name = g_intern_static_string ("folder");
  priv->symbolic_icon_name = g_intern_static_string ("folder-symbolic");
  priv->peers = g_ptr_array_new_with_free_func (g_object_unref);
  priv->shadowed = g_ptr_array_new_with_free_func (g_object_unref);
  priv->reload_file_lists = g_ptr_array_new_with_free_func (g_object_unref);
}

gboolean
roam_mount_get_exportable (RoamMount *self)
{
  RoamMountPrivate *priv = roam_mount_get_instance_private (self);

  g_return_val_if_fail (ROAM_IS_MOUNT (self), FALSE);

  for (guint i = 0; i < priv->shadowed->len; i++)
    {
      RoamMount *shadow = g_ptr_array_index (priv->shadowed, i);

      if (roam_mount_get_exportable (shadow))
        return TRUE;
    }

  return priv->exportable;
}

void
roam_mount_set_exportable (RoamMount *self,
                           gboolean   exportable)
{
  RoamMountPrivate *priv = roam_mount_get_instance_private (self);

  g_return_if_fail (ROAM_IS_MOUNT (self));

  exportable = !!exportable;

  if (priv->exportable != exportable)
    {
      priv->exportable = exportable;
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_EXPORTABLE]);
    }
}

/**
 * roam_mount_get_id:
 * @self: a #RoamMount
 *
 * The identifier of the mount.
 *
 * Returns: the identifier of the mount
 *
 * Since: 3.28
 */
const gchar *
roam_mount_get_id (RoamMount *self)
{
  RoamMountPrivate *priv = roam_mount_get_instance_private (self);

  g_return_val_if_fail (ROAM_IS_MOUNT (self), NULL);

  return priv->id;
}

/**
 * roam_mount_get_name:
 * @self: a #RoamMount
 *
 * Gets the name of the mount, such as "Music", or "Videos".
 *
 * Returns: (transfer full): a newly allocated mount name.
 *
 * Since: 3.28
 */
const gchar *
roam_mount_get_name (RoamMount *self)
{
  RoamMountPrivate *priv = roam_mount_get_instance_private (self);

  g_return_val_if_fail (ROAM_IS_MOUNT (self), NULL);

  return priv->name;
}

/**
 * roam_mount_get_icon_name:
 *
 * Gets the icon-name for the mount.
 *
 * Returns: An intern'd string containing the icon name.
 *
 * Since: 3.28
 */
const gchar *
roam_mount_get_icon_name (RoamMount *self)
{
  RoamMountPrivate *priv = roam_mount_get_instance_private (self);

  g_return_val_if_fail (ROAM_IS_MOUNT (self), NULL);

  return priv->icon_name;
}

/**
 * roam_mount_get_symbolic_icon_name:
 *
 * Gets the symbolic-icon-name for the mount.
 *
 * Returns: An intern'd string containing the symbolic-icon-name.
 *
 * Since: 3.28
 */
const gchar *
roam_mount_get_symbolic_icon_name (RoamMount *self)
{
  RoamMountPrivate *priv = roam_mount_get_instance_private (self);

  g_return_val_if_fail (ROAM_IS_MOUNT (self), NULL);

  return priv->symbolic_icon_name;
}

static void
roam_mount_list_children_async_list_files_cb (GObject      *object,
                                              GAsyncResult *result,
                                              gpointer      user_data)
{
  RoamPeer *peer = (RoamPeer *)object;
  g_autoptr(GPtrArray) items = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;
  RoamFileList *file_list;
  guint *count;

  g_assert (ROAM_IS_PEER (peer));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  file_list = g_task_get_task_data (task);
  g_assert (ROAM_IS_FILE_LIST (file_list));
  roam_file_list_unmark_busy (file_list);

  count = g_object_get_data (G_OBJECT (task), "ACTIVE");
  g_assert (count != NULL);
  g_assert (*count > 0);
  (*count)--;

  items = roam_peer_list_files_finish (peer, result, &error);

  if (error != NULL)
    {
      if (!g_error_matches (error, G_IO_ERROR, G_IO_ERROR_NOT_FOUND))
        g_warning ("Failed to read files: %s", error->message);
      goto finish;
    }

  g_assert (items != NULL);

  for (guint i = 0; i < items->len; i++)
    {
      RoamFile *file = g_ptr_array_index (items, i);
      g_assert (ROAM_IS_FILE (file));
      roam_file_list_add (file_list, file);
    }

finish:
  if (*count == 0)
    g_task_return_pointer (task, g_object_ref (file_list), g_object_unref);
}

/**
 * roam_mount_list_children_async:
 * @self: a #RoamMount
 * @path: (nullable): the path within the mount
 * @cancellable: (nullable): a #GCancellable, or %NULL
 * @callback: (nullable): a #GAsyncReadyCallback or %NULL
 * @user_data: closure data for @callback
 *
 * Asynchronously requests the children of @path on given mount.
 *
 * When this operation completes, @callback will be executed and it
 * must call roam_mount_list_children_finish() to get the result of
 * this operation.
 *
 * It is not guarnateed that all children will be known when this
 * operation completes. The caller should monitor
 * #GListModel::items-changed to monitor items as they become available.
 *
 * Since: 3.28
 */
void
roam_mount_list_children_async (RoamMount            *self,
                                const gchar          *path,
                                GCancellable         *cancellable,
                                GAsyncReadyCallback   callback,
                                gpointer              user_data)
{
  RoamMountPrivate *priv = roam_mount_get_instance_private (self);
  g_autoptr(RoamFileList) file_list = NULL;
  g_autoptr(GTask) task = NULL;
  guint *count = 0;

  g_return_if_fail (ROAM_IS_MOUNT (self));
  g_return_if_fail (path == NULL || *path == '/');
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, roam_mount_list_children_async);
  g_task_set_priority (task, G_PRIORITY_LOW);

  if (path == NULL)
    path = "/";

  g_assert (path != NULL);
  g_assert (*path == '/');
  g_assert (priv->peers == NULL || priv->peers->len == 0);
  g_assert (priv->shadowed != NULL);

  file_list = roam_file_list_new (priv->id, path);
  g_task_set_task_data (task, g_object_ref (file_list), g_object_unref);

  count = g_new0 (guint, 1);
  g_object_set_data_full (G_OBJECT (task), "ACTIVE", count, g_free);

  for (guint i = 0; i < priv->shadowed->len; i++)
    {
      RoamMount *shadowed = g_ptr_array_index (priv->shadowed, i);
      RoamMountPrivate *shadowed_priv = roam_mount_get_instance_private (shadowed);

      g_assert (shadowed_priv->peers != NULL);

      for (guint j = 0; j < shadowed_priv->peers->len; j++)
        {
          RoamPeer *peer = g_ptr_array_index (shadowed_priv->peers, j);

          g_assert (ROAM_IS_PEER (peer));

          (*count)++;

          roam_file_list_mark_busy (file_list);
          roam_peer_list_files_async (peer,
                                      shadowed,
                                      path,
                                      roam_file_list_get_cancellable (file_list),
                                      roam_mount_list_children_async_list_files_cb,
                                      g_object_ref (task));
        }
    }

  if (*count == 0)
    g_task_return_pointer (task,
                           g_steal_pointer (&file_list),
                           g_object_unref);
}

/**
 * roam_mount_list_children_finish:
 * @self: a #RoamMount
 * @result: a #GAsyncResult provided to callback
 * @error: a location for a #GError, or %NULL.
 *
 * Completes an asynchronous request to list the children of a path.
 *
 * The result of this function is not guaranteed to have all known children
 * populated in the #GListModel. The caller should monitor
 * #GListModel::items-changed for updates the list of children.
 *
 * If the path requested does not exist, or was not discovered given
 * the current list of peers, then an error with a domain of %G_IO_ERROR
 * and code of %G_IO_ERROR_NOT_FOUND is returned.
 *
 * Returns: (transfer full): a #GListModel of #RoamFile if successful;
 *   otherwise %NULL and @error is set.
 *
 * Since: 3.28
 */
GListModel *
roam_mount_list_children_finish (RoamMount     *self,
                                 GAsyncResult  *result,
                                 GError       **error)
{
  g_return_val_if_fail (ROAM_IS_MOUNT (self), NULL);
  g_return_val_if_fail (G_IS_TASK (result), NULL);
  g_return_val_if_fail (g_task_is_valid (G_TASK (result), self), NULL);

  return g_task_propagate_pointer (G_TASK (result), error);
}

static void
roam_mount_list_children_list_files_cb (GObject      *object,
                                        GAsyncResult *result,
                                        gpointer      user_data)
{
  RoamPeer *peer = (RoamPeer *)object;
  g_autoptr(RoamFileList) file_list = user_data;
  g_autoptr(GPtrArray) items = NULL;
  g_autoptr(GError) error = NULL;

  g_assert (ROAM_IS_PEER (peer));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (ROAM_IS_FILE_LIST (file_list));

  roam_file_list_unmark_busy (file_list);

  items = roam_peer_list_files_finish (peer, result, &error);

  if (error != NULL)
    {
      if (!g_error_matches (error, G_IO_ERROR, G_IO_ERROR_NOT_FOUND))
        g_warning ("Failed to read files: %s", error->message);
      return;
    }

  g_assert (items != NULL);

  for (guint i = 0; i < items->len; i++)
    {
      RoamFile *file = g_ptr_array_index (items, i);

      g_assert (ROAM_IS_FILE (file));

      roam_file_list_add (file_list, file);
    }
}

/**
 * roam_mount_list_children:
 * @self: a #RoamMount
 * @path: the path within the mount
 *
 * Creates a new #RoamFileList that will contain the files which are children
 * of @path.
 *
 * The resulting @RoamFileList will be updated with changes to the underlying
 * storage until roam_file_list_cancel() has been called or the #RoamFileList
 * has been disposed.
 *
 * Use the #GListModel interface to access items within the resulting
 * #RoamFileList.
 *
 * Returns: (transfer full): a #RoamFileList of #RoamFileInfo objects
 *
 * Since: 3.28
 */
RoamFileList *
roam_mount_list_children (RoamMount   *self,
                          const gchar *path)
{
  RoamMountPrivate *priv = roam_mount_get_instance_private (self);
  g_autoptr(RoamFileList) file_list = NULL;

  g_return_val_if_fail (ROAM_IS_MOUNT (self), NULL);
  g_return_val_if_fail (path == NULL || *path == '/', NULL);

  if (path == NULL)
    path = "/";

  g_assert (path != NULL);
  g_assert (*path == '/');
  g_assert (priv->peers == NULL || priv->peers->len == 0);
  g_assert (priv->shadowed != NULL);

  file_list = roam_file_list_new (priv->id, path);

  for (guint i = 0; i < priv->shadowed->len; i++)
    {
      RoamMount *shadowed = g_ptr_array_index (priv->shadowed, i);
      RoamMountPrivate *shadowed_priv = roam_mount_get_instance_private (shadowed);

      g_assert (shadowed_priv->peers != NULL);

      for (guint j = 0; j < shadowed_priv->peers->len; j++)
        {
          RoamPeer *peer = g_ptr_array_index (shadowed_priv->peers, j);

          g_assert (ROAM_IS_PEER (peer));

          roam_file_list_mark_busy (file_list);

          roam_peer_list_files_async (peer,
                                      shadowed,
                                      path,
                                      roam_file_list_get_cancellable (file_list),
                                      roam_mount_list_children_list_files_cb,
                                      g_object_ref (file_list));
        }
    }

  return g_steal_pointer (&file_list);
}

gint
roam_mount_compare (const RoamMount *a,
                    const RoamMount *b)
{
  RoamMountPrivate *a_priv = roam_mount_get_instance_private ((gpointer)a);
  RoamMountPrivate *b_priv = roam_mount_get_instance_private ((gpointer)b);

  g_assert (ROAM_IS_MOUNT ((gpointer)a));
  g_assert (ROAM_IS_MOUNT ((gpointer)b));

  if (!a_priv->name || !b_priv->name)
    return g_strcmp0 (a_priv->id, b_priv->id);

  return g_utf8_collate (a_priv->name, b_priv->name);
}

/**
 * roam_mount_new:
 *
 * Creates a new #RoamMount.
 *
 * Returns: (transfer full): a #RoamMount
 *
 * Since: 3.28
 */
RoamMount *
roam_mount_new (const gchar *id,
                const gchar *name,
                const gchar *icon_name,
                const gchar *symbolic_icon_name)
{
  g_return_val_if_fail (id != NULL, NULL);
  g_return_val_if_fail (name != NULL, NULL);

  return g_object_new (ROAM_TYPE_MOUNT,
                       "id", id,
                       "name", name,
                       "icon-name", icon_name,
                       "symbolic-icon-name", symbolic_icon_name,
                       NULL);
}

/**
 * roam_mount_add_peer:
 * @self: a #RoamMount
 * @peer: a #RoamPeer
 *
 * This attaches a peer to a mount so that it may be queried
 * for information related to the mount contents.
 *
 * Since: 3.28
 */
void
roam_mount_add_peer (RoamMount *self,
                     RoamPeer  *peer)
{
  RoamMountPrivate *priv = roam_mount_get_instance_private (self);
  guint pos;

  g_return_if_fail (ROAM_IS_MOUNT (self));
  g_return_if_fail (ROAM_IS_PEER (peer));

  /* TODO: We might want per-peer cancellables so we can
   *       cancel any in-flight operations.
   */

  if (!g_ptr_array_find (priv->peers, peer, &pos))
    g_ptr_array_add (priv->peers, g_object_ref (peer));
}

/**
 * roam_mount_remove_peer:
 * @self: a #RoamMount
 * @peer: a #RoamPeer
 *
 * Removes @peer from the list of peers providing information into
 * this particular mount.
 *
 * Since: 3.28
 */
void
roam_mount_remove_peer (RoamMount *self,
                        RoamPeer  *peer)
{
  RoamMountPrivate *priv = roam_mount_get_instance_private (self);

  g_return_if_fail (ROAM_IS_MOUNT (self));
  g_return_if_fail (ROAM_IS_PEER (peer));

  g_ptr_array_remove (priv->peers, peer);
}

static void
roam_mount_merge_info (RoamMount       *self,
                       const RoamMount *other)
{
  RoamMountPrivate *priv = roam_mount_get_instance_private (self);
  RoamMountPrivate *other_priv = roam_mount_get_instance_private ((gpointer)other);

  g_return_if_fail (ROAM_IS_MOUNT (self));
  g_return_if_fail (ROAM_IS_MOUNT ((gpointer)other));
  g_return_if_fail (g_strcmp0 (priv->id, other_priv->id) == 0);

  if (priv->icon_name == NULL)
    priv->icon_name = other_priv->icon_name;

  if (priv->symbolic_icon_name == NULL)
    priv->symbolic_icon_name = other_priv->symbolic_icon_name;

  if (priv->name == NULL)
    priv->name = g_strdup (other_priv->name);
}

/**
 * roam_mount_shadow:
 *
 * Multiple peers may provide the same mount. So allow them to hold private
 * data on their created #RoamMount instance, we keep all of the mounts
 * around and "shadow" the originals.
 *
 * This also allows the mount list to cleanup a mount when the last peer has
 * dropped it's mount (resulting in the UI cleaning up any newly removed
 * items).
 *
 * This function is, generally, only used by a #RoamMountList to join mounts
 * together into a single "virtual mount".
 *
 * Since: 3.28
 */
void
roam_mount_shadow (RoamMount *self,
                   RoamMount *shadowed)
{
  RoamMountPrivate *priv = roam_mount_get_instance_private (self);

  g_return_if_fail (ROAM_IS_MOUNT (self));
  g_return_if_fail (ROAM_IS_MOUNT (shadowed));
  g_return_if_fail (g_strcmp0 (roam_mount_get_id (self),
                               roam_mount_get_id (shadowed)) == 0);

  roam_mount_merge_info (self, shadowed);

  g_ptr_array_add (priv->shadowed, g_object_ref (shadowed));
}

/**
 * roam_mount_has_shadows:
 * @self: a #RoamMount
 *
 * Checks to see if the #RoamMount has any shadow mounts.
 *
 * Returns: %TRUE if @self has shadow mounts.
 *
 * Since: 3.28
 */
gboolean
roam_mount_has_shadows (RoamMount *self)
{
  RoamMountPrivate *priv = roam_mount_get_instance_private (self);

  g_return_val_if_fail (ROAM_IS_MOUNT (self), FALSE);

  return priv->shadowed != NULL && priv->shadowed->len > 0;
}

/**
 * roam_mount_unshadow:
 *
 * Unshadows the mount @shadow, which will drop the reference that is
 * owned by @self. This may also result in roam_mount_has_shadows()
 * returning %FALSE.
 *
 * Since: 3.28
 */
void
roam_mount_unshadow (RoamMount *self,
                     RoamMount *shadowed)
{
  RoamMountPrivate *priv = roam_mount_get_instance_private (self);

  g_return_if_fail (ROAM_IS_MOUNT (self));
  g_return_if_fail (ROAM_IS_MOUNT (shadowed));

  /*
   * Remove the shadow mount and only emit ::changed if this is not the
   * last mount to be removed. In the case of removing the last shadow,
   * we rely on the mount list to emit the ::items-changed signal to
   * cleanup the mount instead.
   */

  if (g_ptr_array_remove (priv->shadowed, shadowed) && priv->shadowed->len > 0)
    g_signal_emit (self, signals [CHANGED], 0);
}

/**
 * roam_mount_to_variant:
 * @self: a #RoamMount
 *
 * Creates a variant to describe the mount. This can be used by
 * the RPC server to proxy the mount information to a peer.
 *
 * Returns: (transfer full): a #GVariant
 *
 * Since: 3.28
 */
GVariant *
roam_mount_to_variant (RoamMount *self)
{
  RoamMountPrivate *priv = roam_mount_get_instance_private (self);

  g_return_val_if_fail (ROAM_IS_MOUNT (self), NULL);

  return JSONRPC_MESSAGE_NEW (
    "mount-id", JSONRPC_MESSAGE_PUT_STRING (priv->id),
    "name", JSONRPC_MESSAGE_PUT_STRING (priv->name),
    "icon-name", JSONRPC_MESSAGE_PUT_STRING (priv->icon_name),
    "symbolic-icon-name", JSONRPC_MESSAGE_PUT_STRING (priv->symbolic_icon_name)
  );
}
