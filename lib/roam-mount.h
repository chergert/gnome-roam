/* roam-mount.h
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "roam-types.h"

G_BEGIN_DECLS

#define ROAM_TYPE_MOUNT (roam_mount_get_type())

G_DECLARE_DERIVABLE_TYPE (RoamMount, roam_mount, ROAM, MOUNT, GObject)

struct _RoamMountClass
{
  GObjectClass parent_class;

  /*< private >*/
  gpointer _reserved[12];
};

RoamMount    *roam_mount_new                    (const gchar          *id,
                                                 const gchar          *name,
                                                 const gchar          *icon_name,
                                                 const gchar          *symbolic_icon_name);
const gchar  *roam_mount_get_id                 (RoamMount            *self);
const gchar  *roam_mount_get_name               (RoamMount            *self);
const gchar  *roam_mount_get_icon_name          (RoamMount            *self);
const gchar  *roam_mount_get_symbolic_icon_name (RoamMount            *self);
gboolean      roam_mount_get_exportable         (RoamMount            *self);
void          roam_mount_set_exportable         (RoamMount            *self,
                                                 gboolean              exportable);
void          roam_mount_list_children_async    (RoamMount            *self,
                                                 const gchar          *path,
                                                 GCancellable         *cancellable,
                                                 GAsyncReadyCallback   callback,
                                                 gpointer              user_data);
GListModel   *roam_mount_list_children_finish   (RoamMount            *self,
                                                 GAsyncResult         *result,
                                                 GError              **error);
RoamFileList *roam_mount_list_children          (RoamMount            *self,
                                                 const gchar          *path);
void          roam_mount_add_peer               (RoamMount            *self,
                                                 RoamPeer             *peer);
void          roam_mount_remove_peer            (RoamMount            *self,
                                                 RoamPeer             *peer);
gint          roam_mount_compare                (const RoamMount      *a,
                                                 const RoamMount      *b);
gboolean      roam_mount_has_shadows            (RoamMount            *self);
void          roam_mount_shadow                 (RoamMount            *self,
                                                 RoamMount            *shadowed);
void          roam_mount_unshadow               (RoamMount            *self,
                                                 RoamMount            *shadowed);
GVariant     *roam_mount_to_variant             (RoamMount            *self);

G_END_DECLS
