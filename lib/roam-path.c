/* roam-path.c
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "roam-path"

#include <string.h>

#include "roam-path.h"

static inline gchar *
scan_backwards (const gchar *begin,
                const gchar *end,
                gchar        c)
{
  while (end >= begin)
    {
      if (*end == c)
        return (gchar *)end;
      end--;
    }

  return NULL;
}

static inline void
pop_to_previous_part (const gchar  *begin,
                      gchar       **out)
{
  if (*out > begin)
    *out = scan_backwards (begin, *out - 1, '/');
}

/*
 * roam_path_canonicalize:
 * @in: the path to be canonicalized
 *
 * The path @in may contain non-canonical path pieces such as "../"
 * or duplicated "/". This will resolve those into a form that only
 * contains a single / at a time and resolves all "../". The resulting
 * path must also start with a /.
 *
 * Returns: the canonical form of the path
 */
gchar *
roam_path_canonicalize (const char *in)
{
  gchar *bptr;
  gchar *out;

  bptr = out = g_malloc (strlen (in) + 2);
  *out = '/';

  while (*in != 0)
    {
      g_assert (*out == '/');

      /* move past slashes */
      while (*in == '/')
        in++;

      /* Handle ./ ../ .\0 ..\0 */
      if (*in == '.')
        {
          /* If this is ../ or ..\0 move up */
          if (in[1] == '.' && (in[2] == '/' || in[2] == 0))
            {
              pop_to_previous_part (bptr, &out);
              in += 2;
              continue;
            }

          /* If this is ./ skip past it */
          if (in[1] == '/' || in[1] == 0)
            {
              in += 1;
              continue;
            }
        }

      /* Scan to the next path piece */
      while (*in != 0 && *in != '/')
        *(++out) = *(in++);

      /* Add trailing /, compress the rest on the next go round. */
      if (*in == '/')
        *(++out) = *(in++);
    }

  /* Trim trailing / from path */
  if (out > bptr && *out == '/')
    *out = 0;
  else
    *(++out) = 0;

  return bptr;
}

const gchar *
roam_path_get_name (const gchar *path)
{
  const gchar *slash;

  if (path == NULL)
    return NULL;

  /*
   * Our path is canonicalized, meaning it cannot have a trailing
   * slash, nor can it have multiple slashes and such. It also does
   * not have any ../ left in the path.
   *
   * That makes this fairly simple. Walk backwards from the end to
   * get the last /, or the name is the full path.
   */

  slash = strrchr (path, '/');

  return slash != NULL ? slash + 1 : path;
}

gboolean
roam_path_is_ignored (const gchar *name,
                      GFileType    file_type)
{
  if G_LIKELY (file_type == G_FILE_TYPE_REGULAR || file_type == G_FILE_TYPE_DIRECTORY)
    {
      return g_str_equal (name, "lost+found") ||
             g_str_equal (name, ".git") ||
             g_str_equal (name, ".gnome-roam");
    }

  return TRUE;
}
