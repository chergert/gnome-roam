/* roam-peer.c
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "roam-peer"

#include "roam-file.h"
#include "roam-file-list.h"
#include "roam-mount.h"
#include "roam-mount-list.h"
#include "roam-peer.h"

/**
 * SECTION:roam-peer
 * @title: RoamPeer
 * @short_description: a communication peer
 *
 * The #RoamPeer represents a peer that we can communicate with to
 * get data indexes and access file content.
 *
 * Since: 3.28
 */

typedef struct
{
  gchar *id;
  GPtrArray *mounts;
} RoamPeerPrivate;

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE (RoamPeer, roam_peer, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_ID,
  N_PROPS
};

enum {
  MOUNT_ADDED,
  MOUNT_REMOVED,
  N_SIGNALS
};

static GParamSpec *properties [N_PROPS];
static guint signals [N_SIGNALS];

static void
roam_peer_real_mount_added (RoamPeer  *self,
                            RoamMount *mount)
{
  RoamPeerPrivate *priv = roam_peer_get_instance_private (self);

  g_return_if_fail (ROAM_IS_PEER (self));
  g_return_if_fail (ROAM_IS_MOUNT (mount));

  g_ptr_array_add (priv->mounts, g_object_ref (mount));
}

static void
roam_peer_real_mount_removed (RoamPeer  *self,
                              RoamMount *mount)
{
  RoamPeerPrivate *priv = roam_peer_get_instance_private (self);

  g_return_if_fail (ROAM_IS_PEER (self));
  g_return_if_fail (ROAM_IS_MOUNT (mount));

  g_ptr_array_remove (priv->mounts, mount);
}

static void
roam_peer_real_list_files_async (RoamPeer            *self,
                                 RoamMount           *mount,
                                 const gchar         *peer,
                                 GCancellable        *cancellable,
                                 GAsyncReadyCallback  callback,
                                 gpointer             user_data)
{
  g_task_report_new_error (self, callback, user_data,
                           roam_peer_real_list_files_async,
                           G_IO_ERROR,
                           G_IO_ERROR_NOT_SUPPORTED,
                           "Listing files is not supported by this peer");
}

static GPtrArray *
roam_peer_real_list_files_finish (RoamPeer      *self,
                                  GAsyncResult  *result,
                                  GError       **error)
{
  return g_task_propagate_pointer (G_TASK (result), error);
}

static void
roam_peer_dispose (GObject *object)
{
  RoamPeer *self = (RoamPeer *)object;
  RoamPeerPrivate *priv = roam_peer_get_instance_private (self);

  g_clear_pointer (&priv->id, g_free);
  g_clear_pointer (&priv->mounts, g_ptr_array_unref);

  G_OBJECT_CLASS (roam_peer_parent_class)->dispose (object);
}

static void
roam_peer_get_property (GObject    *object,
                        guint       prop_id,
                        GValue     *value,
                        GParamSpec *pspec)
{
  RoamPeer *self = ROAM_PEER (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_set_string (value, roam_peer_get_id (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
roam_peer_set_property (GObject      *object,
                        guint         prop_id,
                        const GValue *value,
                        GParamSpec   *pspec)
{
  RoamPeer *self = ROAM_PEER (object);
  RoamPeerPrivate *priv = roam_peer_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_ID:
      priv->id = g_value_dup_string (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
roam_peer_class_init (RoamPeerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = roam_peer_dispose;
  object_class->get_property = roam_peer_get_property;
  object_class->set_property = roam_peer_set_property;

  klass->mount_added = roam_peer_real_mount_added;
  klass->mount_removed = roam_peer_real_mount_removed;
  klass->list_files_async = roam_peer_real_list_files_async;
  klass->list_files_finish = roam_peer_real_list_files_finish;

  /**
   * RoamPeer:id:
   *
   * The "id" property is the unique identifier for the peer.
   *
   * Since: 3.28
   */
  properties [PROP_ID] =
    g_param_spec_string ("id",
                         "Identifier",
                         "The identifier for the peer",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  /**
   * RoamPeer::mount-added:
   *
   * The "mount-added" signal is emitted when a #RoamPeer has discovered
   * a new mount provided by it's peer.
   *
   * #RoamPeer implementations should call roam_peer_emit_mount_added()
   * to emit this signal.
   *
   * Since: 3.28
   */
  signals [MOUNT_ADDED] =
    g_signal_new ("mount-added",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (RoamPeerClass, mount_added),
                  NULL, NULL,
                  g_cclosure_marshal_VOID__OBJECT,
                  G_TYPE_NONE, 1, ROAM_TYPE_MOUNT);
  g_signal_set_va_marshaller (signals [MOUNT_ADDED],
                              G_TYPE_FROM_CLASS (klass),
                              g_cclosure_marshal_VOID__OBJECTv);

  /**
   * RoamPeer::mount-removed:
   *
   * The "mount-removed" signal is emitted when a #RoamPeer has discovered
   * a mount has been removed by it's peer.
   *
   * #RoamPeer implementations should call roam_peer_emit_mount_removed()
   * to emit this signal.
   *
   * Since: 3.28
   */
  signals [MOUNT_REMOVED] =
    g_signal_new ("mount-removed",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (RoamPeerClass, mount_removed),
                  NULL, NULL,
                  g_cclosure_marshal_VOID__OBJECT,
                  G_TYPE_NONE, 1, ROAM_TYPE_MOUNT);
  g_signal_set_va_marshaller (signals [MOUNT_REMOVED],
                              G_TYPE_FROM_CLASS (klass),
                              g_cclosure_marshal_VOID__OBJECTv);
}

static void
roam_peer_init (RoamPeer *self)
{
  RoamPeerPrivate *priv = roam_peer_get_instance_private (self);

  priv->mounts = g_ptr_array_new_with_free_func (g_object_unref);
}

/**
 * roam_peer_get_id:
 *
 * Gets the "id" property. This is a unique identifier for the peer.
 *
 * Returns: (transfer none): the peer identifier
 *
 * Since: 3.28
 */
const gchar *
roam_peer_get_id (RoamPeer *self)
{
  RoamPeerPrivate *priv = roam_peer_get_instance_private (self);

  g_return_val_if_fail (ROAM_IS_PEER (self), NULL);

  return priv->id;
}

void
_roam_peer_populate_list (RoamPeer     *self,
                          RoamFileList *files)
{
  g_return_if_fail (ROAM_IS_PEER (self));
  g_return_if_fail (ROAM_IS_FILE_LIST (files));

}

gint
roam_peer_compare (const RoamPeer *a,
                   const RoamPeer *b)
{
  RoamPeerPrivate *a_priv = roam_peer_get_instance_private ((gpointer)a);
  RoamPeerPrivate *b_priv = roam_peer_get_instance_private ((gpointer)b);

  return g_strcmp0 (a_priv->id, b_priv->id);
}

/**
 * roam_peer_emit_mount_added:
 * self: a #RoamPeer
 * @mount: a #RoamMount owned by @peer
 *
 * Emits the #RoamPeer::mount-added signal.
 *
 * Since: 3.28
 */
void
roam_peer_emit_mount_added (RoamPeer  *self,
                            RoamMount *mount)
{
  g_return_if_fail (ROAM_IS_PEER (self));
  g_return_if_fail (ROAM_IS_MOUNT (mount));

  g_signal_emit (self, signals [MOUNT_ADDED], 0, mount);
}

/**
 * roam_peer_emit_mount_removed:
 * self: a #RoamPeer
 * @mount: a #RoamMount owned by @peer
 *
 * Emits the #RoamPeer::mount-removed signal.
 *
 * Since: 3.28
 */
void
roam_peer_emit_mount_removed (RoamPeer  *self,
                              RoamMount *mount)
{
  g_return_if_fail (ROAM_IS_PEER (self));
  g_return_if_fail (ROAM_IS_MOUNT (mount));

  g_signal_emit (self, signals [MOUNT_REMOVED], 0, mount);
}

/**
 * roam_peer_get_mounts:
 * @self: a #RoamPeer
 *
 * Gets the mounts that have been registered by this peer.
 *
 * #RoamPeer implementations must chain-up in #RoamPeer::mount-added
 * and #RoamPeer::mount-removed if they override the default handler.
 *
 * Returns: (transfer container): a #GPtrArray of #RoamMount
 *
 * Since: 3.28
 */
GPtrArray *
roam_peer_get_mounts (RoamPeer *self)
{
  RoamPeerPrivate *priv = roam_peer_get_instance_private (self);
  GPtrArray *copy;

  g_return_val_if_fail (ROAM_IS_PEER (self), NULL);

  copy = g_ptr_array_new_with_free_func (g_object_unref);
  for (guint i = 0; i < priv->mounts->len; i++)
    g_ptr_array_add (copy, g_object_ref (g_ptr_array_index (priv->mounts, i)));

  return copy;
}

/**
 * roam_peer_get_mount_by_id:
 * @self: a #RoamPeer
 * @mount_id: the id of the mount to lookup
 *
 * Looks for a given mount matching @mount_id.
 *
 * Returns: (transfer none): a #RoamMount
 *
 * Since: 3.28
 */
RoamMount *
roam_peer_get_mount_by_id (RoamPeer    *self,
                           const gchar *mount_id)
{
  RoamPeerPrivate *priv = roam_peer_get_instance_private (self);

  g_return_val_if_fail (ROAM_IS_PEER (self), NULL);
  g_return_val_if_fail (mount_id != NULL, NULL);

  if (priv->mounts == NULL)
    return NULL;

  for (guint i = 0; i < priv->mounts->len; i++)
    {
      RoamMount *mount = g_ptr_array_index (priv->mounts, i);

      if (g_strcmp0 (mount_id, roam_mount_get_id (mount)) == 0)
        return mount;
    }

  return NULL;
}

/**
 * roam_peer_list_files_async:
 * @self: a #RoamPeer
 * @mount: the mount to query
 * @path: the path within @mount
 * @cancellable: (nullable): a #GCancellable or %NULL
 * @callback: (nullable): callback to execute upon completion
 * @user_data: closure data for @callback
 *
 * Asynchronously requests that the #RoamPeer list the files found within
 * @mount at @path.
 *
 * If you want to keep track of updates, you should start a monitor before
 * calling this function to ensure you don't miss any files.
 *
 * Since: 3.28
 */
void
roam_peer_list_files_async (RoamPeer            *self,
                            RoamMount           *mount,
                            const gchar         *path,
                            GCancellable        *cancellable,
                            GAsyncReadyCallback  callback,
                            gpointer             user_data)
{
  g_return_if_fail (ROAM_IS_PEER (self));
  g_return_if_fail (ROAM_IS_MOUNT (mount));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  if (path == NULL)
    path = "/";

  ROAM_PEER_GET_CLASS (self)->list_files_async (self, mount, path, cancellable, callback, user_data);
}

/**
 * roam_peer_list_files_finish:
 * @self: a #RoamPeer
 * @result: a #GAsyncResult provided to callback
 * @error: a location for a #GError or %NULL
 *
 * Completes the asynchronous request to list files
 *
 * Returns: (transfer container) (element-type Roam.FileInfo): a list of
 *   #RoamFileInfo if successful; otherwise %NULL and @error is set.
 *
 * Since: 3.28
 */
GPtrArray *
roam_peer_list_files_finish (RoamPeer      *self,
                             GAsyncResult  *result,
                             GError       **error)
{
  g_return_val_if_fail (ROAM_IS_PEER (self), NULL);
  g_return_val_if_fail (G_IS_ASYNC_RESULT (result), NULL);

  return ROAM_PEER_GET_CLASS (self)->list_files_finish (self, result, error);
}

/**
 * roam_peer_monitor:
 * @self: a #RoamPeer
 * @mount: a #RoamMount
 * @path: (nullable): the path within the mount or %NULL for root
 * @callback: (nullable): a #RoamMonitorCallback
 * @user_data: closure data for @callback
 * @destroy: a #GDestroyNotify for @user_data
 *
 * Creates a monitor for @path of @mount.
 *
 * This can be used to get notifications of creation/deletion events
 * for a path. Such is useful to keep #GListModel such as #RoamFileList
 * up to date with underlying changes.
 *
 * Returns: a monitor id that may be removed with roam_peer_unmonitor().
 *
 * Since: 3.28
 */
guint
roam_peer_monitor (RoamPeer            *self,
                   RoamMount           *mount,
                   const gchar         *path,
                   RoamMonitorCallback  callback,
                   gpointer             user_data,
                   GDestroyNotify       destroy)
{
  g_return_val_if_fail (ROAM_IS_PEER (self), 0);
  g_return_val_if_fail (ROAM_IS_MOUNT (mount), 0);
  g_return_val_if_fail (callback != NULL, 0);

  if (path == NULL)
    path = "/";

  if (ROAM_PEER_GET_CLASS (self)->monitor)
    return ROAM_PEER_GET_CLASS (self)->monitor (self, mount, path, callback, user_data, destroy);

  return 0;
}

/**
 * roam_peer_unmonitor:
 * @self: a #RoamPeer
 * @monitor_id: the id of the monitor to remove
 *
 * Removes a monitor that has been previously created with
 * roam_peer_monitor().
 *
 * Since: 3.28
 */
void
roam_peer_unmonitor (RoamPeer *self,
                     guint     monitor_id)
{
  g_return_if_fail (ROAM_IS_PEER (self));
  g_return_if_fail (monitor_id > 0);

  return ROAM_PEER_GET_CLASS (self)->unmonitor (self, monitor_id);
}
