/* roam-peer.h
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "roam-types.h"

G_BEGIN_DECLS

#define ROAM_TYPE_PEER (roam_peer_get_type())

G_DECLARE_DERIVABLE_TYPE (RoamPeer, roam_peer, ROAM, PEER, GObject)

struct _RoamPeerClass
{
  GObjectClass parent;

  void       (*mount_added)       (RoamPeer             *self,
                                   RoamMount            *mount);
  void       (*mount_removed)     (RoamPeer             *self,
                                   RoamMount            *mount);
  void       (*list_files_async)  (RoamPeer             *self,
                                   RoamMount            *mount,
                                   const gchar          *path,
                                   GCancellable         *cancellable,
                                   GAsyncReadyCallback   callback,
                                   gpointer              user_data);
  GPtrArray *(*list_files_finish) (RoamPeer             *self,
                                   GAsyncResult         *result,
                                   GError              **error);
  guint      (*monitor)           (RoamPeer             *self,
                                   RoamMount            *mount,
                                   const gchar          *path,
                                   RoamMonitorCallback   callback,
                                   gpointer              user_data,
                                   GDestroyNotify        notify);
  void       (*unmonitor)         (RoamPeer             *self,
                                   guint                 monitor_id);

  /*< private >*/
  gpointer _reserved[16];
};

RoamPeer    *roam_peer_new                 (const gchar          *id);
const gchar *roam_peer_get_id              (RoamPeer             *self);
gint         roam_peer_compare             (const RoamPeer       *a,
                                            const RoamPeer       *b);
GPtrArray   *roam_peer_get_mounts          (RoamPeer             *self);
RoamMount   *roam_peer_get_mount_by_id     (RoamPeer             *self,
                                            const gchar          *mount_id);
void         roam_peer_emit_mount_added    (RoamPeer             *self,
                                            RoamMount            *mount);
void         roam_peer_emit_mount_removed  (RoamPeer             *self,
                                            RoamMount            *mount);
void         roam_peer_list_files_async    (RoamPeer             *self,
                                            RoamMount            *mount,
                                            const gchar          *path,
                                            GCancellable         *cancellable,
                                            GAsyncReadyCallback   callback,
                                            gpointer              user_data);
GPtrArray   *roam_peer_list_files_finish   (RoamPeer             *self,
                                            GAsyncResult         *result,
                                            GError              **error);
guint        roam_peer_monitor             (RoamPeer             *self,
                                            RoamMount            *mount,
                                            const gchar          *path,
                                            RoamMonitorCallback   callback,
                                            gpointer              user_data,
                                            GDestroyNotify        destroy);
void         roam_peer_unmonitor           (RoamPeer             *self,
                                            guint                 monitor_id);

G_END_DECLS
