/* roam-publisher.c
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "roam-publisher"

#include "roam-publisher.h"

/**
 * SECTION:roam-publisher
 * @title: RoamPublisher
 * @short_description: publishers of local data to peers
 *
 * The #RoamPublisher interface is responsible for exporting local mounts
 * to acceptable peers. For example, the #RoamPublisherAvahi subclass provides
 * access to mounts via a JSORPC server and mDNS advertisement.
 *
 * Since: 3.28
 */

typedef struct
{
  gchar *id;
} RoamPublisherPrivate;

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE (RoamPublisher, roam_publisher, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_ID,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
dummy_async (RoamPublisher       *self,
             GCancellable        *cancellable,
             GAsyncReadyCallback  callback,
             gpointer             user_data)
{
  g_task_report_new_error (self, callback, user_data, dummy_async,
                           G_IO_ERROR,
                           G_IO_ERROR_NOT_SUPPORTED,
                           "The operation is not supported");
}

static gboolean
dummy_finish (RoamPublisher  *self,
              GAsyncResult   *result,
              GError        **error)
{
  return g_task_propagate_boolean (G_TASK (self), error);
}

static void
roam_publisher_dispose (GObject *object)
{
  RoamPublisher *self = (RoamPublisher *)object;
  RoamPublisherPrivate *priv = roam_publisher_get_instance_private (self);

  g_clear_pointer (&priv->id, g_free);

  G_OBJECT_CLASS (roam_publisher_parent_class)->finalize (object);
}

static void
roam_publisher_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
  RoamPublisher *self = ROAM_PUBLISHER (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_set_string (value, roam_publisher_get_id (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
roam_publisher_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
  RoamPublisher *self = ROAM_PUBLISHER (object);
  RoamPublisherPrivate *priv = roam_publisher_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_ID:
      priv->id = g_value_dup_string (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
roam_publisher_class_init (RoamPublisherClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = roam_publisher_dispose;
  object_class->get_property = roam_publisher_get_property;
  object_class->set_property = roam_publisher_set_property;

  klass->startup_async = dummy_async;
  klass->startup_finish = dummy_finish;
  klass->shutdown_async = dummy_async;
  klass->shutdown_finish = dummy_finish;

  /**
   * RoamPublisher:id:
   *
   * The "id" for the publisher.
   *
   * Since: 3.28
   */
  properties [PROP_ID] =
    g_param_spec_string ("id",
                         "Identifier",
                         "The identifier for the publisher",
                         NULL,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
roam_publisher_init (RoamPublisher *self)
{
}

/**
 * roam_publisher_get_id:
 * @self: a #RoamPublisher
 *
 * Gets the identifier for the publisher.
 *
 * Returns: a string containing the identifier
 *
 * Since: 3.28
 */
const gchar *
roam_publisher_get_id (RoamPublisher *self)
{
  RoamPublisherPrivate *priv = roam_publisher_get_instance_private (self);

  g_return_val_if_fail (ROAM_IS_PUBLISHER (self), NULL);

  if (priv->id == NULL)
    return G_OBJECT_TYPE_NAME (self);

  return priv->id;
}

/**
 * roam_publisher_startup_async:
 * @self: a #RoamPublisher
 * @cancellable: (nullable): a #GCancellable, or %NULL
 * @callback: (nullable): a callback or %NULL
 * @user_data: closure data for @callback
 *
 * Requests that the publisher startup and begin listening on any
 * configured distribution channels.
 *
 * Since: 3.28
 */
void
roam_publisher_startup_async (RoamPublisher       *self,
                              GCancellable        *cancellable,
                              GAsyncReadyCallback  callback,
                              gpointer             user_data)
{
  g_return_if_fail (ROAM_IS_PUBLISHER (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  ROAM_PUBLISHER_GET_CLASS (self)->startup_async (self, cancellable, callback, user_data);
}

/**
 * roam_publisher_startup_finish:
 * @self: a #RoamPublisher
 * @result: a #GAsyncResult provided to callback
 * @error: a location for a #GError, or %NULL
 *
 * Completes an asynchronous request to roam_publisher_startup_async().
 *
 * Returns: %TRUE if startup was successful; otherwise %FALSE and @error is set.
 *
 * Since: 3.28
 */
gboolean
roam_publisher_startup_finish (RoamPublisher  *self,
                               GAsyncResult   *result,
                               GError        **error)
{
  g_return_val_if_fail (ROAM_IS_PUBLISHER (self), FALSE);
  g_return_val_if_fail (G_IS_ASYNC_RESULT (result), FALSE);

  return ROAM_PUBLISHER_GET_CLASS (self)->startup_finish (self, result, error);
}

void
roam_publisher_shutdown_async (RoamPublisher       *self,
                               GCancellable        *cancellable,
                               GAsyncReadyCallback  callback,
                               gpointer             user_data)
{
  g_return_if_fail (ROAM_IS_PUBLISHER (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  ROAM_PUBLISHER_GET_CLASS (self)->shutdown_async (self, cancellable, callback, user_data);
}

/**
 * roam_publisher_shutdown_finish:
 * @self: a #RoamPublisher
 * @result: a #GAsyncResult provided to callback
 * @error: a location for a #GError, or %NULL
 *
 * Completes an asynchronous request to roam_publisher_shutdown_async().
 *
 * Returns: %TRUE if shutdown was successful; otherwise %FALSE and @error is set.
 *
 * Since: 3.28
 */
gboolean
roam_publisher_shutdown_finish (RoamPublisher  *self,
                                GAsyncResult   *result,
                                GError        **error)
{
  g_return_val_if_fail (ROAM_IS_PUBLISHER (self), FALSE);
  g_return_val_if_fail (G_IS_ASYNC_RESULT (result), FALSE);

  return ROAM_PUBLISHER_GET_CLASS (self)->shutdown_finish (self, result, error);
}
