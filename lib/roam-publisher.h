/* roam-publisher.h
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gio/gio.h>

G_BEGIN_DECLS

#define ROAM_TYPE_PUBLISHER (roam_publisher_get_type())

G_DECLARE_DERIVABLE_TYPE (RoamPublisher, roam_publisher, ROAM, PUBLISHER, GObject)

struct _RoamPublisherClass
{
  GObjectClass parent;

  void     (*startup_async)   (RoamPublisher        *self,
                               GCancellable         *cancellable,
                               GAsyncReadyCallback   callback,
                               gpointer              user_data);
  gboolean (*startup_finish)  (RoamPublisher        *self,
                               GAsyncResult         *result,
                               GError              **error);
  void     (*shutdown_async)  (RoamPublisher        *self,
                               GCancellable         *cancellable,
                               GAsyncReadyCallback   callback,
                               gpointer              user_data);
  gboolean (*shutdown_finish) (RoamPublisher        *self,
                               GAsyncResult         *result,
                               GError              **error);

  /*< private >*/
  gpointer _reserved[12];
};

const gchar *roam_publisher_get_id          (RoamPublisher       *self);
void         roam_publisher_startup_async   (RoamPublisher       *self,
                                             GCancellable        *cancellable,
                                             GAsyncReadyCallback  callback,
                                             gpointer             user_data);
gboolean     roam_publisher_startup_finish  (RoamPublisher       *self,
                                             GAsyncResult        *result,
                                             GError             **error);
void         roam_publisher_shutdown_async  (RoamPublisher       *self,
                                             GCancellable        *cancellable,
                                             GAsyncReadyCallback  callback,
                                             gpointer             user_data);
gboolean     roam_publisher_shutdown_finish (RoamPublisher       *self,
                                             GAsyncResult        *result,
                                             GError             **error);

G_END_DECLS
