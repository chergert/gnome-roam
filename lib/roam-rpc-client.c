/* roam-rpc-client.c
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "roam-rpc-client"

#include <jsonrpc-glib.h>

#include "roam-rpc-client.h"
#include "roam-utils.h"

struct _RoamRpcClient
{
  GObject parent_instance;

  /*
   * This is the client we use to communicate with the peer. We create it
   * when handed our IO stream (we don't care if it's TCP/Unix/etc). We
   * connect to signals to get notifications and call methods on the peer
   * when appropriate.
   */
  JsonrpcClient *client;

  /*
   * A cancellable for operations, so that we can cancel everything
   * when we are disposed.
   */
  GCancellable *cancellable;

  /*
   * We keep the information on capabilities from our peer so that we
   * can query them later.
   */
  GVariant *capabilities;
};

static void async_initable_iface_init (GAsyncInitableIface *iface);

G_DEFINE_TYPE_WITH_CODE (RoamRpcClient, roam_rpc_client, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (G_TYPE_ASYNC_INITABLE, async_initable_iface_init))

enum {
  MOUNT_ADDED,
  MOUNT_REMOVED,
  N_SIGNALS
};

static guint signals [N_SIGNALS];

static void
roam_rpc_client_on_mount_added_cb (RoamRpcClient *self,
                                   const gchar   *method,
                                   GVariant      *params)
{
  const gchar *mount_id = NULL;
  gboolean success;

  g_assert (ROAM_IS_RPC_CLIENT (self));
  g_assert (method != NULL);
  g_assert (params != NULL);

  success = JSONRPC_MESSAGE_PARSE (params,
    "mount-id", JSONRPC_MESSAGE_GET_STRING (&mount_id)
  );

  if (success && mount_id != NULL)
    {
      g_debug ("Discovered new mount (%s)", mount_id);
      g_signal_emit (self, signals [MOUNT_ADDED], 0, params);
    }
}

static void
roam_rpc_client_on_mount_removed_cb (RoamRpcClient *self,
                                     const gchar   *method,
                                     GVariant      *params)
{
  const gchar *mount_id = NULL;
  gboolean success;

  g_assert (ROAM_IS_RPC_CLIENT (self));
  g_assert (method != NULL);
  g_assert (params != NULL);

  success = JSONRPC_MESSAGE_PARSE (params,
    "mount-id", JSONRPC_MESSAGE_GET_STRING (&mount_id)
  );

  if (success && mount_id != NULL)
    {
      g_debug ("Mount (%s) removed", mount_id);
      g_signal_emit (self, signals [MOUNT_REMOVED], 0, params);
    }
}

RoamRpcClient *
roam_rpc_client_new_for_stream (GIOStream *io_stream)
{
  g_autoptr(RoamRpcClient) self = NULL;

  g_return_val_if_fail (G_IS_IO_STREAM (io_stream), NULL);

  self = g_object_new (ROAM_TYPE_RPC_CLIENT, NULL);
  self->client = jsonrpc_client_new (io_stream);
  jsonrpc_client_set_use_gvariant (self->client, TRUE);

  g_signal_connect_object (self->client,
                           "notification::mount-added",
                           G_CALLBACK (roam_rpc_client_on_mount_added_cb),
                           self,
                           G_CONNECT_SWAPPED);

  g_signal_connect_object (self->client,
                           "notification::mount-removed",
                           G_CALLBACK (roam_rpc_client_on_mount_removed_cb),
                           self,
                           G_CONNECT_SWAPPED);

  return g_steal_pointer (&self);
}

static gboolean
roam_rpc_client_is_ready (RoamRpcClient  *self,
                          GError        **error)
{
  g_assert (ROAM_IS_RPC_CLIENT (self));

  if (self->client == NULL)
    {
      g_set_error (error,
                   G_IO_ERROR,
                   G_IO_ERROR_CLOSED,
                   "No client connection");
      return FALSE;
    }

  return TRUE;
}

static void
roam_rpc_client_dispose (GObject *object)
{
  RoamRpcClient *self = (RoamRpcClient *)object;

  g_cancellable_cancel (self->cancellable);
  g_clear_object (&self->cancellable);
  roam_dispose_object (&self->client);
  g_clear_pointer (&self->capabilities, g_variant_unref);

  G_OBJECT_CLASS (roam_rpc_client_parent_class)->dispose (object);
}

static void
roam_rpc_client_class_init (RoamRpcClientClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = roam_rpc_client_dispose;

  /**
   * RoamRpcClient::mount-added:
   * @self: a #RoamRpcClient
   * @info: a #GVariant of mount info
   *
   * The "mount-added" signal is emitted when a new mount has been discovered
   * by the #RoamRpcClient peer.
   *
   * Since: 3.28
   */
  signals [MOUNT_ADDED] =
    g_signal_new ("mount-added",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL,
                  g_cclosure_marshal_VOID__VARIANT,
                  G_TYPE_NONE,
                  1,
                  G_TYPE_VARIANT | G_SIGNAL_TYPE_STATIC_SCOPE);
  g_signal_set_va_marshaller (signals [MOUNT_ADDED],
                              G_TYPE_FROM_CLASS (klass),
                              g_cclosure_marshal_VOID__VARIANTv);

  /**
   * RoamRpcClient::mount-removed:
   * @self: a #RoamRpcClient
   * @info: a #GVariant of mount info
   *
   * The "mount-removed" signal is emitted when a mount has been removed
   * by the #RoamRpcClient peer.
   *
   * Since: 3.28
   */
  signals [MOUNT_REMOVED] =
    g_signal_new ("mount-removed",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL,
                  g_cclosure_marshal_VOID__VARIANT,
                  G_TYPE_NONE,
                  1,
                  G_TYPE_VARIANT | G_SIGNAL_TYPE_STATIC_SCOPE);
  g_signal_set_va_marshaller (signals [MOUNT_REMOVED],
                              G_TYPE_FROM_CLASS (klass),
                              g_cclosure_marshal_VOID__VARIANTv);
}

static void
roam_rpc_client_init (RoamRpcClient *self)
{
  self->cancellable = g_cancellable_new ();
}

static void
roam_rpc_client_initialize_cb (GObject      *object,
                               GAsyncResult *result,
                               gpointer      user_data)
{
  JsonrpcClient *client = (JsonrpcClient *)object;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GVariant) reply = NULL;
  RoamRpcClient *self;

  g_assert (JSONRPC_IS_CLIENT (client));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!jsonrpc_client_call_finish (client, result, &reply, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  /* Ensure our capabilities are reasonably sized */
  if (g_variant_get_size (reply) > 4096)
    {
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               G_IO_ERROR_MESSAGE_TOO_LARGE,
                               "Peer sent implausabile reply");
      return;
    }

  self = g_task_get_source_object (task);
  g_assert (ROAM_IS_RPC_CLIENT (self));

  g_clear_pointer (&self->capabilities, g_variant_unref);
  self->capabilities = g_steal_pointer (&reply);

  g_task_return_boolean (task, TRUE);
}

static void
roam_rpc_client_init_async (GAsyncInitable      *initable,
                            int                  io_priority,
                            GCancellable        *cancellable,
                            GAsyncReadyCallback  callback,
                            gpointer             user_data)
{
  RoamRpcClient *self = (RoamRpcClient *)initable;
  g_autoptr(GVariant) params = NULL;
  g_autoptr(GTask) task = NULL;

  g_assert (ROAM_IS_RPC_CLIENT (initable));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (initable, cancellable, callback, user_data);
  g_task_set_source_tag (task, roam_rpc_client_init_async);
  g_task_set_priority (task, io_priority);

  /* Make we were constructed correctly */
  if (self->client == NULL)
    {
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               G_IO_ERROR_INVAL,
                               "%s is not configured correctly",
                               G_OBJECT_TYPE_NAME (self));
      return;
    }

  /*
   * We need to initialize with the peer so that we know what capabiltiies
   * are supported and information about the peer.
   */
  params = JSONRPC_MESSAGE_NEW ("capabilities", "{", "}");

  jsonrpc_client_call_async (self->client, "initialize", params,
                             self->cancellable,
                             roam_rpc_client_initialize_cb,
                             g_steal_pointer (&task));
}

static gboolean
roam_rpc_client_init_finish (GAsyncInitable  *initable,
                             GAsyncResult    *result,
                             GError         **error)
{
  g_assert (ROAM_IS_RPC_CLIENT (initable));
  g_assert (G_IS_TASK (result));

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
async_initable_iface_init (GAsyncInitableIface *iface)
{
  iface->init_async = roam_rpc_client_init_async;
  iface->init_finish = roam_rpc_client_init_finish;
}

static void
roam_rpc_client_list_mounts_cb (GObject      *object,
                                GAsyncResult *result,
                                gpointer      user_data)
{
  JsonrpcClient *client = (JsonrpcClient *)object;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;
  g_autoptr(GVariant) reply = NULL;

  g_assert (JSONRPC_IS_CLIENT (client));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!jsonrpc_client_call_finish (client, result, &reply, &error))
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_pointer (task,
                           g_steal_pointer (&reply),
                           (GDestroyNotify)g_variant_unref);
}

/**
 * roam_rpc_client_list_mounts_async:
 * @self: a #RoamRpcClient
 * @cancellable: (nullable): a #GCancellable or %NULL
 * @callback: (nullable): a #GAsyncReadyCallback or %NULL
 * @user_data: closure data for @callback
 *
 * Asynchronous request to list the mounts available on the peer.
 *
 * You may also listen to the #RoamRpcClient::mount-added and
 * #RoamRpcClient::mount-removed signals for updates to the list.
 *
 * Since: 3.28
 */
void
roam_rpc_client_list_mounts_async (RoamRpcClient       *self,
                                   GCancellable        *cancellable,
                                   GAsyncReadyCallback  callback,
                                   gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;
  g_autoptr(GError) error = NULL;

  g_return_if_fail (ROAM_IS_RPC_CLIENT (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, roam_rpc_client_list_mounts_async);
  g_task_set_priority (task, G_PRIORITY_LOW);

  if (!roam_rpc_client_is_ready (self, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  jsonrpc_client_call_async (self->client, "list-mounts", NULL,
                             self->cancellable,
                             roam_rpc_client_list_mounts_cb,
                             g_steal_pointer (&task));
}

/**
 * roam_rpc_client_list_mounts_finish:
 * @self: a #RoamRpcClient
 * @result: a #GAsyncResult
 * @error: a location for a #GError, or %NULL
 *
 * Completes an asynchronous request to list the mounts available
 * on the peer.
 *
 * You may also listen to the #RoamRpcClient::mount-added and
 * #RoamRpcClient::mount-removed signals for updates to the list.
 *
 * Returns: (transfer full): a #GVariant containing an array of
 *   variant ("av") describing the available mounts.
 *
 * Since: 3.28
 */
GVariant *
roam_rpc_client_list_mounts_finish (RoamRpcClient  *self,
                                    GAsyncResult   *result,
                                    GError        **error)
{
  g_return_val_if_fail (ROAM_IS_RPC_CLIENT (self), NULL);
  g_return_val_if_fail (G_IS_TASK (result), NULL);
  g_return_val_if_fail (g_task_is_valid (G_TASK (result), self), NULL);

  return g_task_propagate_pointer (G_TASK (result), error);
}

static void
roam_rpc_client_list_files_cb (GObject      *object,
                               GAsyncResult *result,
                               gpointer      user_data)
{
  JsonrpcClient *client = (JsonrpcClient *)object;
  g_autoptr(GVariant) reply = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;

  g_assert (JSONRPC_IS_CLIENT (client));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!jsonrpc_client_call_finish (client, result, &reply, &error))
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_pointer (task,
                           g_steal_pointer (&reply),
                           (GDestroyNotify)g_variant_unref);
}

/**
 * roam_rpc_client_list_files_async:
 * @self: a #RoamRpcClient
 * @mount_id: the identifier for the mount
 * @path: (nullable): the path within the mount or %NULL for the root
 * @cancellable: (nullable): a #GCancellable or %NULL
 * @callback: (nullable): a #GAsyncReadyCallback or %NULL
 * @user_data: user data for @callback
 *
 * Asynchronously requests a list of files from the peer. To ensure that
 * you maintain an updated list of files, you may want to subscribe to updates
 * in the path using roam_rpc_client_monitor_files_async() so that
 * notifications for updated files are received.
 *
 * @callback should call roam_rpc_client_list_files_finish() to receive the
 * result of this asynchronous operation.
 *
 * Since: 3.28
 */
void
roam_rpc_client_list_files_async (RoamRpcClient       *self,
                                  const gchar         *mount_id,
                                  const gchar         *path,
                                  GCancellable        *cancellable,
                                  GAsyncReadyCallback  callback,
                                  gpointer             user_data)
{
  g_autoptr(GVariant) params = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = NULL;

  g_return_if_fail (ROAM_IS_RPC_CLIENT (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, roam_rpc_client_list_files_async);
  g_task_set_priority (task, G_PRIORITY_LOW);

  if (!roam_rpc_client_is_ready (self, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  params = JSONRPC_MESSAGE_NEW (
    "mount-id", JSONRPC_MESSAGE_PUT_STRING (mount_id),
    "path", JSONRPC_MESSAGE_PUT_STRING (path)
  );

  jsonrpc_client_call_async (self->client, "list-files", params,
                             self->cancellable,
                             roam_rpc_client_list_files_cb,
                             g_steal_pointer (&task));
}

/**
 * roam_rpc_client_list_files_finish:
 * @self: a #RoamRpcClient
 * @result: a #GAsyncResult
 * @error: a location for a #GError, or %NULL
 *
 * Completes an asynchronous request to roam_rpc_client_list_files_async().
 *
 * Returns: (transfer full): a #GVariant if successful; otherwise %NULL and
 *   @error is set.
 *
 * Since: 3.28
 */
GVariant *
roam_rpc_client_list_files_finish (RoamRpcClient  *self,
                                   GAsyncResult   *result,
                                   GError        **error)
{
  g_return_val_if_fail (ROAM_IS_RPC_CLIENT (self), NULL);
  g_return_val_if_fail (G_IS_TASK (result), NULL);
  g_return_val_if_fail (g_task_is_valid (G_TASK (result), self), NULL);

  return g_task_propagate_pointer (G_TASK (result), error);
}

static void
roam_rpc_client_monitor_cb (GObject      *object,
                            GAsyncResult *result,
                            gpointer      user_data)
{
  JsonrpcClient *client = (JsonrpcClient *)object;
  g_autoptr(GVariant) reply = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = user_data;
  gboolean success;
  gint32 monitor_id = 0;

  g_assert (JSONRPC_IS_CLIENT (client));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!jsonrpc_client_call_finish (client, result, &reply, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  success = JSONRPC_MESSAGE_PARSE (reply,
      "monitor_id", JSONRPC_MESSAGE_GET_INT32 (&monitor_id)
  );

  if (success == FALSE || monitor_id <= 0)
    {
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               G_IO_ERROR_FAILED,
                               "Unknown failure requesting monitor");
      return;
    }

  g_task_return_int (task, monitor_id);
}

void
roam_rpc_client_monitor_async (RoamRpcClient       *self,
                               const gchar         *mount_id,
                               const gchar         *path,
                               GCancellable        *cancellable,
                               GAsyncReadyCallback  callback,
                               gpointer             user_data)
{
  g_autoptr(GVariant) params = NULL;
  g_autoptr(GTask) task = NULL;
  g_autoptr(GError) error = NULL;

  g_return_if_fail (ROAM_IS_RPC_CLIENT (self));
  g_return_if_fail (mount_id != NULL);
  g_return_if_fail (path != NULL);
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, roam_rpc_client_monitor_async);
  g_task_set_priority (task, G_PRIORITY_LOW);

  if (!roam_rpc_client_is_ready (self, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  params = JSONRPC_MESSAGE_NEW (
    "mount-id", JSONRPC_MESSAGE_PUT_STRING (mount_id),
    "path", JSONRPC_MESSAGE_PUT_STRING (path)
  );

  jsonrpc_client_call_async (self->client, "monitor", params,
                             cancellable,
                             roam_rpc_client_monitor_cb,
                             g_steal_pointer (&task));
}

/**
 * roam_rpc_client_monitor_finish:
 * @self: a #RoamRpcClient
 * @result: a #GAsyncResult provided to callback
 * @error: a location for a #GError, or %NULL
 *
 * Completes an asynchronous request to monitor a directory for changes.
 *
 * Returns: a non-zero value containing the monitor id if successful,
 *   otherwise zero and @error is set.
 *
 * Since: 3.28
 */
guint
roam_rpc_client_monitor_finish (RoamRpcClient  *self,
                                GAsyncResult   *result,
                                GError        **error)
{
  g_return_val_if_fail (ROAM_IS_RPC_CLIENT (self), FALSE);
  g_return_val_if_fail (G_IS_TASK (result), FALSE);
  g_return_val_if_fail (g_task_is_valid (G_TASK (result), self), FALSE);

  return g_task_propagate_int (G_TASK (result), error);
}

static void
roam_rpc_client_unmonitor_cb (GObject      *object,
                              GAsyncResult *result,
                              gpointer      uesr_data)
{
  JsonrpcClient *client = (JsonrpcClient *)object;
  g_autoptr(GVariant) reply = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTask) task = NULL;

  g_assert (JSONRPC_IS_CLIENT (client));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!jsonrpc_client_call_finish (client, result, &reply, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  g_task_return_boolean (task, TRUE);
}

/**
 * roam_rpc_client_unmonitor_async:
 * @self: a #RoamRpcClient
 * @monitor_id: a previously registered monitor identifier
 * @cancellable: (nullable): a #GCancellable or %NULL
 * @callback: (nullable): a callback or %NULL
 * @user_data: closure data for @callback
 *
 * Asynchronously requests that the monitor known as @monitor_id be
 * cancelled and removed.
 *
 * When a client disconnects from a server, the service is responsible
 * for cleaning up any monitors held by the client.
 *
 * Also note that @monitor_id values are only unique per-client and
 * not globally.
 *
 * Since: 3.29
 */
void
roam_rpc_client_unmonitor_async (RoamRpcClient       *self,
                                 guint                monitor_id,
                                 GCancellable        *cancellable,
                                 GAsyncReadyCallback  callback,
                                 gpointer             user_data)
{
  g_autoptr(GVariant) params = NULL;
  g_autoptr(GTask) task = NULL;
  g_autoptr(GError) error = NULL;

  g_return_if_fail (ROAM_IS_RPC_CLIENT (self));
  g_return_if_fail (monitor_id > 0);
  g_return_if_fail (monitor_id <= G_MAXINT32);
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, roam_rpc_client_unmonitor_async);
  g_task_set_priority (task, G_PRIORITY_LOW);

  if (!roam_rpc_client_is_ready (self, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  params = JSONRPC_MESSAGE_NEW (
    "monitor_id", JSONRPC_MESSAGE_PUT_INT32 (monitor_id)
  );

  jsonrpc_client_call_async (self->client, "unmonitor", params,
                             cancellable,
                             roam_rpc_client_unmonitor_cb,
                             g_steal_pointer (&task));
}

/**
 * roam_rpc_client_unmonitor_finish:
 * @self: a #RoamRpcClient
 * @result: a #GAsyncResult provided to callback
 * @error: a location for a #GError, or %NULL
 *
 * Completes an asynchronous request to release a directory monitor
 * on a remove mount.
 *
 * Returns: %TRUE if the monitor was successfully removed. Otherwise, %FALSE
 *   and @error is set.
 *
 * Since: 3.28
 */
gboolean
roam_rpc_client_unmonitor_finish (RoamRpcClient  *self,
                                  GAsyncResult   *result,
                                  GError        **error)
{
  g_return_val_if_fail (ROAM_IS_RPC_CLIENT (self), FALSE);
  g_return_val_if_fail (G_IS_TASK (result), FALSE);
  g_return_val_if_fail (g_task_is_valid (G_TASK (result), self), FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}
