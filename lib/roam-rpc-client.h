/* roam-rpc-client.h
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gio/gio.h>

G_BEGIN_DECLS

#define ROAM_TYPE_RPC_CLIENT (roam_rpc_client_get_type())

G_DECLARE_FINAL_TYPE (RoamRpcClient, roam_rpc_client, ROAM, RPC_CLIENT, GObject)

RoamRpcClient *roam_rpc_client_new_for_stream      (GIOStream            *stream);
void           roam_rpc_client_list_mounts_async   (RoamRpcClient        *self,
                                                    GCancellable         *cancellable,
                                                    GAsyncReadyCallback   callback,
                                                    gpointer              user_data);
GVariant      *roam_rpc_client_list_mounts_finish  (RoamRpcClient        *self,
                                                    GAsyncResult         *result,
                                                    GError              **error);
void           roam_rpc_client_list_files_async    (RoamRpcClient        *self,
                                                    const gchar          *mount_id,
                                                    const gchar          *path,
                                                    GCancellable         *cancellable,
                                                    GAsyncReadyCallback   callback,
                                                    gpointer              user_data);
GVariant      *roam_rpc_client_list_files_finish   (RoamRpcClient        *self,
                                                    GAsyncResult         *result,
                                                    GError              **error);
void           roam_rpc_client_monitor_async       (RoamRpcClient        *self,
                                                    const gchar          *mount_id,
                                                    const gchar          *path,
                                                    GCancellable         *cancellable,
                                                    GAsyncReadyCallback   callback,
                                                    gpointer              user_data);
guint          roam_rpc_client_monitor_finish      (RoamRpcClient        *self,
                                                    GAsyncResult         *result,
                                                    GError              **error);
void           roam_rpc_client_unmonitor_async     (RoamRpcClient        *self,
                                                    guint                 monitor_id,
                                                    GCancellable         *cancellable,
                                                    GAsyncReadyCallback   callback,
                                                    gpointer              user_data);
gboolean       roam_rpc_client_unmonitor_finish    (RoamRpcClient        *self,
                                                    GAsyncResult         *result,
                                                    GError              **error);

G_END_DECLS
