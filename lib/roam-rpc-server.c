/* roam-rpc-server.c
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "roam-rpc-server"

#include <jsonrpc-glib.h>

#include "roam-file.h"
#include "roam-manager.h"
#include "roam-mount.h"
#include "roam-mount-list.h"
#include "roam-rpc-server.h"
#include "roam-utils.h"

struct _RoamRpcServer
{
  GSocketService parent_instance;

  /*
   * Our manager instance that the server is exporting. This is used
   * to locate available mounts and such.
   */
  RoamManager *manager;

  /*
   * Our JsonrpcService which manages the creation of new clients
   * for each of our incoming connections. We just hookup signals
   * to it, and then forward it streams.
   */
  JsonrpcServer *server;

  /*
   * The certificate is used to communicate with peers. If this has not
   * been set, we'll do things in clear text. Not ideal of course, but lets us
   * make progress until we get certificate generation working.
   */
  GTlsCertificate *certificate;
};

typedef struct
{
  JsonrpcClient *client;
  GVariant      *id;
} ListFiles;

G_DEFINE_TYPE (RoamRpcServer, roam_rpc_server, G_TYPE_SOCKET_SERVICE)

enum {
  PROP_0,
  PROP_CERTIFICATE,
  PROP_MANAGER,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
list_files_free (gpointer data)
{
  ListFiles *lf = data;

  g_clear_pointer (&lf->id, g_variant_unref);
  g_clear_object (&lf->client);
  g_slice_free (ListFiles, lf);
}

G_DEFINE_AUTOPTR_CLEANUP_FUNC (ListFiles, list_files_free)

static void
roam_rpc_server_mount_added_foreach (JsonrpcClient *client,
                                     GVariant      *mount_info)
{
  g_assert (JSONRPC_IS_CLIENT (client));
  g_assert (mount_info != NULL);

  jsonrpc_client_send_notification_async (client, "mount-added", mount_info,
                                          NULL, NULL, NULL);
}

static void
roam_rpc_server_mount_added_cb (RoamRpcServer *self,
                                RoamMount     *mount,
                                RoamManager   *manager)
{
  g_autoptr(GVariant) mount_info = NULL;

  g_assert (ROAM_IS_RPC_SERVER (self));
  g_assert (JSONRPC_IS_SERVER (self->server));
  g_assert (ROAM_IS_MOUNT (mount));
  g_assert (ROAM_IS_MANAGER (manager));

  mount_info = roam_mount_to_variant (mount);

  jsonrpc_server_foreach (self->server,
                          (GFunc) roam_rpc_server_mount_added_foreach,
                          mount_info);
}

static void
roam_rpc_server_mount_removed_foreach (JsonrpcClient *client,
                                       GVariant      *mount_info)
{
  g_assert (JSONRPC_IS_CLIENT (client));
  g_assert (mount_info != NULL);

  jsonrpc_client_send_notification_async (client, "mount-removed", mount_info, NULL, NULL, NULL);
}

static void
roam_rpc_server_mount_removed_cb (RoamRpcServer *self,
                                  RoamMount     *mount,
                                  RoamManager   *manager)
{
  g_autoptr(GVariant) mount_info = NULL;

  g_assert (ROAM_IS_RPC_SERVER (self));
  g_assert (ROAM_IS_MOUNT (mount));
  g_assert (ROAM_IS_MANAGER (manager));

  mount_info = roam_mount_to_variant (mount);

  jsonrpc_server_foreach (self->server,
                          (GFunc) roam_rpc_server_mount_removed_foreach,
                          mount_info);
}

static void
roam_rpc_server_set_manager (RoamRpcServer *self,
                             RoamManager   *manager)
{
  g_assert (ROAM_IS_RPC_SERVER (self));
  g_assert (ROAM_IS_MANAGER (manager));

  if (g_set_weak_pointer (&self->manager, manager))
    {
      g_autoptr(GListModel) mounts = NULL;
      guint n_items;

      g_signal_connect_object (manager,
                               "mount-added",
                               G_CALLBACK (roam_rpc_server_mount_added_cb),
                               self,
                               G_CONNECT_SWAPPED);

      g_signal_connect_object (manager,
                               "mount-removed",
                               G_CALLBACK (roam_rpc_server_mount_removed_cb),
                               self,
                               G_CONNECT_SWAPPED);

      mounts = roam_manager_get_mounts (manager);
      n_items = g_list_model_get_n_items (mounts);

      for (guint i = 0; i < n_items; i++)
        {
          g_autoptr(RoamMount) mount = g_list_model_get_item (mounts, i);

          roam_rpc_server_mount_added_cb (self, mount, manager);
        }
    }
}

static gboolean
roam_rpc_server_accept_certificate_cb (RoamRpcServer        *self,
                                       GTlsCertificate      *certificate,
                                       GTlsCertificateFlags  errors,
                                       GTlsConnection       *connection)
{
  g_assert (ROAM_IS_RPC_SERVER (self));
  g_assert (G_IS_TLS_CERTIFICATE (certificate));
  g_assert (G_IS_TLS_CONNECTION (connection));

  /* Allow any client cert for now */

  return TRUE;
}

static gboolean
roam_rpc_server_incoming (GSocketService    *service,
                          GSocketConnection *connection,
                          GObject           *source_object)
{
  RoamRpcServer *self = (RoamRpcServer *)service;
  g_autoptr(GIOStream) tls_stream = NULL;
  g_autoptr(GError) error = NULL;

  g_assert (ROAM_IS_RPC_SERVER (self));
  g_assert (G_IS_SOCKET_CONNECTION (connection));
  g_assert (self->server != NULL);

  /* We always require TLS */
  if (self->certificate == NULL)
    return FALSE;

  tls_stream = g_tls_server_connection_new (G_IO_STREAM (connection),
                                            self->certificate,
                                            &error);

  if (tls_stream == NULL)
    {
      g_warning ("Failed to create TLS server connection: %s", error->message);
      return FALSE;
    }

  g_signal_connect_object (tls_stream,
                           "accept-certificate",
                           G_CALLBACK (roam_rpc_server_accept_certificate_cb),
                           self,
                           G_CONNECT_SWAPPED);

  jsonrpc_server_accept_io_stream (self->server, tls_stream);

  return TRUE;
}

static void
roam_rpc_server_handle_initialize (JsonrpcServer *server,
                                   JsonrpcClient *client,
                                   const gchar   *method,
                                   GVariant      *id,
                                   GVariant      *params,
                                   gpointer       user_data)
{
  RoamRpcServer *self = user_data;
  g_autoptr(GVariant) reply = NULL;
  g_autoptr(GListModel) mounts = NULL;
  guint n_items;

  g_assert (ROAM_IS_RPC_SERVER (self));
  g_assert (JSONRPC_IS_CLIENT (client));
  g_assert (g_str_equal (method, "initialize"));
  g_assert (id != NULL);
  g_assert (JSONRPC_IS_SERVER (server));

  /*
   * Now emit mount-added signals for all of the mounts that have been
   * discovered. This allows the clients to simply calling initalize after
   * connecting to signals and we'll get their state updated.
   */
  mounts = roam_manager_get_mounts (self->manager);
  n_items = g_list_model_get_n_items (mounts);
  for (guint i = 0; i < n_items; i++)
    {
      g_autoptr(RoamMount) mount = NULL;
      g_autoptr(GVariant) mount_info = NULL;
      g_autoptr(GError) error = NULL;

      mount = g_list_model_get_item (mounts, i);
      mount_info = roam_mount_to_variant (mount);

      if (!jsonrpc_client_send_notification (client, "mount-added", mount_info, NULL, &error))
        g_warning ("%s", error->message);
    }

  /*
   * Send the reply last so that the peer is guaranteed to have received all of
   * the mount-added notifications before this reply.
   */
  reply = JSONRPC_MESSAGE_NEW (
    "peer-id", JSONRPC_MESSAGE_PUT_STRING (roam_get_peer_id ())
  );
  jsonrpc_client_reply_async (client, id, reply, NULL, NULL, NULL);
}

static void
roam_rpc_server_handle_list_mounts (JsonrpcServer *server,
                                    JsonrpcClient *client,
                                    const gchar   *method,
                                    GVariant      *id,
                                    GVariant      *params,
                                    gpointer       user_data)
{
  RoamRpcServer *self = user_data;
  g_autoptr(GVariant) reply = NULL;
  g_autoptr(GListModel) mounts = NULL;
  RoamCredentials *creds = NULL;
  guint n_items;

  g_assert (ROAM_IS_RPC_SERVER (self));
  g_assert (JSONRPC_IS_CLIENT (client));
  g_assert (g_str_equal (method, "list-mounts"));
  g_assert (id != NULL);
  g_assert (JSONRPC_IS_SERVER (server));

  mounts = roam_manager_get_mounts (self->manager);
  n_items = g_list_model_get_n_items (mounts);

  if (n_items > 0)
    {
      GVariantBuilder builder;

      g_variant_builder_init (&builder, G_VARIANT_TYPE ("aa{sv}"));

      for (guint i = 0; i < n_items; i++)
        {
          g_autoptr(RoamMount) mount = g_list_model_get_item (mounts, i);

          if (roam_manager_can_export (self->manager, creds, mount))
            {
              g_autoptr(GVariant) vmount = roam_mount_to_variant (mount);
              g_variant_builder_add_value (&builder, vmount);
            }
        }

      reply = g_variant_take_ref (g_variant_builder_end (&builder));
    }

  jsonrpc_client_reply_async (client, id, reply, NULL, NULL, NULL);
}

static void
roam_rpc_server_list_files_cb (GObject      *object,
                               GAsyncResult *result,
                               gpointer      user_data)
{
  RoamMount *mount = (RoamMount *)object;
  g_autoptr(ListFiles) lf = user_data;
  g_autoptr(GListModel) files = NULL;
  g_autoptr(GVariant) reply = NULL;
  g_autoptr(GError) error = NULL;
  guint n_items;

  g_assert (ROAM_IS_MOUNT (mount));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (lf != NULL);
  g_assert (JSONRPC_IS_CLIENT (lf->client));
  g_assert (lf->id != NULL);

  files = roam_mount_list_children_finish (mount, result, &error);

  if (error != NULL)
    {
      jsonrpc_client_reply_error_async (lf->client, lf->id,
                                        JSONRPC_CLIENT_ERROR_INTERNAL_ERROR,
                                        error->message,
                                        NULL, NULL, NULL);
      return;
    }

  n_items = g_list_model_get_n_items (files);

  if (n_items > 0)
    {
      GVariantBuilder builder;

      g_variant_builder_init (&builder, G_VARIANT_TYPE ("aa{sv}"));

      for (guint i = 0; i < n_items; i++)
        {
          g_autoptr(RoamFile) file = g_list_model_get_item (files, i);
          g_autoptr(GVariant) vfile = roam_file_to_variant (file);

          g_variant_builder_add_value (&builder, vfile);
        }

      reply = g_variant_take_ref (g_variant_builder_end (&builder));
    }

  jsonrpc_client_reply_async (lf->client, lf->id, reply, NULL, NULL, NULL);
}

static void
roam_rpc_server_handle_list_files (JsonrpcServer *server,
                                   JsonrpcClient *client,
                                   const gchar   *method,
                                   GVariant      *id,
                                   GVariant      *params,
                                   gpointer       user_data)
{
  RoamRpcServer *self = user_data;
  g_autoptr(GListModel) mounts = NULL;
  g_autoptr(ListFiles) info = NULL;
  const gchar *mount_id = NULL;
  const gchar *path = NULL;
  RoamMount *mount;
  gboolean success;

  g_assert (ROAM_IS_RPC_SERVER (self));
  g_assert (JSONRPC_IS_CLIENT (client));
  g_assert (g_str_equal (method, "list-files"));
  g_assert (id != NULL);
  g_assert (JSONRPC_IS_SERVER (server));

  success = JSONRPC_MESSAGE_PARSE (params,
    "mount-id", JSONRPC_MESSAGE_GET_STRING (&mount_id),
    "path", JSONRPC_MESSAGE_GET_STRING (&path)
  );

  if (!success)
    {
      jsonrpc_client_reply_error_async (client, id,
                                        JSONRPC_CLIENT_ERROR_INVALID_PARAMS,
                                        "Request requires mount-id and path params",
                                        NULL, NULL, NULL);
      return;
    }

  mounts = roam_manager_get_mounts (self->manager);
  mount = roam_mount_list_get_by_id (ROAM_MOUNT_LIST (mounts), mount_id);

  if (mount == NULL)
    {
      jsonrpc_client_reply_error_async (client, id,
                                        JSONRPC_CLIENT_ERROR_INVALID_REQUEST,
                                        "No such mount was found",
                                        NULL, NULL, NULL);
      return;
    }

  info = g_slice_new0 (ListFiles);
  info->id = g_variant_ref (id);
  info->client = g_object_ref (client);

  roam_mount_list_children_async (mount,
                                  path,
                                  NULL,
                                  roam_rpc_server_list_files_cb,
                                  g_steal_pointer (&info));
}

static void
roam_rpc_server_get_property (GObject    *object,
                              guint       prop_id,
                              GValue     *value,
                              GParamSpec *pspec)
{
  RoamRpcServer *self = ROAM_RPC_SERVER (object);

  switch (prop_id)
    {
    case PROP_CERTIFICATE:
      g_value_set_object (value, self->certificate);
      break;

    case PROP_MANAGER:
      g_value_set_object (value, self->manager);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
roam_rpc_server_set_property (GObject      *object,
                              guint         prop_id,
                              const GValue *value,
                              GParamSpec   *pspec)
{
  RoamRpcServer *self = ROAM_RPC_SERVER (object);

  switch (prop_id)
    {
    case PROP_CERTIFICATE:
      self->certificate = g_value_dup_object (value);
      break;

    case PROP_MANAGER:
      roam_rpc_server_set_manager (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
roam_rpc_server_dispose (GObject *object)
{
  RoamRpcServer *self = (RoamRpcServer *)object;

  roam_dispose_object (&self->server);
  g_clear_object (&self->certificate);

  G_OBJECT_CLASS (roam_rpc_server_parent_class)->dispose (object);
}

static void
roam_rpc_server_class_init (RoamRpcServerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GSocketServiceClass *socket_service_class = G_SOCKET_SERVICE_CLASS (klass);

  object_class->dispose = roam_rpc_server_dispose;
  object_class->get_property = roam_rpc_server_get_property;
  object_class->set_property = roam_rpc_server_set_property;

  socket_service_class->incoming = roam_rpc_server_incoming;

  /**
   * RoamRpcServer:certificate:
   *
   * The "certificate" property is the TLS certificate to use for
   * communication with peers.
   *
   * Since: 3.28
   */
  properties [PROP_CERTIFICATE] =
    g_param_spec_object ("certificate",
                         "Certificate",
                         "The TLS certificate to use for communication",
                         G_TYPE_TLS_CERTIFICATE,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  properties [PROP_MANAGER] =
    g_param_spec_object ("manager",
                         "Manager",
                         "The RoamManager backing the server",
                         ROAM_TYPE_MANAGER,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
roam_rpc_server_init (RoamRpcServer *self)
{
  self->server = jsonrpc_server_new ();

  jsonrpc_server_add_handler (self->server,
                              "initialize",
                              roam_rpc_server_handle_initialize,
                              self, NULL);

  jsonrpc_server_add_handler (self->server,
                              "list-mounts",
                              roam_rpc_server_handle_list_mounts,
                              self, NULL);

  jsonrpc_server_add_handler (self->server,
                              "list-files",
                              roam_rpc_server_handle_list_files,
                              self, NULL);
}

/**
 * roam_rpc_server_new:
 * @manager: a #RoamManager
 * @certificate: a #GTlsCertificate
 *
 * Creates a new #RoamRpcServer that communicates with peers using
 * TLS and the specified @certificate.
 *
 * This inherits from #GSocketService and #GSocketListener, so you may
 * use those APIs to add socket listeners and start processing messages.
 *
 * Returns: (transfer full): a #RoamRpcServer
 *
 * Since: 3.28
 */
RoamRpcServer *
roam_rpc_server_new (RoamManager     *manager,
                     GTlsCertificate *certificate)
{
  g_return_val_if_fail (ROAM_IS_MANAGER (manager), NULL);
  g_return_val_if_fail (G_IS_TLS_CERTIFICATE (certificate), NULL);

  return g_object_new (ROAM_TYPE_RPC_SERVER,
                       "manager", manager,
                       "certificate", certificate,
                       NULL);
}
