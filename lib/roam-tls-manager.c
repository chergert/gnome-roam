/* roam-tls-manager.c
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "roam-tls-manager"

#include <errno.h>
#include <openssl/bio.h>
#include <openssl/pem.h>
#include <openssl/x509.h>

#include "roam-tls-manager.h"

#define DEFAULT_KEYSIZE 4096

struct _RoamTlsManager
{
  GObject          parent_instance;

  GTlsCertificate *certificate;
  const gchar     *public_key_file;
  const gchar     *private_key_file;
  GQueue           queued_tasks;
  guint            building_cert : 1;
};

G_DEFINE_TYPE (RoamTlsManager, roam_tls_manager, G_TYPE_OBJECT)

G_DEFINE_AUTOPTR_CLEANUP_FUNC (BIGNUM, BN_free)
G_DEFINE_AUTOPTR_CLEANUP_FUNC (EVP_PKEY, EVP_PKEY_free)
G_DEFINE_AUTOPTR_CLEANUP_FUNC (X509, X509_free)
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RSA, RSA_free)
G_DEFINE_AUTOPTR_CLEANUP_FUNC (BIO, BIO_free)

static void
roam_tls_manager_dispose (GObject *object)
{
  RoamTlsManager *self = (RoamTlsManager *)object;
  g_autoptr(GList) list = NULL;

  list = self->queued_tasks.head;
  self->queued_tasks.head = NULL;
  self->queued_tasks.tail = NULL;
  self->queued_tasks.length = 0;

  for (const GList *iter = list; iter != NULL; iter = iter->next)
    {
      g_autoptr(GTask) task = iter->data;
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               G_IO_ERROR_CANCELLED,
                               "Task was disposed");
    }

  g_clear_object (&self->certificate);

  G_OBJECT_CLASS (roam_tls_manager_parent_class)->dispose (object);
}

static void
roam_tls_manager_class_init (RoamTlsManagerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = roam_tls_manager_dispose;
}

static void
roam_tls_manager_init (RoamTlsManager *self)
{
  g_autofree gchar *pub_file = NULL;
  g_autofree gchar *priv_file = NULL;

  pub_file = g_build_filename (g_get_user_config_dir (),
                               "gnome-roam", "tls", "public.pem",
                               NULL);
  priv_file = g_build_filename (g_get_user_config_dir (),
                                "gnome-roam", "tls", "private.pem",
                                NULL);

  self->public_key_file = g_intern_string (pub_file);
  self->private_key_file = g_intern_string (priv_file);

  self->certificate = g_tls_certificate_new_from_files (pub_file, priv_file, NULL);
}

RoamTlsManager *
roam_tls_manager_get_default (void)
{
  static RoamTlsManager *instance;

  if (instance == NULL)
    instance = g_object_new (ROAM_TYPE_TLS_MANAGER, NULL);

  return instance;
}

void
roam_tls_manager_set_certificate (RoamTlsManager  *self,
                                  GTlsCertificate *certificate)
{
  g_return_if_fail (ROAM_IS_TLS_MANAGER (self));
  g_return_if_fail (!certificate || G_IS_TLS_CERTIFICATE (certificate));

  g_set_object (&self->certificate, certificate);
}

static void
roam_tls_manager_build_certificate_worker (GTask        *task,
                                           gpointer      source_object,
                                           gpointer      task_data,
                                           GCancellable *cancellable)
{
  RoamTlsManager *self = source_object;
  g_autoptr(GTlsCertificate) certificate = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(EVP_PKEY) pk  = NULL;
  g_autoptr(BIGNUM) bne = NULL;
  g_autoptr(X509) x = NULL;
  g_autoptr(RSA) rsa = NULL;
  g_autoptr(BIO) pubkey = NULL;
  g_autoptr(BIO) privkey = NULL;
  g_autofree gchar *dir = NULL;
  X509_NAME *name;

  g_assert (ROAM_IS_TLS_MANAGER (self));
  g_assert (G_IS_TASK (task));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  dir = g_path_get_dirname (self->private_key_file);

  if (g_mkdir_with_parents (dir, 0750) != 0)
    {
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               g_io_error_from_errno (errno),
                               "%s", g_strerror (errno));
      return;
    }

  pk = EVP_PKEY_new ();
  if (pk == NULL)
    goto failure;

  x = X509_new ();
  if (x == NULL)
    goto failure;

  bne = BN_new ();
  if (bne == NULL || !BN_set_word (bne, RSA_F4))
    goto failure;

  rsa = RSA_new ();
  if (rsa == NULL || !RSA_generate_key_ex (rsa, DEFAULT_KEYSIZE, bne, NULL))
    goto failure;

  if (!EVP_PKEY_assign_RSA (pk, g_steal_pointer (&rsa)))
    goto failure;

  X509_set_version (x, 2);
  ASN1_INTEGER_set (X509_get_serialNumber (x), 0);
  X509_gmtime_adj (X509_get_notBefore (x), 0);
  /* 5 years. We'll figure out key rotation in that time... */
	X509_gmtime_adj (X509_get_notAfter (x), (long)60*60*24*5*365);
	X509_set_pubkey (x, pk);

  name = X509_get_subject_name (x);

  if (!X509_NAME_add_entry_by_txt (name, "C",
                                   MBSTRING_ASC, (const guchar *)"US", -1, -1, 0))
    goto failure;

  if (!X509_NAME_add_entry_by_txt (name, "CN",
                                   MBSTRING_ASC, (const guchar *)"GNOME", -1, -1, 0))
    goto failure;

  if (!X509_set_issuer_name (x, name))
    goto failure;

  if (!X509_sign (x, pk, EVP_md5 ()))
    goto failure;

  pubkey = BIO_new_file (self->public_key_file, "w+");
  if (pubkey == NULL || !PEM_write_bio_X509 (pubkey, x))
    goto failure;

  privkey = BIO_new_file (self->private_key_file, "w+");
  if (privkey == NULL || !PEM_write_bio_PrivateKey (privkey, pk, NULL, NULL, 0, NULL, NULL))
    goto failure;

  BIO_flush (pubkey);
  BIO_flush (privkey);

  certificate = g_tls_certificate_new_from_files (self->public_key_file,
                                                  self->private_key_file,
                                                  &error);

  if (certificate != NULL)
    g_task_return_pointer (task, g_steal_pointer (&certificate), g_object_unref);
  else
    g_task_return_error (task, g_steal_pointer (&error));

  return;

failure:
  g_task_return_new_error (task,
                           G_IO_ERROR,
                           G_IO_ERROR_FAILED,
                           "Failed to generate keypair");
}

static void
roam_tls_manager_build_certificate_async (RoamTlsManager      *self,
                                          GCancellable        *cancellable,
                                          GAsyncReadyCallback  callback,
                                          gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;

  g_assert (ROAM_IS_TLS_MANAGER (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));
  g_assert (self->building_cert == FALSE);

  self->building_cert = TRUE;

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, roam_tls_manager_build_certificate_async);
  g_task_set_priority (task, G_PRIORITY_LOW);
  g_task_run_in_thread (task, roam_tls_manager_build_certificate_worker);
}

static GTlsCertificate *
roam_tls_manager_build_certificate_finish (RoamTlsManager  *self,
                                           GAsyncResult    *result,
                                           GError         **error)
{
  g_return_val_if_fail (ROAM_IS_TLS_MANAGER (self), NULL);
  g_return_val_if_fail (G_IS_TASK (result), NULL);

  self->building_cert = FALSE;

  return g_task_propagate_pointer (G_TASK (result), error);
}

static void
roam_tls_manager_load_certificate_cb (GObject      *object,
                                      GAsyncResult *result,
                                      gpointer      user_data)
{
  RoamTlsManager *self = (RoamTlsManager *)object;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GTlsCertificate) certificate = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GList) list = NULL;

  g_assert (ROAM_IS_TLS_MANAGER (self));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  certificate = roam_tls_manager_build_certificate_finish (self, result, &error);

  if (self->certificate == NULL && certificate != NULL)
    g_set_object (&self->certificate, certificate);

  list = g_steal_pointer (&self->queued_tasks.head);
  self->queued_tasks.tail = NULL;
  self->queued_tasks.length = 0;

  if (certificate == NULL)
    {
      for (const GList *iter = list; iter != NULL; iter = iter->next)
        {
          g_autoptr(GTask) queued = iter->data;

          g_task_return_error (queued, g_error_copy (error));
        }

      g_task_return_error (task, g_steal_pointer (&error));
    }
  else
    {
      for (const GList *iter = list; iter != NULL; iter = iter->next)
        {
          g_autoptr(GTask) queued = iter->data;

          g_task_return_pointer (queued, g_object_ref (certificate), g_object_unref);
        }

      g_task_return_pointer (task, g_steal_pointer (&certificate), g_object_unref);
    }
}

/**
 * roam_tls_manager_load_certificate_async:
 * @self: a #RoamTlsManager
 * @cancellable: (nullable): a #GCancellable or %NULL
 * @callback: (nullable): a #GAsyncReadyCallback
 * @user_data: closure data for @callback
 *
 * Asynchronously loads the certificate for use by the peer. If
 * #RoamTlsManager:certificate has not been set, then a self-signed
 * certificate will be generated.
 *
 * @callback should call roam_tls_manager_load_certificate_finish() to
 * completion this operation and get the resulting #GTlsCertificate.
 *
 * Since: 3.28
 */
void
roam_tls_manager_load_certificate_async (RoamTlsManager      *self,
                                         GCancellable        *cancellable,
                                         GAsyncReadyCallback  callback,
                                         gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;

  g_return_if_fail (ROAM_IS_TLS_MANAGER (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, roam_tls_manager_load_certificate_async);
  g_task_set_priority (task, G_PRIORITY_LOW);

  if (self->certificate != NULL)
    {
      g_task_return_pointer (task, g_object_ref (self->certificate), g_object_unref);
      return;
    }

  if (self->building_cert)
    {
      g_queue_push_tail (&self->queued_tasks, g_steal_pointer (&task));
      return;
    }

  roam_tls_manager_build_certificate_async (self,
                                            cancellable,
                                            roam_tls_manager_load_certificate_cb,
                                            g_steal_pointer (&task));
}

/**
 * roam_tls_manager_load_certificate_finish:
 * @self: a #RoamTlsManager
 * @result: a #GAsyncResult
 * @error: a location for a #GError, or %NULL
 *
 * Completes an asynchrons request to
 * roam_tls_manager_load_certificate_async(). The result is a self-signed
 * cetificate that is suitable for use, or the result of
 * #RoamTlsManager:certificate if it has been set.
 *
 * Returns: (transfer full): a #GTlsCertificate if successful; otherwise
 *   %NULL and @error is set.
 *
 * Since: 3.28
 */
GTlsCertificate *
roam_tls_manager_load_certificate_finish (RoamTlsManager  *self,
                                          GAsyncResult    *result,
                                          GError         **error)
{
  g_return_val_if_fail (ROAM_IS_TLS_MANAGER (self), NULL);
  g_return_val_if_fail (G_IS_TASK (result), NULL);

  return g_task_propagate_pointer (G_TASK (result), error);
}
