/* roam-types.h
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gio/gio.h>

G_BEGIN_DECLS

typedef struct _RoamCredentials    RoamCredentials;
typedef struct _RoamDiscoveryLocal RoamDiscoveryLocal;
typedef struct _RoamDiscovery      RoamDiscovery;
typedef struct _RoamFile           RoamFile;
typedef struct _RoamFileList       RoamFileList;
typedef struct _RoamManager        RoamManager;
typedef struct _RoamMountList      RoamMountList;
typedef struct _RoamMount          RoamMount;
typedef struct _RoamPeerLocal      RoamPeerLocal;
typedef struct _RoamPeer           RoamPeer;
typedef struct _RoamPublisher      RoamPublisher;
typedef struct _RoamPublisherAvahi RoamPublisherAvahi;

typedef enum
{
  ROAM_MONITOR_EVENT_ADDED   = 1,
  ROAM_MONITOR_EVENT_REMOVED = 2,
} RoamMonitorEvent;

typedef void (*RoamMonitorCallback) (RoamMount        *mount,
                                     const gchar      *path,
                                     RoamMonitorEvent  event,
                                     gpointer          user_data);

G_END_DECLS
