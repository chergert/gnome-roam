/* roam-utils.c
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "roam-utils"

#include <unistd.h>

#include "roam-utils.h"

static const gchar *
get_machine_id_raw (void)
{
  static gchar *machine_id;

#ifdef __linux__
  if (machine_id == NULL)
    {
      g_file_get_contents ("/etc/machine-id", &machine_id, NULL, NULL);
      g_strstrip (machine_id);
    }
#else
# error "Add support for getting a machine identifier"
  g_assert_not_reached ();
#endif

  return machine_id;
}

/**
 * roam_get_peer_id:
 *
 * Gets a value that can be used to uniquely identify this instance
 * of the roaming process. It uses various information about the current
 * host and user to generate a hash.
 *
 * Returns: a string which should not be modified or freed.
 */
const gchar *
roam_get_peer_id (void)
{
  static gchar *machine_id;

  if (g_once_init_enter (&machine_id))
    {
      const gchar *raw = get_machine_id_raw ();
      GChecksum *csum = g_checksum_new (G_CHECKSUM_SHA256);
      GPid pid = getpid ();
      gchar *ret;

      g_checksum_update (csum, (const guchar *)raw, -1);
      g_checksum_update (csum, (const guchar *)g_get_host_name (), -1);
      g_checksum_update (csum, (const guchar *)g_get_user_name (), -1);
      g_checksum_update (csum, (const guchar *)&pid, sizeof pid);
      ret = g_strdup (g_checksum_get_string (csum));
      g_checksum_free (csum);

      g_once_init_leave (&machine_id, ret);
    }

  return machine_id;
}
