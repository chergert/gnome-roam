/* roam-utils.h
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

static inline void
roam_clear_source (guint *source_id)
{
  guint val = *source_id;
  *source_id = 0;
  if (val != 0)
    g_source_remove (val);
}

static inline void
roam_object_dispose_unref (gpointer ptr)
{
  g_object_run_dispose (ptr);
  g_object_unref (ptr);
}

#define roam_dispose_object(ptr) g_clear_pointer(ptr, roam_object_dispose_unref)

const gchar *roam_get_peer_id (void);

static inline gboolean
roam_str_empty0 (const gchar *str)
{
  return str == NULL || *str == 0;
}

G_END_DECLS
