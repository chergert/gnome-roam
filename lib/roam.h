/* roam.h
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gio/gio.h>

#define ROAM_INSIDE

#include "roam-enums.h"
#include "roam-types.h"
#include "roam-version.h"

#include "roam-avatar.h"
#include "roam-discovery.h"
#include "roam-file.h"
#include "roam-file-list.h"
#include "roam-manager.h"
#include "roam-mount.h"
#include "roam-mount-list.h"
#include "roam-peer.h"
#include "roam-publisher.h"
#include "roam-rpc-client.h"
#include "roam-rpc-server.h"
#include "roam-tls-manager.h"

#include "avahi/roam-discovery-avahi.h"
#include "avahi/roam-peer-avahi.h"
#include "avahi/roam-publisher-avahi.h"

#include "local/roam-discovery-local.h"
#include "local/roam-mount-local.h"
#include "local/roam-peer-local.h"

#include "volumes/roam-discovery-volumes.h"
#include "volumes/roam-peer-volume.h"

#undef ROAM_INSIDE
