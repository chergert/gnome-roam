/* roam-discovery-volumes.c
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "roam-discovery-volumes"

#include "roam-discovery-volumes.h"
#include "roam-peer-volume.h"

/**
 * SECTION:roam-discovery-volumes
 * @title: RoamDiscoveryVolumes
 * @short_description: content discovery for mounted volumes
 *
 * This #RoamDiscovery agent will discover volumes as they are mounted
 * and check to see if they are to be merged into the roam data set.
 *
 * For example, when a USB stick is connected, we will look for a hint
 * in the volume meta-data that we are to export the content into our
 * data sets.
 *
 * Since: 3.28
 */

struct _RoamDiscoveryVolumes
{
  RoamDiscovery   parent_instance;

  GVolumeMonitor *monitor;
  GHashTable     *peers_by_uuid;
};

G_DEFINE_TYPE (RoamDiscoveryVolumes, roam_discovery_volumes, ROAM_TYPE_DISCOVERY)

static gboolean
mount_is_interesting (RoamDiscoveryVolumes *self,
                      GMount               *mount)
{
  g_assert (ROAM_IS_DISCOVERY_VOLUMES (self));
  g_assert (G_IS_MOUNT (mount));

  if (g_mount_is_shadowed (mount))
    return FALSE;

  /* TODO: More checks */

  return TRUE;
}

static void
roam_discovery_volumes_mount_added_cb (RoamDiscoveryVolumes *self,
                                       GMount               *mount,
                                       GVolumeMonitor       *monitor)
{
  g_assert (ROAM_IS_DISCOVERY_VOLUMES (self));
  g_assert (G_IS_MOUNT (mount));
  g_assert (G_IS_VOLUME_MONITOR (monitor));

  if (mount_is_interesting (self, mount))
    {
      g_autofree gchar *name = g_mount_get_name (mount);
      g_autofree gchar *uuid = g_mount_get_uuid (mount);
      g_autoptr(RoamPeerVolume) peer = NULL;

      peer = roam_peer_volume_new (mount);

      if (uuid == NULL)
        uuid = g_strdup_printf ("org.gnome.roam.discovery.volume:%s", name);

      g_debug ("Discovered mount \"%s\" (%s)", name, uuid);

      g_hash_table_insert (self->peers_by_uuid,
                           g_steal_pointer (&uuid),
                           g_object_ref (peer));

      roam_discovery_emit_peer_added (ROAM_DISCOVERY (self), ROAM_PEER (peer));

      g_async_initable_init_async (G_ASYNC_INITABLE (peer),
                                   G_PRIORITY_LOW, NULL, NULL, NULL);
    }
}

static void
roam_discovery_volumes_mount_pre_unmount_cb (RoamDiscoveryVolumes *self,
                                             GMount               *mount,
                                             GVolumeMonitor       *monitor)
{
  g_assert (ROAM_IS_DISCOVERY_VOLUMES (self));
  g_assert (G_IS_MOUNT (mount));
  g_assert (G_IS_VOLUME_MONITOR (monitor));

}

static void
roam_discovery_volumes_mount_removed_cb (RoamDiscoveryVolumes *self,
                                         GMount               *mount,
                                         GVolumeMonitor       *monitor)
{
  g_autofree gchar *name = NULL;
  g_autofree gchar *uuid = NULL;
  RoamPeer *peer;

  g_assert (ROAM_IS_DISCOVERY_VOLUMES (self));
  g_assert (G_IS_MOUNT (mount));
  g_assert (G_IS_VOLUME_MONITOR (monitor));

  name = g_mount_get_name (mount);

  if (!(uuid = g_mount_get_uuid (mount)))
    uuid = g_strdup_printf ("org.gnome.roam.discovery.volume:%s", name);

  g_debug ("Removing mount \"%s\" (%s)", name, uuid);

  if ((peer = g_hash_table_lookup (self->peers_by_uuid, uuid)))
    {
      g_autoptr(RoamPeer) hold = g_object_ref (peer);

      g_hash_table_remove (self->peers_by_uuid, uuid);
      roam_discovery_emit_peer_removed (ROAM_DISCOVERY (self), hold);
    }
}

static void
roam_discovery_volumes_startup_async (RoamDiscovery       *discovery,
                                      GCancellable        *cancellable,
                                      GAsyncReadyCallback  callback,
                                      gpointer             user_data)
{
  RoamDiscoveryVolumes *self = (RoamDiscoveryVolumes *)discovery;
  g_autoptr(GTask) task = NULL;
  g_autoptr(GVolumeMonitor) monitor = NULL;
  g_autoptr(GList) mounts = NULL;

  g_assert (ROAM_IS_DISCOVERY_VOLUMES (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, roam_discovery_volumes_startup_async);
  g_task_set_priority (task, G_PRIORITY_LOW);

  monitor = g_volume_monitor_get ();

  g_signal_connect_object (monitor,
                           "mount-added",
                           G_CALLBACK (roam_discovery_volumes_mount_added_cb),
                           self,
                           G_CONNECT_SWAPPED);

  g_signal_connect_object (monitor,
                           "mount-pre-unmount",
                           G_CALLBACK (roam_discovery_volumes_mount_pre_unmount_cb),
                           self,
                           G_CONNECT_SWAPPED);

  g_signal_connect_object (monitor,
                           "mount-removed",
                           G_CALLBACK (roam_discovery_volumes_mount_removed_cb),
                           self,
                           G_CONNECT_SWAPPED);

  mounts = g_volume_monitor_get_mounts (monitor);

  for (const GList *iter = mounts; iter != NULL; iter = iter->next)
    {
      g_autoptr(GMount) mount = iter->data;

      roam_discovery_volumes_mount_added_cb (self, mount, monitor);
    }

  self->monitor = g_steal_pointer (&monitor);

  g_task_return_boolean (task, TRUE);
}

static gboolean
roam_discovery_volumes_startup_finish (RoamDiscovery  *discovery,
                                       GAsyncResult   *result,
                                       GError        **error)
{
  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
roam_discovery_volumes_dispose (GObject *object)
{
  RoamDiscoveryVolumes *self = (RoamDiscoveryVolumes *)object;

  g_clear_object (&self->monitor);
  g_clear_pointer (&self->peers_by_uuid, g_hash_table_unref);

  G_OBJECT_CLASS (roam_discovery_volumes_parent_class)->dispose (object);
}

static void
roam_discovery_volumes_class_init (RoamDiscoveryVolumesClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  RoamDiscoveryClass *discovery_class = ROAM_DISCOVERY_CLASS (klass);

  object_class->dispose = roam_discovery_volumes_dispose;

  discovery_class->startup_async = roam_discovery_volumes_startup_async;
  discovery_class->startup_finish = roam_discovery_volumes_startup_finish;
}

static void
roam_discovery_volumes_init (RoamDiscoveryVolumes *self)
{
  self->peers_by_uuid = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_object_unref);
}

RoamDiscoveryVolumes *
roam_discovery_volumes_new (void)
{
  return g_object_new (ROAM_TYPE_DISCOVERY_VOLUMES, NULL);
}
