/* roam-peer-volume.c
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "roam-peer-volume"

#include "roam-file.h"
#include "roam-file-list.h"
#include "roam-mount.h"
#include "roam-mount-list.h"
#include "roam-path.h"
#include "roam-peer-volume.h"
#include "roam-utils.h"

#define DEFAULT_ENUM_LIST_SIZE 25

struct _RoamPeerVolume
{
  RoamPeer  parent_instance;
  GMount   *mount;
};

static void async_initable_iface_init (GAsyncInitableIface *iface);

G_DEFINE_TYPE_WITH_CODE (RoamPeerVolume, roam_peer_volume, ROAM_TYPE_PEER,
                         G_IMPLEMENT_INTERFACE (G_TYPE_ASYNC_INITABLE, async_initable_iface_init))

enum {
  PROP_0,
  PROP_MOUNT,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
roam_peer_volume_list_files_next_files_cb (GObject      *object,
                                           GAsyncResult *result,
                                           gpointer      user_data)
{
  GFileEnumerator *enumerator = (GFileEnumerator *)object;
  g_autoptr(GTask) task = user_data;
  g_autolist(GFileInfo) children = NULL;
  g_autoptr(GError) error = NULL;
  const gchar *base;
  GPtrArray *files;

  g_assert (G_IS_FILE_ENUMERATOR (enumerator));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  files = g_task_get_task_data (task);
  g_assert (files != NULL);

  children = g_file_enumerator_next_files_finish (enumerator, result, &error);

  if (error != NULL)
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  base = g_object_get_data (G_OBJECT (task), "PATH_BASE");
  g_assert (base != NULL);

  for (const GList *iter = children; iter != NULL; iter = iter->next)
    {
      GFileInfo *info = iter->data;
      g_autoptr(RoamFile) file = NULL;
      g_autofree gchar *path = NULL;
      const  gchar *name;
      GIcon *symbolic_icon;
      GIcon *icon;
      GFileType file_type;

      name = g_file_info_get_name (info);
      file_type = g_file_info_get_file_type (info);

      if (roam_path_is_ignored (name, file_type))
        continue;

      path = g_build_filename (base, name, NULL);
      file = roam_file_new_for_path (path);
      roam_file_set_is_directory (file, file_type == G_FILE_TYPE_DIRECTORY);

      icon = g_file_info_get_icon (info);
      symbolic_icon = g_file_info_get_symbolic_icon (info);

      roam_file_set_icon (file, icon);
      roam_file_set_symbolic_icon (file, symbolic_icon);

      g_ptr_array_add (files, g_steal_pointer (&file));
    }

  if (children != NULL)
    {
      GCancellable *cancellable = g_task_get_cancellable (task);
      g_file_enumerator_next_files_async (enumerator,
                                          DEFAULT_ENUM_LIST_SIZE,
                                          G_PRIORITY_LOW,
                                          cancellable,
                                          roam_peer_volume_list_files_next_files_cb,
                                          g_steal_pointer (&task));
      return;
    }

  g_task_return_pointer (task,
                         g_ptr_array_ref (files),
                         (GDestroyNotify) g_ptr_array_unref);
}

static void
roam_peer_volume_list_files_enumerate_cb (GObject      *object,
                                          GAsyncResult *result,
                                          gpointer      user_data)
{
  GFile *file = (GFile *)object;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GFileEnumerator) enumerator = NULL;
  g_autoptr(GError) error = NULL;
  GCancellable *cancellable;

  g_assert (G_IS_FILE (file));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  enumerator = g_file_enumerate_children_finish (file, result, &error);

  if (error != NULL)
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  cancellable = g_task_get_cancellable (task);

  g_file_enumerator_next_files_async (enumerator,
                                      DEFAULT_ENUM_LIST_SIZE,
                                      G_PRIORITY_LOW,
                                      cancellable,
                                      roam_peer_volume_list_files_next_files_cb,
                                      g_steal_pointer (&task));
}

static void
roam_peer_volume_list_files_async (RoamPeer            *peer,
                                   RoamMount           *mount,
                                   const gchar         *path,
                                   GCancellable        *cancellable,
                                   GAsyncReadyCallback  callback,
                                   gpointer             user_data)
{
  RoamPeerVolume *self = (RoamPeerVolume *)peer;
  g_autoptr(GTask) task = NULL;
  g_autoptr(GFile) root = NULL;
  g_autoptr(GFile) parent = NULL;
  g_autoptr(GFile) child = NULL;

  g_assert (ROAM_IS_PEER_VOLUME (self));
  g_assert (ROAM_IS_MOUNT (mount));
  g_assert (path != NULL);
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, roam_peer_volume_list_files_async);
  g_task_set_priority (task, G_PRIORITY_LOW);
  g_task_set_task_data (task,
                        g_ptr_array_new_with_free_func (g_object_unref),
                        (GDestroyNotify) g_ptr_array_unref);

  root = g_mount_get_root (self->mount);
  parent = g_file_get_child (root, roam_mount_get_name (mount));

  if (path == NULL)
    path = "/";

  g_assert (path != NULL);

  g_object_set_data_full (G_OBJECT (task), "PATH_BASE", g_strdup (path), g_free);

  while (*path == '/')
    path++;

  if (!roam_str_empty0 (path))
    child = g_file_get_child (parent, path);
  else
    child = g_steal_pointer (&parent);

  g_file_enumerate_children_async (child,
                                   G_FILE_ATTRIBUTE_STANDARD_NAME","
                                   G_FILE_ATTRIBUTE_STANDARD_SIZE","
                                   G_FILE_ATTRIBUTE_STANDARD_ICON","
                                   G_FILE_ATTRIBUTE_STANDARD_SYMBOLIC_ICON","
                                   G_FILE_ATTRIBUTE_STANDARD_TYPE,
                                   G_FILE_QUERY_INFO_NOFOLLOW_SYMLINKS,
                                   G_PRIORITY_LOW,
                                   cancellable,
                                   roam_peer_volume_list_files_enumerate_cb,
                                   g_steal_pointer (&task));
}

static GPtrArray *
roam_peer_volume_list_files_finish (RoamPeer      *peer,
                                    GAsyncResult  *result,
                                    GError       **error)
{
  g_assert (ROAM_IS_PEER_VOLUME (peer));
  g_assert (G_IS_TASK (result));
  g_assert (g_task_is_valid (G_TASK (result), peer));

  return g_task_propagate_pointer (G_TASK (result), error);
}

static void
roam_peer_volume_enumerate_mount_next_files_cb (GObject      *object,
                                                GAsyncResult *result,
                                                gpointer      user_data)
{
  GFileEnumerator *enumerator = (GFileEnumerator *)object;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;
  g_autoptr(GList) files = NULL;
  g_autoptr(GFile) root = NULL;
  RoamPeerVolume *self;
  GCancellable *cancellable;
  int io_priority;

  g_assert (G_IS_FILE_ENUMERATOR (enumerator));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  io_priority = g_task_get_priority (task);

  cancellable = g_task_get_cancellable (task);
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  self = g_task_get_source_object (task);
  g_assert (ROAM_IS_PEER_VOLUME (self));

  files = g_file_enumerator_next_files_finish (enumerator, result, &error);

  if (error != NULL)
    {
      g_warning ("%s", error->message);
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  if (files == NULL || self->mount == NULL)
    {
      g_task_return_boolean (task, TRUE);
      return;
    }

  root = g_mount_get_root (self->mount);

  for (const GList *iter = files; iter != NULL; iter = iter->next)
    {
      g_autoptr(GFileInfo) info = iter->data;
      g_autoptr(RoamMount) mount = NULL;
      g_autoptr(GFile) file = NULL;
      g_autofree gchar *path = NULL;
      g_autofree gchar *id = NULL;
      const gchar *icon_name = NULL;
      const gchar *symbolic_icon_name = NULL;
      const gchar *name;
      GFileType file_type;

      file_type = g_file_info_get_file_type (info);
      if (file_type != G_FILE_TYPE_DIRECTORY)
        continue;

      name = g_file_info_get_name (info);

      if (roam_path_is_ignored (name, file_type))
        continue;

      file = g_file_get_child (root, name);
      path = g_file_get_path (file);

      if (g_strcmp0 (name, "Music") == 0)
        {
          id = g_strdup ("org.gnome.roam.xdg-music");
          icon_name = "folder-music";
          symbolic_icon_name = "folder-music-symoblic";
        }
      else if (g_strcmp0 (name, "Videos") == 0)
        {
          id = g_strdup ("org.gnome.roam.xdg-videos");
          icon_name = "folder-videos";
          symbolic_icon_name = "folder-videos-symoblic";
        }
      else if (g_strcmp0 (name, "Documents") == 0)
        {
          id = g_strdup ("org.gnome.roam.xdg-documents");
          icon_name = "folder-documents";
          symbolic_icon_name = "folder-documents-symbolic";
        }
      else
        {
          id = g_strdup_printf ("org.gnome.roam.directory:%s", name);
          icon_name = "folder";
          symbolic_icon_name = "folder-symbolic";
        }

      mount = g_object_new (ROAM_TYPE_MOUNT,
                            "exportable", TRUE,
                            "icon-name", icon_name,
                            "id", id,
                            "name", name,
                            "symbolic-icon-name", symbolic_icon_name,
                            NULL);
      roam_mount_add_peer (mount, ROAM_PEER (self));
      roam_peer_emit_mount_added (ROAM_PEER (self), mount);
    }

  g_file_enumerator_next_files_async (enumerator,
                                      DEFAULT_ENUM_LIST_SIZE,
                                      io_priority,
                                      cancellable,
                                      roam_peer_volume_enumerate_mount_next_files_cb,
                                      g_steal_pointer (&task));
}

static void
roam_peer_volume_enumerate_mount_children_cb (GObject      *object,
                                              GAsyncResult *result,
                                              gpointer      user_data)
{
  GFile *file = (GFile *)object;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GFileEnumerator) enumerator = NULL;
  g_autoptr(GError) error = NULL;
  GCancellable *cancellable;
  int io_priority;

  g_assert (G_IS_FILE (file));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  io_priority = g_task_get_priority (task);

  cancellable = g_task_get_cancellable (task);
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  enumerator = g_file_enumerate_children_finish (file, result, &error);
  g_assert (!enumerator || G_IS_FILE_ENUMERATOR (enumerator));

  if (error != NULL)
    {
      g_warning ("%s", error->message);
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  g_file_enumerator_next_files_async (enumerator,
                                      DEFAULT_ENUM_LIST_SIZE,
                                      io_priority,
                                      cancellable,
                                      roam_peer_volume_enumerate_mount_next_files_cb,
                                      g_steal_pointer (&task));
}

static void
roam_peer_volume_mount_exists_cb (GObject      *object,
                                  GAsyncResult *result,
                                  gpointer      user_data)
{
  GFile *file = (GFile *)object;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;
  g_autoptr(GFileInfo) info = NULL;
  g_autoptr(GFile) parent = NULL;
  GCancellable *cancellable;

  g_assert (G_IS_FILE (file));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  info = g_file_query_info_finish (file, result, &error);

  if (g_error_matches (error, G_IO_ERROR, G_IO_ERROR_NOT_FOUND) ||
      g_error_matches (error, G_IO_ERROR, G_IO_ERROR_CANCELLED))
    {
      g_task_return_boolean (task, TRUE);
      return;
    }

  if (error != NULL)
    {
      g_warning ("Failed to query .gnome-roam: %s", error->message);
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  if (g_file_info_get_file_type (info) != G_FILE_TYPE_DIRECTORY)
    {
      g_task_return_boolean (task, TRUE);
      return;
    }

  cancellable = g_task_get_cancellable (task);
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  parent = g_file_get_parent (file);
  g_assert (G_IS_FILE (parent));

  g_file_enumerate_children_async (parent,
                                   G_FILE_ATTRIBUTE_STANDARD_NAME","
                                   G_FILE_ATTRIBUTE_STANDARD_TYPE,
                                   G_FILE_QUERY_INFO_NOFOLLOW_SYMLINKS,
                                   G_PRIORITY_LOW,
                                   cancellable,
                                   roam_peer_volume_enumerate_mount_children_cb,
                                   g_steal_pointer (&task));
}

static void
roam_peer_volume_dispose (GObject *object)
{
  RoamPeerVolume *self = (RoamPeerVolume *)object;

  g_clear_object (&self->mount);

  G_OBJECT_CLASS (roam_peer_volume_parent_class)->dispose (object);
}

static void
roam_peer_volume_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  RoamPeerVolume *self = ROAM_PEER_VOLUME (object);

  switch (prop_id)
    {
    case PROP_MOUNT:
      g_value_set_object (value, self->mount);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
roam_peer_volume_set_property (GObject      *object,
                               guint         prop_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  RoamPeerVolume *self = ROAM_PEER_VOLUME (object);

  switch (prop_id)
    {
    case PROP_MOUNT:
      self->mount = g_value_dup_object (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
roam_peer_volume_class_init (RoamPeerVolumeClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  RoamPeerClass *peer_class = ROAM_PEER_CLASS (klass);

  object_class->dispose = roam_peer_volume_dispose;
  object_class->get_property = roam_peer_volume_get_property;
  object_class->set_property = roam_peer_volume_set_property;

  peer_class->list_files_async = roam_peer_volume_list_files_async;
  peer_class->list_files_finish = roam_peer_volume_list_files_finish;

  /**
   * RoamPeerVolume:mount:
   *
   * The "mount" property is the underling #GMount
   *
   * Since: 3.28
   */
  properties [PROP_MOUNT] =
    g_param_spec_object ("mount",
                         "Mount",
                         "The underlying mount",
                         G_TYPE_MOUNT,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
roam_peer_volume_init (RoamPeerVolume *self)
{
}

RoamPeerVolume *
roam_peer_volume_new (GMount *mount)
{
  g_autofree gchar *uuid = NULL;

  g_return_val_if_fail (G_IS_MOUNT (mount), NULL);

  uuid = g_mount_get_uuid (mount);

  return g_object_new (ROAM_TYPE_PEER_VOLUME,
                       "id", uuid,
                       "mount", mount,
                       NULL);
}

static void
roam_peer_volume_init_async (GAsyncInitable      *initable,
                             gint                 io_priority,
                             GCancellable        *cancellable,
                             GAsyncReadyCallback  callback,
                             gpointer             user_data)
{
  RoamPeerVolume *self = (RoamPeerVolume *)initable;
  g_autoptr(GTask) task = NULL;
  g_autoptr(GFile) root = NULL;
  g_autoptr(GFile) data_dir = NULL;

  g_assert (ROAM_IS_PEER_VOLUME (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, roam_peer_volume_init_async);
  g_task_set_priority (task, io_priority);

  if (self->mount == NULL)
    {
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               G_IO_ERROR_INVAL,
                               "%s not configured correctly",
                               G_OBJECT_TYPE_NAME (self));
      return;
    }

  /*
   * We only enroll mounts on this drive if we discover that the
   * .gnome-roam directory exists.
   */
  root = g_mount_get_root (self->mount);
  data_dir = g_file_get_child (root, ".gnome-roam");

  g_file_query_info_async (data_dir,
                           G_FILE_ATTRIBUTE_STANDARD_TYPE,
                           G_FILE_QUERY_INFO_NOFOLLOW_SYMLINKS,
                           G_PRIORITY_LOW,
                           cancellable,
                           roam_peer_volume_mount_exists_cb,
                           g_steal_pointer (&task));
}

static gboolean
roam_peer_volume_init_finish (GAsyncInitable  *initable,
                              GAsyncResult    *result,
                              GError         **error)
{
  g_assert (ROAM_IS_PEER_VOLUME (initable));
  g_assert (G_IS_TASK (result));

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
async_initable_iface_init (GAsyncInitableIface *iface)
{
  iface->init_async = roam_peer_volume_init_async;
  iface->init_finish = roam_peer_volume_init_finish;
}
