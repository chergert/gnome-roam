/* test-roam-manager.c
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "roam.h"

#define file_new_build_filename(...) \
  ({ gchar *_path = g_build_filename (__VA_ARGS__); \
     GFile *file = g_file_new_for_path (_path); \
     g_free (_path); \
     file; })

static GMainLoop *main_loop;
static GPtrArray *peers;
static GListModel *mounts;
static GListModel *children;

static gboolean
wakeup (gpointer data)
{
  g_main_loop_quit (data);
  return G_SOURCE_REMOVE;
}

static void
main_sleep (GMainLoop *main_loop,
            guint      msec)
{
  g_timeout_add (msec, wakeup, main_loop);
  g_main_loop_run (main_loop);
}

static void
peer_added_cb (RoamManager   *manager,
               RoamDiscovery *discovery,
               RoamPeer      *peer)
{
  g_assert (ROAM_IS_MANAGER (manager));
  g_assert (ROAM_IS_PEER (peer));

  g_ptr_array_add (peers, g_object_ref (peer));
}

static void
peer_removed_cb (RoamManager   *manager,
                 RoamDiscovery *discovery,
                 RoamPeer      *peer)
{
  g_assert (ROAM_IS_MANAGER (manager));
  g_assert (ROAM_IS_PEER (peer));

  g_ptr_array_remove (peers, peer);
}

static void
startup_cb (GObject      *object,
            GAsyncResult *result,
            gpointer      user_data)
{
  RoamManager *manager = (RoamManager *)object;
  g_autoptr(GError) error = NULL;

  g_assert (ROAM_IS_MANAGER (manager));
  g_assert (G_IS_ASYNC_RESULT (result));

  if (!roam_manager_startup_finish (manager, result, &error))
    g_error ("%s", error->message);

  g_main_loop_quit (main_loop);
}

static void
list_children_cb (GObject      *object,
                  GAsyncResult *result,
                  gpointer      user_data)
{
  RoamMount *mount = (RoamMount *)object;
  g_autoptr(GError) error = NULL;

  g_assert (ROAM_IS_MOUNT (mount));
  g_assert (G_IS_ASYNC_RESULT (result));

  children = roam_mount_list_children_finish (mount, result, &error);
  g_assert_no_error (error);
  g_assert_nonnull (children);

  g_main_loop_quit (main_loop);
}

static void
test_basic (void)
{
  static const gchar *peer_names[] = { "peer-1", "peer-2", "peer-3" };
  g_autoptr(RoamManager) manager = NULL;
  g_autoptr(RoamMount) mount = NULL;

  main_loop = g_main_loop_new (NULL, FALSE);
  peers = g_ptr_array_new_with_free_func (g_object_unref);

  manager = g_object_new (ROAM_TYPE_MANAGER, NULL);

  for (guint i = 0; i < G_N_ELEMENTS (peer_names); i++)
    {
      g_autoptr(RoamDiscoveryLocal) discovery = roam_discovery_local_new ();
      static const struct {
        const gchar *id;
        const gchar *name;
        const gchar *icon_name;
        const gchar *symbolic_icon_name;
      } mount_info[] = {
        { "org.gnome.roam.xdg-music", "Music", "folder-music", "folder-music-symbolic" },
        { "org.gnome.roam.xdg-videos", "Videos", "folder-videos", "folder-videos-symbolic" },
      };

      for (guint j = 0; j < G_N_ELEMENTS (mount_info); j++)
        {
          g_autofree gchar *path = g_build_filename (TEST_DATA_DIR,
                                                     peer_names[i],
                                                     mount_info[j].name,
                                                     NULL);
          g_autoptr(GFile) file = g_file_new_for_path (path);

          roam_discovery_local_add_mount (discovery,
                                          mount_info[j].id,
                                          mount_info[j].name,
                                          mount_info[j].icon_name,
                                          mount_info[j].symbolic_icon_name,
                                          file);
        }

      roam_manager_add_discovery (manager, ROAM_DISCOVERY (discovery));
    }

  g_signal_connect (manager, "peer-added", G_CALLBACK (peer_added_cb), NULL);
  g_signal_connect (manager, "peer-removed", G_CALLBACK (peer_removed_cb), NULL);

  roam_manager_startup_async (manager, NULL, startup_cb, NULL);
  g_main_loop_run (main_loop);
  g_assert_cmpint (peers->len, ==, 3);

  mounts = roam_manager_get_mounts (manager);
  g_assert (mounts != NULL);
  g_assert_cmpint (g_list_model_get_n_items (mounts), ==, 2);

  mount = g_list_model_get_item (mounts, 0);
  g_assert (mount != NULL);
  g_assert (ROAM_IS_MOUNT (mount));
  g_assert_cmpstr ("Music", ==, roam_mount_get_name (mount));

  roam_mount_list_children_async (mount, "/", NULL, list_children_cb, NULL);
  g_main_loop_run (main_loop);
  g_assert (children != NULL);
  g_assert (ROAM_IS_FILE_LIST (children));
  g_assert_cmpstr ("/", ==, roam_file_list_get_path (ROAM_FILE_LIST (children)));

  main_sleep (main_loop, 30);
  g_assert_cmpint (g_list_model_get_n_items (children), ==, 9);

  g_main_loop_unref (main_loop);
  g_clear_pointer (&peers, g_ptr_array_unref);
  g_clear_object (&mounts);
  g_clear_object (&children);
}

gint
main (gint argc,
      gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);
  g_test_add_func ("/Roam/Manager/basic", test_basic);
  return g_test_run ();
}
