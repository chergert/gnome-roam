/* gnome-roam-browser.c
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <dazzle.h>
#include <roam.h>

static void
node_items_changed (GListModel  *children,
                    guint        position,
                    guint        removed,
                    guint        added,
                    DzlTreeNode *parent)
{
  const gchar *child_type;

  g_assert (G_IS_LIST_MODEL (children));
  g_assert (DZL_IS_TREE_NODE (parent));

  child_type = g_object_get_data (G_OBJECT (parent), "CHILD_TYPE");

  for (; removed > 0; removed--)
    {
      DzlTreeNode *child;

      child = dzl_tree_node_nth_child (parent, position);

      /* Could be NULL if we in a racey removal cleanup */
      if (child != NULL)
        dzl_tree_node_remove (parent, child);
    }

  for (; added > 0; added--)
    {
      g_autoptr(GObject) item = g_list_model_get_item (children, position + added - 1);
      DzlTreeNode *child;

      child = dzl_tree_node_new ();
      dzl_tree_node_set_item (child, item);
      g_object_set_data (G_OBJECT (child), "TYPE", (gpointer)child_type);
      dzl_tree_node_insert (parent, child, position);
    }

  if (!dzl_tree_node_get_expanded (parent))
    dzl_tree_node_expand (parent, TRUE);
}

static void
use_model_for_node_children (DzlTreeNode *parent,
                             GListModel  *children,
                             const gchar *type)
{
  guint n_items;

  g_assert (DZL_IS_TREE_NODE (parent));
  g_assert (G_IS_LIST_MODEL (children));

  type = g_intern_string (type);
  n_items = g_list_model_get_n_items (children);

  for (guint i = 0; i < n_items; i++)
    {
      g_autoptr(GObject) item = g_list_model_get_item (children, i);
      DzlTreeNode *child;

      child = dzl_tree_node_new ();
      dzl_tree_node_set_item (child, item);
      g_object_set_data (G_OBJECT (child), "TYPE", (gpointer)type);
      dzl_tree_node_append (parent, child);
    }

  g_object_set_data (G_OBJECT (parent), "CHILD_TYPE", (gpointer)type);

  g_signal_connect_object (children,
                           "items-changed",
                           G_CALLBACK (node_items_changed),
                           parent,
                           0);

  /* Keep a reference around so the list is not disposed.
   * We need it alive to get signals.
   */

  g_object_set_data_full (G_OBJECT (parent),
                          "CHILDREN",
                          g_object_ref (children),
                          g_object_unref);
}

static void
build_children_cb (DzlTreeBuilder *builder,
                   DzlTreeNode    *node,
                   gpointer        user_data)
{
  const gchar *type;
  gpointer item;

  g_assert (DZL_IS_TREE_BUILDER (builder));
  g_assert (DZL_IS_TREE_NODE (node));

  type = g_object_get_data (G_OBJECT (node), "TYPE");
  item = dzl_tree_node_get_item (node);

  if (type == NULL)
    {
      DzlTreeNode *mounts;
      DzlTreeNode *peers;
      DzlTreeNode *discovery;

      g_assert (ROAM_IS_MANAGER (item));

      mounts = dzl_tree_node_new ();
      dzl_tree_node_set_item (mounts, item);
      g_object_set_data (G_OBJECT (mounts), "TYPE", "MOUNTS");
      dzl_tree_node_set_reset_on_collapse (mounts, TRUE);
      dzl_tree_node_append (node, mounts);

      peers = dzl_tree_node_new ();
      dzl_tree_node_set_item (peers, item);
      g_object_set_data (G_OBJECT (peers), "TYPE", "PEERS");
      dzl_tree_node_set_reset_on_collapse (peers, TRUE);
      dzl_tree_node_append (node, peers);

      discovery = dzl_tree_node_new ();
      dzl_tree_node_set_item (discovery, item);
      g_object_set_data (G_OBJECT (discovery), "TYPE", "DISCOVERERS");
      dzl_tree_node_set_reset_on_collapse (discovery, TRUE);
      dzl_tree_node_append (node, discovery);
    }
  else if (g_strcmp0 (type, "MOUNTS") == 0)
    {
      g_autoptr(GListModel) mounts = NULL;

      g_assert (ROAM_IS_MANAGER (item));

      mounts = roam_manager_get_mounts (item);
      use_model_for_node_children (node, mounts, "MOUNT");
    }
  else if (g_strcmp0 (type, "MOUNT") == 0)
    {
      g_autoptr(RoamFileList) children = NULL;

      g_assert (ROAM_IS_MOUNT (item));

      children = roam_mount_list_children (item, "/");
      use_model_for_node_children (node, G_LIST_MODEL (children), "FILE");
    }
  else if (g_strcmp0 (type, "DISCOVERERS") == 0)
    {
      g_autoptr(GListModel) peers = NULL;

      g_assert (ROAM_IS_MANAGER (item));

      peers = roam_manager_get_discoverers (item);
      use_model_for_node_children (node, peers, "DISCOVERY");
    }
  else if (g_strcmp0 (type, "PEERS") == 0)
    {
      g_autoptr(GListModel) peers = NULL;

      g_assert (ROAM_IS_MANAGER (item));

      peers = roam_manager_get_peers (item);
      use_model_for_node_children (node, peers, "PEER");
    }
  else if (g_strcmp0 (type, "FILE") == 0)
    {
      RoamMount *mount = NULL;

      g_assert (ROAM_IS_FILE (item));

      for (DzlTreeNode *parent = dzl_tree_node_get_parent (node);
           parent != NULL;
           parent = dzl_tree_node_get_parent (parent))
        {
          if (ROAM_IS_MOUNT (dzl_tree_node_get_item (parent)))
            {
              mount = ROAM_MOUNT (dzl_tree_node_get_item (parent));
              break;
            }
        }

      if (mount != NULL)
        {
          g_autoptr(RoamFileList) children = NULL;
          const gchar *path;

          g_assert (ROAM_IS_FILE (item));

          path = roam_file_get_path (item);
          children = roam_mount_list_children (mount, path);
          use_model_for_node_children (node, G_LIST_MODEL (children), "FILE");
        }
    }
}

static void
build_node_cb (DzlTreeBuilder *builder,
               DzlTreeNode    *node)
{
  gpointer item;
  const gchar *type;

  g_assert (DZL_IS_TREE_BUILDER (builder));
  g_assert (DZL_IS_TREE_NODE (node));

  item = dzl_tree_node_get_item (node);
  type = g_object_get_data (G_OBJECT (node), "TYPE");

  if (g_strcmp0 (type, "MOUNTS") == 0)
    {
      dzl_tree_node_set_text (node, "Mounts");
      dzl_tree_node_set_icon_name (node, "folder-remote-symbolic");
      dzl_tree_node_set_children_possible (node, TRUE);
    }
  else if (g_strcmp0 (type, "MOUNT") == 0)
    {
      g_assert (ROAM_IS_MOUNT (item));

      dzl_tree_node_set_text (node, roam_mount_get_name (item));
      dzl_tree_node_set_icon_name (node, roam_mount_get_symbolic_icon_name (item));
      dzl_tree_node_set_children_possible (node, TRUE);
    }
  else if (g_strcmp0 (type, "PEERS") == 0)
    {
      dzl_tree_node_set_icon_name (node, "network-workgroup-symbolic");
      dzl_tree_node_set_text (node, "Peers");
      dzl_tree_node_set_children_possible (node, TRUE);
    }
  else if (g_strcmp0 (type, "PEER") == 0)
    {
      const gchar *id = roam_peer_get_id (item);
      if (id == NULL)
        id = G_OBJECT_TYPE_NAME (item);
      dzl_tree_node_set_icon_name (node, "folder-symbolic");
      dzl_tree_node_set_text (node, id);
    }
  else if (g_strcmp0 (type, "DISCOVERERS") == 0)
    {
      dzl_tree_node_set_icon_name (node, "edit-find-symbolic");
      dzl_tree_node_set_text (node, "Discovery");
      dzl_tree_node_set_children_possible (node, TRUE);
    }
  else if (g_strcmp0 (type, "DISCOVERY") == 0)
    {
      dzl_tree_node_set_icon_name (node, "edit-find-symbolic");
      dzl_tree_node_set_text (node, G_OBJECT_TYPE_NAME (item));
    }
  else if (g_strcmp0 (type, "FILE") == 0)
    {
      g_assert (ROAM_IS_FILE (item));

      dzl_tree_node_set_gicon (node, roam_file_get_symbolic_icon (item));
      dzl_tree_node_set_text (node, roam_file_get_name (item));
      dzl_tree_node_set_children_possible (node, roam_file_get_is_directory (item));
    }
}

static GtkWidget *
create_window (RoamManager *manager)
{
  GtkScrolledWindow *list_scroller;
  GtkScrolledWindow *tree_scroller;
  DzlTreeBuilder *builder;
  DzlTreeNode *root;
  GtkTreeView *list;
  GtkWindow *window;
  GtkPaned *paned;
  DzlTree *tree;

  window = g_object_new (GTK_TYPE_WINDOW,
                         "title", "Roam Browser",
                         "default-width", 900,
                         "default-height", 600,
                         NULL);

  paned = g_object_new (GTK_TYPE_PANED,
                        "orientation", GTK_ORIENTATION_HORIZONTAL,
                        "position", 275,
                        "visible", TRUE,
                        NULL);
  gtk_container_add (GTK_CONTAINER (window), GTK_WIDGET (paned));

  tree_scroller = g_object_new (GTK_TYPE_SCROLLED_WINDOW,
                                "visible", TRUE,
                                NULL);
  gtk_container_add (GTK_CONTAINER (paned), GTK_WIDGET (tree_scroller));

  list_scroller = g_object_new (GTK_TYPE_SCROLLED_WINDOW,
                                "visible", TRUE,
                                NULL);
  gtk_container_add (GTK_CONTAINER (paned), GTK_WIDGET (list_scroller));

  tree = g_object_new (DZL_TYPE_TREE,
                       "activate-on-single-click", TRUE,
                       "show-icons", TRUE,
                       "headers-visible", FALSE,
                       "visible", TRUE,
                       NULL);
  gtk_tree_selection_set_mode (gtk_tree_view_get_selection (GTK_TREE_VIEW (tree)),
                               GTK_SELECTION_BROWSE);
  gtk_container_add (GTK_CONTAINER (tree_scroller), GTK_WIDGET (tree));

  builder = dzl_tree_builder_new ();
  g_signal_connect (builder, "build-children", G_CALLBACK (build_children_cb), NULL);
  g_signal_connect (builder, "build-node", G_CALLBACK (build_node_cb), NULL);
  dzl_tree_add_builder (tree, builder);

  root = g_object_new (DZL_TYPE_TREE_NODE,
                       "item", manager,
                       NULL);
  dzl_tree_set_root (tree, root);

  list = g_object_new (GTK_TYPE_TREE_VIEW,
                       "visible", TRUE,
                       "headers-visible", TRUE,
                       NULL);
  gtk_container_add (GTK_CONTAINER (list_scroller), GTK_WIDGET (list));

  return GTK_WIDGET (window);
}

static RoamManager *
create_manager (void)
{
  g_autoptr(RoamDiscoveryAvahi) avahi = NULL;
  g_autoptr(RoamDiscoveryLocal) local = NULL;
  g_autoptr(RoamDiscoveryVolumes) volumes = NULL;
  g_autoptr(GFile) music_dir = NULL;
  g_autoptr(GFile) video_dir = NULL;
  RoamManager *manager;

  manager = roam_manager_new ();

  /* Create our discovery agents */
  avahi = roam_discovery_avahi_new ();
  local = roam_discovery_local_new ();
  volumes = roam_discovery_volumes_new ();

  /* Setup local music dir */
  music_dir = g_file_new_build_filename (g_get_home_dir (), "Music", NULL);
  roam_discovery_local_add_mount (local,
                                  "org.gnome.roam.xdg-music",
                                  "Music",
                                  "folder-music",
                                  "folder-music-symbolic",
                                  music_dir);

  /* Setup local video dir */
  video_dir = g_file_new_build_filename (g_get_home_dir (), "Videos", NULL);
  roam_discovery_local_add_mount (local,
                                  "org.gnome.roam.xdg-videos",
                                  "Videos",
                                  "folder-videos",
                                  "folder-videos-symbolic",
                                  video_dir);

  /* Register discovery agents */
  roam_manager_add_discovery (manager, ROAM_DISCOVERY (avahi));
  roam_manager_add_discovery (manager, ROAM_DISCOVERY (local));
  roam_manager_add_discovery (manager, ROAM_DISCOVERY (volumes));

  return manager;
}

static void
startup_cb (GObject      *object,
            GAsyncResult *result,
            gpointer      user_data)
{
  RoamManager *manager = (RoamManager *)object;
  g_autoptr(GError) error = NULL;
  GtkWidget *window;

  g_assert (ROAM_IS_MANAGER (manager));

  if (!roam_manager_startup_finish (manager, result, &error))
    g_error ("%s", error->message);

  window = create_window (manager);
  g_signal_connect (window, "delete-event", gtk_main_quit, NULL);
  gtk_window_present (GTK_WINDOW (window));
}

gint
main (gint   argc,
      gchar *argv[])
{
  g_autoptr(RoamManager) manager = NULL;

  gtk_init (&argc, &argv);
  manager = create_manager ();
  roam_manager_startup_async (manager, NULL, startup_cb, NULL);
  gtk_main ();

  return 0;
}
