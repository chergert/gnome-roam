/* gnome-roam-query.c
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <jsonrpc-glib.h>
#include <roam.h>
#include <stdlib.h>

static GMainLoop *main_loop;
static GTlsCertificate *cert;

static void 
load_cert_cb (GObject      *object,
              GAsyncResult *result,
              gpointer      user_data)
{
  RoamTlsManager *tls_manager = (RoamTlsManager *)object;

  g_assert (ROAM_IS_TLS_MANAGER (tls_manager));
  g_assert (G_IS_ASYNC_RESULT (result));

  cert = roam_tls_manager_load_certificate_finish (tls_manager, result, NULL);
  g_main_loop_quit (main_loop);
}

static void
on_mount_added_cb (RoamRpcClient *client,
                   GVariant      *mount_info)
{
  g_assert (ROAM_IS_RPC_CLIENT (client));
  g_assert (mount_info != NULL);
  g_assert (g_variant_is_of_type (mount_info, G_VARIANT_TYPE_VARDICT));

  g_print ("Mount Added: %s\n",
           g_variant_print (mount_info, TRUE));
}

static void
rpc_initialized_cb (GObject      *object,
                    GAsyncResult *result,
                    gpointer      user_data)
{
  RoamRpcClient *client = (RoamRpcClient *)object;
  g_autoptr(GError) error = NULL;

  g_assert (ROAM_IS_RPC_CLIENT (client));
  g_assert (G_IS_ASYNC_RESULT (result));

  if (!g_async_initable_init_finish (G_ASYNC_INITABLE (client), result, &error))
    g_error ("%s", error->message);

  g_main_loop_quit (main_loop);
}

gint
main (gint   argc,
      gchar *argv[])
{
  g_autoptr(GOptionContext) context = NULL;
  g_autoptr(GSocketClient) client = NULL;
  g_autoptr(GSocketConnection) conn = NULL;
  g_autoptr(RoamRpcClient) rpc = NULL;
  g_autoptr(GError) error = NULL;
  RoamTlsManager *tls_manager;
  const gchar *host_and_port;

  main_loop = g_main_loop_new (NULL, FALSE);
  context = g_option_context_new ("- query a roam server");

  if (!g_option_context_parse (context, &argc, &argv, &error))
    {
      g_printerr ("%s\n", error->message);
      return EXIT_FAILURE;
    }

  if (argc < 2)
    {
      g_printerr ("usage: %s HOST:PORT\n", argv[0]);
      return EXIT_FAILURE;
    }

  tls_manager = roam_tls_manager_get_default ();
  roam_tls_manager_load_certificate_async (tls_manager, NULL, load_cert_cb, NULL);
  g_main_loop_run (main_loop);
  if (cert == NULL)
    g_error ("Failed to load certificate");

  host_and_port = argv[1];

  client = g_socket_client_new ();
  g_socket_client_set_tls (client, TRUE);
  g_socket_client_set_tls_validation_flags (client, 0);

  conn = g_socket_client_connect_to_host (client, host_and_port, 0, NULL, &error);
  if (conn == NULL)
    g_error ("%s", error->message);
  g_print ("Connected.\n");

  rpc = roam_rpc_client_new_for_stream (G_IO_STREAM (conn));

  g_signal_connect (rpc,
                    "mount-added",
                    G_CALLBACK (on_mount_added_cb),
                    NULL);

  g_async_initable_init_async (G_ASYNC_INITABLE (rpc),
                               G_PRIORITY_LOW,
                               NULL,
                               rpc_initialized_cb,
                               NULL);

  g_main_loop_run (main_loop);
  g_main_loop_unref (main_loop);

  return EXIT_SUCCESS;
}
