/* gnome-roam-server.c
 *
 * Copyright © 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <roam.h>

static RoamManager *manager;

static void
manager_startup_cb (GObject      *object,
                    GAsyncResult *result,
                    gpointer      user_data)
{
  RoamManager *manager = (RoamManager *)object;
  g_autoptr(GError) error = NULL;

  g_assert (ROAM_IS_MANAGER (manager));
  g_assert (G_IS_ASYNC_RESULT (result));

  if (!roam_manager_startup_finish (manager, result, &error))
    g_error ("%s", error->message);
}

static void
activate_cb (GApplication *app,
             gpointer      user_data)
{
  g_autoptr(RoamPublisherAvahi) publisher = NULL;
  g_autoptr(RoamDiscoveryLocal) local = NULL;
  g_autoptr(RoamDiscoveryVolumes) volumes = NULL;
  g_autoptr(GFile) music_dir = NULL;
  g_autoptr(GFile) videos_dir = NULL;

  g_assert (G_IS_APPLICATION (app));

  if (manager != NULL)
    return;

  g_application_hold (app);

  manager = roam_manager_new ();

  publisher = roam_publisher_avahi_new ();
  roam_manager_add_publisher (manager, ROAM_PUBLISHER (publisher));

  local = roam_discovery_local_new ();
  roam_manager_add_discovery (manager, ROAM_DISCOVERY (local));

  volumes = roam_discovery_volumes_new ();
  roam_manager_add_discovery (manager, ROAM_DISCOVERY (volumes));

  music_dir = g_file_new_build_filename (g_get_home_dir (), "Music", NULL);
  roam_discovery_local_add_mount (local,
                                  "org.gnome.roam.xdg-music",
                                  "Music",
                                  "folder-music",
                                  "folder-music-symbolic",
                                  music_dir);

  videos_dir = g_file_new_build_filename (g_get_home_dir (), "Videos", NULL);
  roam_discovery_local_add_mount (local,
                                  "org.gnome.roam.xdg-videos",
                                  "Videos",
                                  "folder-videos",
                                  "folder-videos-symbolic",
                                  videos_dir);

  roam_manager_startup_async (manager, NULL, manager_startup_cb, NULL);
}

gint
main (gint   argc,
      gchar *argv[])
{
  g_autoptr(GApplication) app = NULL;

  app = g_object_new (G_TYPE_APPLICATION,
                      "application-id", "org.gnome.Roam.Server",
                      NULL);
  g_signal_connect (app, "activate", G_CALLBACK (activate_cb), NULL);
  return g_application_run (app, argc, argv);
}
